;
/**
 * @module userchallengestatusview
 */
define(['ember', 'scrollmixin', 'eventregister'], function (ember, scrollmixin, eventregister) {
	return Em.View.extend(scrollmixin, eventregister, {
		eventsToRegister: [{eventName: 'refreshAllData.user-challenge'}],
		scrollElement: '.scroll_custom',
		autoApply: false,
		onPromiseSuccess: function () {
			if(this.get('controller.ajaxCalls.getUserChallenge.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					this.applyScroll();
				});
			}
		}.observes('controller.ajaxCalls.getUserChallenge.status'),

		setuUIEvents: function () {
			this.bindEvents();
		}.on('didInsertElement'),
		removeUIEvents: function () {
			this.removeAllEvents();
		}.on('willDestroyElement'),
		onRefreshAllData: function (ev) {
			//true for forcing fresh call
			if(this.get('state') === 'inDOM') {
				this.get('controller').send('getUserChallenge', true);
			}
		},

		titleText: function () {
			if(this.get('controller.isChallengeYetToStart')) {
				return "Cannot view the team before the match starts";
			}
			return "Click to view team";
		}.property('controller.isChallengeYetToStart')
	});
});