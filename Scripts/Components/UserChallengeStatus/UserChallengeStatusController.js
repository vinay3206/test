;
/**
 * @module userchallengestatuscontroller
 */
define(['ember', 'userchallengemodel', 'ajaxcallstatushandler'],
	function (ember, userchallengemodel, ajaxcallstatushandler) {
		return Em.ObjectController.extend(ajaxcallstatushandler, {
			//filters from the route, i.e. challenge_id and match_id
			//are provided in this object.
			inputParams: null,
			//these three properties are required. Provide them when implementing.
			selectedTournament: null,
			selectedMatch: null,
			selectedChallenge: null,
			ajaxCallsToRegister: ['getUserChallenge'],
			/**
			 * Observer to make call to service/store on change of input params.
			 */
			onInputParamsUpdate: function() {
				if(this.get('inputParams') !== null) {
					this.send('getUserChallenge');
				}
			}.observes('inputParams'),
			isChallengeYetToStart: function () {
				if(this.get('status') === 'UpComing') {
					return true;
				}
				return false;
			}.property('status'),

			actions: {
				/**
				 * Gets user challenge info and challengers list for that challenge for the logged in user. And sets it
				 * to content.userChallengeInfo and content.challengers respectively.
				 * @param {Boolean} forceFreshCall If true then makes a fresh call not from cache
				 */
				getUserChallenge: function (forceFreshCall) {
					//default calls on this page are non-cacheable
					if(forceFreshCall === undefined || forceFreshCall === null) {
						forceFreshCall = true;
					}
					
					var filterObj = $.extend({}, this.get('inputParams'), {
						apiUrl: 'Services/ChallengeService.svc/GetChallengeDetails/{challenge_id}'
					});
					var promise = userchallengemodel.fetchChallenge(filterObj, forceFreshCall);
					//set promise to be used for loaders, etc.
					this.get('ajaxCalls.getUserChallenge').setPromise(promise);
					promise.then($.proxy(function (apiResponse) {
						this.set('content', apiResponse.data);
					}, this));
 
					//register this promise for loader
					//this event ripples to application route
					this.send('registerPromiseforLoader', promise);
				},

				/**
				 * Shows the team of the challenger. But not when the challenge is open
				 * @param  {String} teamName Name of the team for which
				 *                           to fetch team.
				 */
				showChallengerTeam: function (teamName) {
					if(this.get('isChallengeYetToStart')){
						return;
					}
					//also needs to update its current status too.
					this.send('getUserChallenge');
					//trigger event for userchallengeteamcontroller/view to listen
					$('body').trigger('selectChallenger', [teamName]);
				}
			}
		});
	});