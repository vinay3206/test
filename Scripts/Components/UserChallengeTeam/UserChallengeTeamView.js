;
/*require.config({
	paths: {
		userteamview: 'Components/UserTeam/UserTeamView'
	}
});*/
/**
 * @module userchallengeteamview
 */
define(['ember', 'userteamview', 'eventregister'],
	function (ember, userteamview, eventregister) {
		/**
		 * Overrides the base userteamview to specifically provide the template name as
		 * the module name is different thus it won't pick default name as base 
		 * userteamview.
		 * @type {UserTeamView}
		 */
		return userteamview.extend(eventregister, {
			eventsToRegister: [{eventName: 'refreshAllData.user-challenge'}],
			//overriding base functions of userteamview
			bindEvents: function () {
				this._super();
			}.on('didInsertElement'),
			removeAllEvents: function () {
				this._super();
			}.on('willDestroyElement'),
			onRefreshAllData: function (ev) {
				//true for forcing fresh call
				this.get('controller').send('getUserTeam', null, true);
			}
		});
	});