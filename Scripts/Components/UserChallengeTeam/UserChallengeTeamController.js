;
/*require.config({
	paths: {
		userteamcontroller: 'Components/UserTeam/UserTeamController'
	}
});*/
/**
 * @module userchallengeteamcontroller.
 */
define(['ember', 'userteamcontroller', 'userteam', 'eventregister'],
	function (ember, userteamcontroller, userteam, eventregister) {
		var userteam = userteam.extend();
		userteam.reopenClass({
			apiUrl: "Services/UserTeamService.svc/UserTeam/{match_id}"
		});
		/**
		 * Override UserTeamController to change the getUserTeam action.
		 */
		return userteamcontroller.extend(eventregister, {
			eventsToRegister: [{ eventName: 'selectChallenger'}],
			//these three properties are required. Provide them when implementing.
			selectedTournament: null,
			selectedMatch: null,
			selectedChallenge: null,

			selectedTeamName: null,
			allowEditTeam: function () {
				if(
					(this.get('selectedChallenge.status') === 'UpComing') &&
						((this.get('name') === this.get('selectedTeamName')) || (this.get('selectedTeamName') === null))){
					return true;
				} else {
					return false;
				}
			}.property('name', 'selectedTeamName', 'selectedChallenge.status'),

			onSelectChallenger: function (event, teamName) {
				//since object fetch reads challenge id thus we need to set it at challenge_id
				var inputParamsCopy = $.extend({},this.get('inputParams'), {'challenge_id': teamName});
				//also set it to controller for other properties to know
				this.set('selectedTeamName', teamName);
				//since it observes only inputParams object not its internal properties
				//thus needs to trigger it manually
				this.send('getUserTeam', inputParamsCopy);
			},
			registerUIEvents: function () {
				this.bindEvents();
			}.on('init'),

			actions: {
				getUserTeam: function (inputParams, forceFreshCall) {
					if(forceFreshCall === undefined || forceFreshCall === null) {
						//default calls on this page are non-cacheable
						forceFreshCall = true;
					}
					if (inputParams) {
					    //create a local copy as we dont want to change the original object
					    inputParams = $.extend({}, inputParams);
					}
					inputParams = inputParams || this.get('inputParams.match_id');
                    
					var apiUrl = userteam.apiUrl;
					if(inputParams.challenge_id) {
						//the cases where challenge_id is provided then
						//actually in those cases that challenge_id is the 
						//userteamname. We are giving it in challenge_id only
						//because its implemented in model that way.
						apiUrl += '/{challenge_id}';
						inputParams.apiUrl = apiUrl;
					}
					var promise = userteam.fetch(inputParams, forceFreshCall);
					this.get('ajaxCalls.getUserTeam').setPromise(promise);
					promise.then($.proxy(function (userTeam) {
						//userTeam.data contains the userTeam converted to client model thus can be directely used.
						this.set('content', userTeam.data);
						//call only when its not empty team.
						if(!this.get('isEmptyTeam')) {
							$('body').trigger('userTeamFetched', [this.get('inputParams.match_id'), this.get('name')]);
						}
						//$('body').trigger('updateSelectedPlayers', [userTeam.data.get('players').mapBy('id')]);
					},this));

					//register this promise for loader
					//this event ripples to application route
					this.send('registerPromiseforLoader', promise);
				}
			}
		});
	});