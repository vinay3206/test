;
/**
 * @module scorecardcontroller
 */
define(['ember', 'scorecard', 'ajaxcallstatushandler'], function (ember, scorecard, ajaxcallstatushandler) {
	return Em.ObjectController.extend(ajaxcallstatushandler, {
		ajaxCallsToRegister: ['getScorecard'],
		//params provided from the url are given here
		inputParams: null,
		/*onInputParamsUpdate: function () {
			if(this.get('inputParams') !== null) {
				this.send('getScorecard');
			}
		}.observes('inputParams'),*/
		actions: {
		    getScorecard: function (inputParams, forceFreshCall) {
                //default value for score card is to make fresh call
		        forceFreshCall = forceFreshCall || true;
				//combine provided inputParams and those present in the inputParams provided
			    //by the router
				inputParams = $.extend({}, this.get('inputParams'), inputParams);
				var promise = scorecard.fetch(inputParams, forceFreshCall);
				this.get('ajaxCalls.getScorecard').setPromise(promise);
				promise.then($.proxy(function (scorecardResponse) {
					this.set('content', scorecardResponse.data);
				}, this));

				//add this promise for loader if required
				//this event ripples to application router.
				//this.send('registerPromiseforLoader', promise);

			}
		}
	});
});