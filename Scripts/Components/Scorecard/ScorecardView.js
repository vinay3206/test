;
/**
 * @module scorecardview
 * @return {[type]} [description]
 */
define(['ember', 'eventregister'], function (ember, eventregister) {
	return Em.View.extend(eventregister, {
		eventsToRegister: [{eventName: 'userTeamFetched.scorecard'}],
		/**
		 * Overriding base bindEvents of event register mixin and calling
		 * super for the base functin to do its intended functionality.
		 */
		bindEvents: function () {
			this._super();
		}.on('init'),
		onUserTeamFetched: function (ev, match_id, teamName, forceFreshCall) {
			//all calls on scorecard are non-cacheable
			if(this.get('state') === 'inDOM') {
				this.get('controller').send('getScorecard', {team_id: teamName, match_id: match_id}, true);
			}
		},
		/**
		 * Overriding the base functionality of eventregistermixin
		 * to get itself called on 'willDestroyElement'
		 */
		removeAllEvents: function () {
			this._super();
		}.on('willDestroyElement')
	});
});