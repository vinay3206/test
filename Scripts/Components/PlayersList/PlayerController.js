;
/**
 * @module playercontroller
 */
define(['ember', 'bs_floatingmessageview'], function (ember, bs_floatingmessageview) {
	return Em.ObjectController.extend({
		needs: ['playersList'],
		//private info for debugging.
		_info: {
			name: 'PlayerController',
			desc: 'Created to become the itemController for PlayersController'
		},
		/**
		 * Calculates that wether this player can be affordable or not as per remBudget value.
		 * @return {Boolean}
		 */
		isAffordable: function () {
			return (this.get('value') <= this.get('controllers.playersList.remBudget'));
		}.property('controllers.playersList.remBudget', 'value'),

		disableAddition: function () {
			//check if low on budget
			if(!this.get('isAffordable')){
				//i.e. disable
				return true;
			}

			if(this.get('category') === "Batsman") {
				return this.get('controllers.playersList.stopBatsman');
			} else if(this.get('category') === "Bowler") {
				return this.get('controllers.playersList.stopBowler');
			} else if(this.get('category') === "AllRounder") {
				return this.get('controllers.playersList.stopAllRounder');
			} else if(this.get('category') === "WicketKeeper") {
				return this.get('controllers.playersList.stopKeeper');
			}
		}.property('category', 'controllers.playersList.stopBatsman', 'controllers.playersList.stopBowler','controllers.playersList.stopAllRounder', 'controllers.playersList.stopKeeper',
			'isAffordable'),

		actions: {
			addToUserTeam: function () {
				$('body').trigger('addToUserTeam', [this.get('model'), $.proxy(function (err, status) {
					//callback handler
					if(!err) {
						//i.e. successfuly added
						//update current player status //i.e. added
						this.set('isAdded', true);
						//alsoupdate the status from user team about vacancy
						this.send('onAddToUserTeamSuccess', status);
					} else {
						//failure
						console.error(err);
						var messageView = bs_floatingmessageview.create({
							leftCord: '30%',
							rightCord: '30%',
							topCord: '60px',
							autoClose: true,
							autoCloseInterval: 1,
							type: 'error',
							classNames: ['font16', 'trans_background', 'white-color'],
							message: err.message
						}).appendTo(KhelkundApp.rootElement);

						//show error message
					}
				}, this)]);
			}
		}
	});
});