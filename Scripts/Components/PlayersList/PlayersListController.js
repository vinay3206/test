;
/**
 * @module playerslistcontroller
 */
define(['ember', 'ajaxcallstatushandler', 'player'], function (ember, ajaxcallstatushandler, player) {
	//update player model as per ourneeds and create a local copy of it.
	var player = player.reopen({
		//to keep track if its been already added to the userTeam or not
		isAdded: false
	});


    return Em.ArrayController.extend(ajaxcallstatushandler, {
		//itemController: 'activeChallenge',
		ajaxCallsToRegister: ['getPlayersList'],
		/**
		 * Contains the filters provided by the router.
		 * @type {Object}
		 */
		inputParams: null,
		stopBatsman: false,
		stopBowler: false,
		stopAllRounder: false,
		stopKeeper: false,
		remBudget: 10000000,
		/**
		 * Makes call to service for the players list. Triggered when the input match id in url changes.
		 * Thus each time match id canges this makes call to store for getting players list.
		 */
		onInputParamsUpdate: function () {
			if(this.get('inputParams')!== null){
				this.send('getPlayersList');
			}
			
		}.observes('inputParams'),

		batsmanFilter: {
			category: 'Batsman',
			teamName: 'all',
			searchText: '',
			sortingParameter: 'value',
			sortingOrder: 'desc'
		},
		batsmans: function () {
			var filterObj = this.get('batsmanFilter');
			var filteredList = this.filterPlayers(filterObj);
			var sortedList = filteredList.sortBy(Em.get(filterObj, 'sortingParameter'));
			if(Em.get(filterObj, 'sortingOrder') === 'desc') {
				sortedList.reverseObjects();
			}
			return sortedList;
		}.property('content.@each', 'batsmanFilter.teamName', 'batsmanFilter.searchText', 'batsmanFilter.sortingOrder', 'batsmanFilter.sortingParameter'),
		bowlerFilter: {
			category: 'Bowler',
			teamName: 'all',
			searchText: '',
			sortingParameter: 'value',
			sortingOrder: 'desc'
		},
		bowlers: function () {
			var filterObj = this.get('bowlerFilter');
			var filteredList = this.filterPlayers(filterObj);
			var sortedList = filteredList.sortBy(Em.get(filterObj, 'sortingParameter'));
			if(Em.get(filterObj, 'sortingOrder') === 'desc') {
				sortedList.reverseObjects();
			}
			return sortedList;
		}.property('content.@each', 'bowlerFilter.teamName', 'bowlerFilter.searchText', 'bowlerFilter.sortingOrder', 'bowlerFilter.sortingParameter'),
		allRounderFilter: {
			category: 'AllRounder',
			teamName: 'all',
			searchText: '',
			sortingParameter: 'value',
			sortingOrder: 'desc'
		},
		allRounders: function () {
			var filterObj = this.get('allRounderFilter');
			var filteredList = this.filterPlayers(filterObj);
			var sortedList = filteredList.sortBy(Em.get(filterObj, 'sortingParameter'));
			if(Em.get(filterObj, 'sortingOrder') === 'desc') {
				sortedList.reverseObjects();
			}
			return sortedList;
		}.property('content.@each', 'allRounderFilter.teamName', 'allRounderFilter.searchText', 'allRounderFilter.sortingOrder', 'allRounderFilter.sortingParameter'),
		keeperFilter: {
			category: 'WicketKeeper',
			teamName: 'all',
			searchText: '',
			sortingParameter: 'value',
			sortingOrder: 'desc'
		},
		keepers: function () {
			var filterObj = this.get('keeperFilter');
			var filteredList = this.filterPlayers(filterObj);
			var sortedList = filteredList.sortBy(Em.get(filterObj, 'sortingParameter'));
			if(Em.get(filterObj, 'sortingOrder') === 'desc') {
				sortedList.reverseObjects();
			}
			return sortedList;
		}.property('content.@each', 'keeperFilter.teamName', 'keeperFilter.searchText', 'keeperFilter.sortingOrder', 'keeperFilter.sortingParameter'),

		filterPlayers: function (params) {
			return this.get('content').filter(function (playerItem, index, contentArr) {
				if(Em.get(playerItem, 'category') === params.category){
					if(Em.get(params, 'teamName').toLowerCase() !== 'all' && Em.get(params, 'teamName') !== Em.get(playerItem, 'team.name')){
						return false;
					}
					var playerName = (Em.get(playerItem, 'firstName') + ' ' + Em.get(playerItem, 'lastName')).toLowerCase();
					if(Em.get(params, 'searchText') !== '' && playerName.indexOf(Em.get(params, 'searchText').toLowerCase()) === -1) {
						return false;
					}
					return true;
				} else {
					return false;
				}
			});
		},
		
		updateVacancyStatus: function (status) {
			this.setProperties({
				stopBatsman: status.batsmansFull,
				stopBowler: status.bowlersFull,
				stopAllRounder: status.allRoundersFull,
				stopKeeper: status.keepersFull,
				remBudget: status.remBudget
			});
		},
		/**
		 * Updates the players with the ids provided to mark them as added(isAdded: true).
		 * @param  {Array} playerIds Contains the list of players to mark as added. To disable them from further addition.
		 */
		updateSelectedPlayers: function (playerIds) {
			if(Em.isArray(playerIds)){
				//check if player call is still in progress then wait.
				if(this.get('ajaxCalls.getPlayersList.callPromise')){
					this.get('ajaxCalls.getPlayersList.callPromise').then($.proxy(function () {
						//running in next loop cuz we want the content to be setup.
						Em.run.next(this, function () {
							this.get('content').forEach(function (playerItem, index) {
								var isPresent = playerIds.contains(playerItem.id);
								if(isPresent){
									playerItem.set('isAdded', true);
								}
							}, this);
						});
					}, this));
				}
			}
		},
		returnToPlayerPool: function (player, status) {
			var foundPlayer = this.get('model').findBy('id', Em.get(player, 'id'));
			if(foundPlayer !== undefined) {
				//add this player back to pool
				foundPlayer.set('isAdded', false);
				this.updateVacancyStatus(status);
			}
		},
		actions: {
			getPlayersList: function () {
				//get challenges from service.
				//forcing fresh call cuz of some problems
				var promise = player.fetch(this.get('inputParams'), true);
				this.get('ajaxCalls.getPlayersList').setPromise(promise);
				promise.then($.proxy(function (apiResponse) {
					//received playersList is already converted to array of playermodel types
					this.set('content', apiResponse.data);
				}, this));

				//register this promise for loader
				//this event ripples to application route
				this.send('registerPromiseforLoader', promise);
			},
			onAddToUserTeamSuccess: function (status) {
				this.updateVacancyStatus(status);
			},
			toggleSorting: function (category, parameter) {
				if(category === 'batsman') {
					if(parameter === 'points') {
						this.set('batsmanFilter.sortingParameter', 'points');
						if(this.get('batsmanFilter.sortingOrder') === 'desc') {
							this.set('batsmanFilter.sortingOrder', 'asc');
						} else {
							this.set('batsmanFilter.sortingOrder', 'desc');
						}
					} else if (parameter === 'value') {
						this.set('batsmanFilter.sortingParameter', 'value');
						if(this.get('batsmanFilter.sortingOrder') === 'desc') {
							this.set('batsmanFilter.sortingOrder', 'asc');
						} else {
							this.set('batsmanFilter.sortingOrder', 'desc');
						}
					}
				} else if(category === 'bowler') {
					if(parameter === 'points') {
						this.set('bowlerFilter.sortingParameter', 'points');
						if(this.get('bowlerFilter.sortingOrder') === 'desc') {
							this.set('bowlerFilter.sortingOrder', 'asc');
						} else {
							this.set('bowlerFilter.sortingOrder', 'desc');
						}
					} else if (parameter === 'value') {
						this.set('bowlerFilter.sortingParameter', 'value');
						if(this.get('bowlerFilter.sortingOrder') === 'desc') {
							this.set('bowlerFilter.sortingOrder', 'asc');
						} else {
							this.set('bowlerFilter.sortingOrder', 'desc');
						}
					}
				} else if (category === 'allRounder') {
					if(parameter === 'points') {
						this.set('allRounderFilter.sortingParameter', 'points');
						if(this.get('allRounderFilter.sortingOrder') === 'desc') {
							this.set('allRounderFilter.sortingOrder', 'asc');
						} else {
							this.set('allRounderFilter.sortingOrder', 'desc');
						}
					} else if (parameter === 'value') {
						this.set('allRounderFilter.sortingParameter', 'value');
						if(this.get('allRounderFilter.sortingOrder') === 'desc') {
							this.set('allRounderFilter.sortingOrder', 'asc');
						} else {
							this.set('allRounderFilter.sortingOrder', 'desc');
						}
					}
				} else if (category === 'keeper') {
					if(parameter === 'points') {
						this.set('keeperFilter.sortingParameter', 'points');
						if(this.get('keeperFilter.sortingOrder') === 'desc') {
							this.set('keeperFilter.sortingOrder', 'asc');
						} else {
							this.set('keeperFilter.sortingOrder', 'desc');
						}
					} else if (parameter === 'value') {
						this.set('keeperFilter.sortingParameter', 'value');
						if(this.get('keeperFilter.sortingOrder') === 'desc') {
							this.set('keeperFilter.sortingOrder', 'asc');
						} else {
							this.set('keeperFilter.sortingOrder', 'desc');
						}
					}
				}
			}
		},
		
    });
});