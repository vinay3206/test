;
/**
 * @module playerslistview
 */
define(['ember', 'eventregister', 'scrollmixin'], function (ember, eventregister, scrollmixin) {
    return Em.View.extend(eventregister, scrollmixin, {
		eventsToRegister: [{ eventName: 'returnToPlayerPool'}, {eventName: 'updateVacancyStatus'},
			{eventName: 'updateSelectedPlayers'}
		],
		bindEvents: function () {
			this._super();
		}.on('init'),
		removeAllEvents: function () {
			this._super();
		}.on('willDestroyElement'),

		getPlayersList: function () {
			this.get('controller').send('getPlayersList');
		}.on('didInsertElement'),

		//dom events
		onReturnToPlayerPool: function (ev, player, status) {

			this.get('controller').returnToPlayerPool(player, status);
		},
		onUpdateVacancyStatus: function (ev, status) {
			this.get('controller').updateVacancyStatus(status);
		},
		onUpdateSelectedPlayers: function (ev, playerIds) {
			this.get('controller').updateSelectedPlayers(playerIds);
		},

		initializeEasyResponsive: function () {
			$('.pool_playerstab', this.$()).easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true,   // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);

                    $name.text($tab.text());

                    $info.show();
                }
            });
		}.on('didInsertElement'),
		bindEasyTabEvents: function () {
			$($('.pool_playerstab'), this.$()).on('tabactivate', $.proxy(function (e) {
				Em.run.next(this, function () {
					this.updateScroll();
				});
			}, this));
		}.on('didInsertElement'),
		scrollElement: '.scroll_custom1',
		autoApply: false,
		onPromiseSuccess: function () {
			if(this.get('controller.ajaxCalls.getPlayersList.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					this.applyScroll();
				});
			}
		}.observes('controller.ajaxCalls.getPlayersList.status')

    });
});