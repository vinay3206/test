;
/**
 * @module playerview
 */
define(['ember'], function (ember) {
	return Em.View.extend({
		templateName: 'player'
	});
});