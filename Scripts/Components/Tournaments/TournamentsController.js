;
define(['ember', 'ajaxcallstatushandler', 'tournamentmodel'], function (ember, ajaxcallstatushandler, tournamentmodel) {
	return Em.ArrayController.extend(ajaxcallstatushandler, {
		needs: ['application'],
		ajaxCallsToRegister: ['getTournaments'],
		isUserLoggedIn: function () {
			return this.get('controllers.application.userInfo') !== null;
		}.property('controllers.application.isUserLoggedIn'),
		t20: function () {
			//filter on the basis of match type.
			return [];
		}.property('content'),
		ipl: function () {
			//filter on the basis of match type.
			return [];
		}.property('content'),
		actions: {
			getTournaments: function () {
				//fetch tournaments
				var promise = tournamentmodel.fetch();
				this.get('ajaxCalls.getTournaments').setPromise(promise);
				promise.then($.proxy(function (tournaments) {
					this.set('content', tournaments.data);
				}, this));

				//register this promise for loader.
				//this actions ripples to application router
				this.send('registerPromiseforLoader', promise);
			}
		}
	});
});