;
define(['ember', 'scrollmixin'], function (ember, scrollmixin) {
	return Em.View.extend(scrollmixin, {
		getTournaments: function () {
			this.get('controller').send('getTournaments');
		}.on('didInsertElement'),
		scrollElement: '.scroll_custom',
		autoApply: false,
		onPromiseSuccess: function () {
			if(this.get('controller.ajaxCalls.getTournaments.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					this.applyScroll();
				});
			}
		}.observes('controller.ajaxCalls.getTournaments.status')
	});
});