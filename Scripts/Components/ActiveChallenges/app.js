﻿;
require.config({
    paths: {
        activechallengescontroller: 'Components/ActiveChallenges/ActiveChallengesController',
        activechallengesview: '',
        activechallengecontroller: '',
        activechallengeview: ''

    }
});

define(['activechallengescontroller', 'activechallengesview', 'activechallengecontroller', 'activechallengeview'],
    function (activechallengescontroller, activechallengesview, activechallengecontroller, activechallengeview) {
        return [activechallengescontroller, activechallengesview, activechallengecontroller, activechallengeview];

    });