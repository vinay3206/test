;
define(['ember'], function (ember) {
    return Em.View.extend({
		templateName: 'activeChallenge',

		//to be provided by hbs
		match_id: null,
		//to be provided by hbs
		challenge_id: null,
		linkTransitionModel: function () {
			return {
				match_id: this.get('match_id'),
				challenge_id: this.get('challenge_id')
			};
		}.property('match_id', 'challenge_id')
    });
});