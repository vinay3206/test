;
/**
 * @module activechallengescontroller
 */
define(['ember', 'ajaxcallstatushandler', 'userchallengemodel'], function (ember, ajaxcallstatushandler, userchallengemodel) {

	var userchallengemodel = userchallengemodel.extend().reopenClass({
		apiUrl: 'Services/ChallengeService.svc/GetActiveChallenges'
	});

    return Em.ObjectController.extend(ajaxcallstatushandler, {
		//itemController: 'activeChallenge',
		ajaxCallsToRegister: ['getChallenges', 'getCompletedChallenges'],
		actions: {
			/**
			 * Gets the user challenges as per logged in user. i.e. the challenges joined by the user.
			 * Also sets them to the content of the controller.
			 */
			getChallenges: function () {
				//get challenges from service.
				//non-cached call always
				var promise = userchallengemodel.fetch(this.get('userId'), null, true);
				this.get('ajaxCalls.getChallenges').setPromise(promise);
				promise.then($.proxy(function (challenges) {
					if(!this.get('content')) {
						this.set('content', Em.Object.create());
					}
					this.set('content.activeChallenges', challenges.data);
				}, this));

				//register this promise for loader
				//this event ripples to application route
				this.send('registerPromiseforLoader', promise);
			},
			getCompletedChallenges: function () {
				var apiUrl = 'Services/ChallengeService.svc/GetCompletedChallenges';
				//get challenges from service.
				//non-cached call always
				var promise = userchallengemodel.fetch(this.get('userId'), null, true, apiUrl);
				this.get('ajaxCalls.getCompletedChallenges').setPromise(promise);
				promise.then($.proxy(function (challenges) {
					if(!this.get('content')) {
						this.set('content', Em.Object.create());
					}
					this.set('content.completedChallenges', challenges.data);
				}, this));

				//no need to register for loader in  this call
				/*//this event ripples to application route
				this.send('registerPromiseforLoader', promise);	*/
			}
		}
    });
});