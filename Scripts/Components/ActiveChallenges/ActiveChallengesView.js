;
define(['ember', 'scrollmixin'], function (ember, scrollmixin) {
    return Em.View.extend(scrollmixin, {
		getChallenges: function () {
			this.get('controller').send('getChallenges');
			this.get('controller').send('getCompletedChallenges');
		}.on('didInsertElement'),
		initializeEasyResponsive: function () {
			$('#act-comp-challengesTab', this.$()).easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true,   // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);

                    $name.text($tab.text());

                    $info.show();
                }
            });
		}.on('didInsertElement'),
		bindEasyTabEvents: function () {
			$('#act-comp-challengesTab', this.$()).on('tabactivate', $.proxy(function (e) {
				Em.run.next(this, function () {
					this.updateScroll();
				});
			}, this));
		}.on('didInsertElement'),
		scrollElement: '.scroll_custom',
		autoApply: false,
		onPromiseSuccess: function () {
			if(this.get('controller.ajaxCalls.getChallenges.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					//run scroll on first element when its call is success
					this.applyScroll(this.$('.scroll_custom.jactive_challenges'));
				});
			}

			if(this.get('controller.ajaxCalls.getCompletedChallenges.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					//run scroll on second element when its call is success
					this.applyScroll(this.$('.scroll_custom.jcompleted_challenges'));
				});
			}
		}.observes('controller.ajaxCalls.getChallenges.status', 'controller.ajaxCalls.getCompletedChallenges.status')
    });
});