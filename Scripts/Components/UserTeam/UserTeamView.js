;
/**
 * @module userteamview
 */
define(['ember', 'eventregister'], function (ember, eventregister) {
	return Em.View.extend(eventregister, {
		eventsToRegister: [{ eventName: 'addToUserTeam'}],
		bindEvents: function () {
			this._super();
		}.on('init'),
		removeAllEvents: function () {
			this._super();
		}.on('willDestroyElement'),

		//dom events
		onAddToUserTeam: function (ev, player, callback) {
			this.get('controller').send('addPlayer', player, callback);
		},


		//dom events handling ends here

		/*getUserTeam: function () {
			this.get('controller').send('getUserTeam');
		}.on('didInsertElement'),*/

		disableSaveTeam: Em.computed.not('controller.isTeamComplete.status'),



	});
});