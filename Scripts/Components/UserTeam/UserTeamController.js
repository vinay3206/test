;
/**
 * @module userteamcontroller
 */
define(['ember', 'ajaxcallstatushandler', 'userteam', 'teamplayer', 'bs_floatingmessageview'], function (ember, ajaxcallstatushandler, userteam, teamplayer, bs_floatingmessageview) {

	//create local copy of teamplayer for specific/personal use.
	var teamplayer = teamplayer.extend({
		isEmpty: false
	});

	return Em.ObjectController.extend(ajaxcallstatushandler, {
		//all the filters from the app are provided in this variable
		inputParams: null,
		ajaxCallsToRegister: ['getUserTeam', 'saveUserTeam'],
		/**
		 * It contains the count of players allowed from same team.
		 * @type {Number}
		 */
		allowedPlayersFromSameTeam: 7,
		/**
		 * Makes a call to service/store if params are changed.
		 */
		onInputParamsUpdate: function () {
			if(this.get('inputParams')!== null){
				this.send('getUserTeam');
			}
		}.observes('inputParams'),

		formationConfig: function () {
			var formationObj = {
				batsmans: 0,
				bowlers: 0,
				allRounders: 0,
				keepers: 0
			};
			if(this.get('selectedFormation')) {
				var distribution = this.get('selectedFormation').split('_');
				if(distribution.length === 4) {
					formationObj = {
						batsmans: +distribution[0],
						bowlers: +distribution[1],
						allRounders: +distribution[2],
						keepers: +distribution[3]
					};
				}
			}
			return formationObj;
		}.property('selectedFormation'),
		/**
		 * It deciede wether the team formation is complete. Can be used to enable or disable saveTeam buttton.
		 * @return {Object} Contains status and reasons(Array).
		 */
		isTeamComplete: function () {
			var completionStatus = {
				status: true,
				reasons: []	//array of reasons
			};

			var formationObj = this.get('formationConfig');
			if(formationObj) {
				if(this.get('batsmans.length') !== formationObj.batsmans){
					completionStatus.status = false;
					completionStatus.reasons.push('Not enough batsmans.' + formationObj.batsmans + ' are required.');
				}
				if(this.get('bowlers.length') !== formationObj.bowlers){
					completionStatus.status = false;
					completionStatus.reasons.push('Not enough bowlers.' + formationObj.bowlers + ' are required.');
				}
				if(this.get('allRounders.length') !== formationObj.allRounders){
					completionStatus.status = false;
					completionStatus.reasons.push('Not enough allRounders.' + formationObj.allRounders + ' are required.');
				}
				if(this.get('keepers.length') !== formationObj.keepers){
					completionStatus.status = false;
					completionStatus.reasons.push('Not enough keepers.' + formationObj.keepers + ' are required.');
				}
			}

			if(!this.get('captainId')) {
				completionStatus.status = false;
				completionStatus.reasons.push('Captain not selected. Use star icon to make a player as captain.');
			}
			return completionStatus;
		}.property('formationConfig', 'captainId', 'batsmans.length', 'bowlers.length', 'allRounders.length', 'keepers.length'),
		invalidTeamReason: function () {

		}.property(),
		/**
		 * Removes the unnecessary players from the team.
		 */
		reCreateValidTeam: function () {
			var formationObj = this.get('formationConfig');
			var diff, playersToRemove = [], i;
			if(Em.get(formationObj, 'batsmans') < this.get('batsmans').length){
				diff = this.get('batsmans').length - Em.get(formationObj, 'batsmans');
				for(i=0; i< diff; i++) {
					playersToRemove.pushObject(this.get('batsmans').objectAt(this.get('batsmans').length - i -1));
				}
			}
			if(Em.get(formationObj, 'bowlers') < this.get('bowlers').length){
				diff = this.get('bowlers').length - Em.get(formationObj, 'bowlers');
				for(i=0; i< diff; i++) {
					playersToRemove.pushObject(this.get('bowlers').objectAt(this.get('bowlers').length - i -1));
				}
			}
			if(Em.get(formationObj, 'allRounders') < this.get('allRounders').length){
				diff = this.get('allRounders').length - Em.get(formationObj, 'allRounders');
				for(i=0; i< diff; i++) {
					playersToRemove.pushObject(this.get('allRounders').objectAt(this.get('allRounders').length - i -1));
				}
			}
			if(Em.get(formationObj, 'keepers') < this.get('keepers').length){
				diff = this.get('keepers').length - Em.get(formationObj, 'keepers');
				for(i=0; i< diff; i++) {
					playersToRemove.pushObject(this.get('keepers').objectAt(this.get('keepers').length - i -1));
				}
			}

			playersToRemove.forEach(function (playerItem, index) {
				this.send('removePlayer', playerItem);
			}, this);
			//also update vacancy status to players list. Since it can also change without affect the
			//present formation to invalidate.
			$('body').trigger('updateVacancyStatus', [this.get('vacancyStatus')]);
		}.observes('selectedFormation'),
		/**
		 * Creates an array for empty batsmans based on selected formation and number of batsmans already added.
		 * @return {Array} Array of empty batsmans to show empty players to fill the space.
		 */
		emptyBatsmans: function () {
			var emptyPlayers = [];
			for(var i=this.get('content.batsmans.length'); i< this.get('formationConfig.batsmans'); i++){
				emptyPlayers.pushObject(teamplayer.create({
					category: 'Batsman',
					firstName: 'BAT',
					isEmpty: true
				}));
			}
			return emptyPlayers;
		}.property('content.batsmans.length', 'formationConfig.batsmans'),
		/**
		 * Create and array for empty players based on selected formation and number of bowlers already added.
		 * @return {Array} Array of empty players to fill the empty space it its section
		 */
		emptyBowlers: function () {
			var emptyPlayers = [];
			for(var i=this.get('content.bowlers.length'); i< this.get('formationConfig.bowlers'); i++){
				emptyPlayers.pushObject(teamplayer.create({
					category: 'Bowler',
					firstName: 'BOW',
					isEmpty: true
				}));
			}
			return emptyPlayers;
		}.property('content.bowlers.length', 'formationConfig.bowlers'),
		/**
		 * Create and array for empty players based on selected formation and number of allRounders already added.
		 * @return {Array} Array of empty players to fill the empty space it its section
		 */
		emptyAllRounders: function () {
			var emptyPlayers = [];
			for(var i=this.get('content.allRounders.length'); i< this.get('formationConfig.allRounders'); i++){
				emptyPlayers.pushObject(teamplayer.create({
					category: 'AllRounder',
					firstName: 'AR',
					
					isEmpty: true
				}));
			}
			return emptyPlayers;
		}.property('content.allRounders.length', 'formationConfig.allRounders'),
		/**
		 * Create and array for empty players based on selected formation and number of keepers already added.
		 * @return {Array} Array of empty players to fill the empty space it its section
		 */
		emptyKeepers: function () {
			var emptyPlayers = [];
			for(var i=this.get('content.keepers.length'); i< this.get('formationConfig.keepers'); i++){
				emptyPlayers.pushObject(teamplayer.create({
					category: 'WicketKeeper',
					firstName: 'WK',
					
					isEmpty: true
				}));
			}
			return emptyPlayers;
		}.property('content.keepers.length', 'formationConfig.keepers'),

		vacancyStatus: function () {
			return {
				batsmansFull: this.get('batsmans').length === this.get('formationConfig.batsmans'),
				bowlersFull: this.get('bowlers').length === this.get('formationConfig.bowlers'),
				allRoundersFull: this.get('allRounders').length === this.get('formationConfig.allRounders'),
				keepersFull: this.get('keepers').length === this.get('formationConfig.keepers'),
				remBudget: this.get('remBudget')
			};
		}.property('formationConfig', 'players.length', 'remBudget'),
		actions: {
			/**
			 * Asks the userteam model to fetch team based on input parameters. And set the team 
			 * on content property of the controller.
			 */
			getUserTeam: function () {
				//get userTeam from service
				var promise = userteam.fetch(this.get('inputParams.match_id'));
				this.get('ajaxCalls.getUserTeam').setPromise(promise);
				promise.then($.proxy(function (userTeam) {
					//userTeam.data contains the userTeam converted to client model thus can be directely used.
					this.set('content', userTeam.data);
					$('body').trigger('updateSelectedPlayers', [userTeam.data.get('players').mapBy('id')]);
				},this));

				//register this promise for loader
				//this event ripples to application route
				this.send('registerPromiseforLoader', promise);
			},
			saveTeam: function () {
				if(this.get('isTeamComplete.status') === false) {
					var messageView = bs_floatingmessageview.create({
						leftCord: '30%',
						rightCord: '30%',
						topCord: '60px',
						autoClose: true,
						autoCloseInterval: 2,
						type: 'error',
						classNames: ['font16', 'trans_background', 'white-color'],
						viewHeader: 'Invalid Team',
						message: this.get('isTeamComplete.reasons.firstObject')
					}).appendTo(KhelkundApp.rootElement);
					return;
				}


				var savePromise = this.get('model').save(this.get('inputParams.match_id'));
				this.get('ajaxCalls.saveUserTeam').setPromise(savePromise);
				savePromise.then(function () {
					//success
					var messageView = bs_floatingmessageview.create({
						leftCord: '30%',
						rightCord: '30%',
						topCord: '60px',
						autoClose: true,
						autoCloseInterval: 2,
						type: 'success',
						classNames: ['font16', 'trans_background', 'white-color'],
						message: "Save Team Success! Redirecting you back.."
					}).appendTo(KhelkundApp.rootElement);

					//redirect to the page where it came from.
					Em.run.later(this, function () {
						history.back();
					}, 2000);

				}, function (reason) {
					//failure
					console.error(reason);
					var messageView = bs_floatingmessageview.create({
						leftCord: '30%',
						rightCord: '30%',
						topCord: '60px',
						autoClose: true,
						type: 'error',
						classNames: ['font16', 'trans_background', 'white-color'],
						message: "Save Team Failed. Try Again."
					}).appendTo(KhelkundApp.rootElement);

					//show error message
				});

				//register this promise for loader
				//this event ripples to application route
				this.send('registerPromiseforLoader', savePromise);
			},
			/*
			@param: object. Player to add to userTeam
			@param: function(err, status). error is null in case of success else send message in err object. Status object sends status after successful operation.
			*/
			addPlayer: function (player, callback) {
				if(!callback) {
					callback = Em.K;
				}

				//check if already exists


				//validate player addition
				if(player.category === "Batsman" && (this.get('batsmans').length >= this.get('formationConfig.batsmans'))) {
					callback({message: "Batsman slots already full."});
					return;
				} else if(player.category === 'Bowler' && (this.get('bowlers').length >= this.get('formationConfig.bowlers'))) {
					callback({message: "Bowler slots already full."});
					return;
				} else if(player.category === 'AllRounder' && (this.get('allRounders').length >= this.get('formationConfig.allRounders'))) {
					callback({message: "AllRounder slots already full."});
					return;
				} else if(player.category === 'WicketKeeper' && (this.get('keepers').length >= this.get('formationConfig.keepers'))) {
					callback({message: "WicketKeeper slots already full."});
					return;
				}

				//check for number of players from same team.
				var playerTeamName = Em.get(player, 'team.name');
				if(playerTeamName) {
					var sameTeamPlayersCount = 0;
					this.get('players').forEach(function (playerItem, index) {
						if(playerTeamName === playerItem.get('team.name')) {
							sameTeamPlayersCount++;
						}
					}, this);
					if (sameTeamPlayersCount === this.get('allowedPlayersFromSameTeam')){
						//i.e. same team rule is at its limit
						callback({message: 'You can\'t have more than ' + this.get('allowedPlayersFromSameTeam') + ' players from same team.'});
						return;
					}
				}
				

				//todo: create a copy function for the object and use it here.
				this.get('players').pushObject(teamplayer.create(player));
				Em.run.once(this, function () {
					callback(null, this.get('vacancyStatus'));
				});
			},
			/**
			 * Removes player from its team and triggers event: "returnToPlayerPool"
			 * @param  {TeamPlayer} player Instance of TeamPlayer model.
			 */
			removePlayer: function (player) {
				this.get('players').removeObject(player);
				$('body').trigger('returnToPlayerPool', [player, this.get('vacancyStatus')]);
			},
			/**
			 * Makes current player as team captain and remove the captain from current player 
			 * if any.
			 * @param  {TeamPlayer} player Player whom to make captain.
			 */
			makeCaptain: function (player) {
				//turn off captain from all players
				this.get('players').forEach(function (playerDatum, index) {
					playerDatum.set('isCaptain', false);
				});
				//make provided player as caption
				player.set('isCaptain', true);
			}
		}
	});
});