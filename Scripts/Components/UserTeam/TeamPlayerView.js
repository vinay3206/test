;
/**
 * @module teamplayerview
 */
define(['ember'], function (ember) {
	return Em.View.extend({
		templateName: 'teamPlayer'
	});
});