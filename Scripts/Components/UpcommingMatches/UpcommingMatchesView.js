;
define(['ember', 'scrollmixin'], function (ember, scrollmixin) {
    return Em.View.extend(scrollmixin, {
		getUpcommingMatches: function () {
			this.get('controller').send('getUpcommingMatches');
		}.on('didInsertElement'),
		scrollElement: '.scroll_custom',
		autoApply: false,
		onPromiseSuccess: function () {
			if(this.get('controller.ajaxCalls.getUpcommingMatches.isSuccess') === true){
				//run next because current run loop will add elements to DOM
				Em.run.next(this, function () {
					this.applyScroll();
				});
			}
		}.observes('controller.ajaxCalls.getUpcommingMatches.status')
    });
});