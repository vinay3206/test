;
define(['ember', 'ajaxcallstatushandler', 'match'], function (ember, ajaxcallstatushandler, match) {
    return Em.ArrayController.extend(ajaxcallstatushandler, {
		//itemController: 'upcommingMatch',
		ajaxCallsToRegister: ['getUpcommingMatches'],
		actions: {
			getUpcommingMatches: function () {
				//get challenges from service.
				var promise = match.fetch();
				this.get('ajaxCalls.getUpcommingMatches').setPromise(promise);
				promise.then($.proxy(function (matches) {
					this.set('content', matches.data);
				}, this));

				//register promise for loader
				//this event ripples to application router
				this.send('registerPromiseforLoader', promise);
			}
		}
    });
});