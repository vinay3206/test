;
define(['ember'], function (ember) {
    return Em.ObjectController.extend({
		needs: ['application'],
		name: 'upcommingMatch'
    });
});