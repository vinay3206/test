﻿
//require.config({
//    paths: {

//        addUserTeamView: 'Apps/UserProfile/View/AddUserTeamView'

//    }
//});


define(['ajaxcallstatushandler', 'khelkundutils', 'user', 'addUserTeamView', 'eventregister', 'loginpopupview'],


    function (ajaxcallstatushandler, khelkundutils, user, addUserTeamView, eventregister, loginpopupview) {

        return Em.Component.extend(eventregister,Khelkund.Mixins.AjaxCallsHandler, {

            ajaxCallsToRegister: ['logout','getLoggedInUser'],

            eventsToRegister: [{ eventName: 'userLoggedIn' }],

            bindUIEvents:function(){
                this.bindEvents();

            }.on('init'),

            onUserLoggedIn: function (ev, user) {
                this.onGetLoggedInUserSuccess(user);

            },

            onGetLoggedInUserSuccess:function(user){
                this.set('user', user);
                if (!this.get('user.personalInfo.teamName')) {
                    this.set('showAddTeamName', true);
                }
                this.sendAction('getUserInfoSuccess', user);


            },


            getLoggedInUser: function () {

                var promise = user.fetchLoggedInUser();
                this.get('ajaxCalls.getLoggedInUser').setPromise(promise);
                promise.then($.proxy(function (apiResult) {
                    this.onGetLoggedInUserSuccess(apiResult.data);
                }, this));

              

            }.on('init'),

            user: null,

            showAddTeamName: false,

            showLoginPopUp:false,



            addTeamNameView: addUserTeamView.extend({

                

                showPopUp: function () {
                    this.popup();

                }.on('didInsertElement')
            }),


            loginPopUpView: loginpopupview.extend({
                showPopUp: function () {
                    this.popup();

                }.on('didInsertElement')

            }),


            onGetUserInfoSuccess:function(){
                
                if(this.get('user')){
                   var intervalHandler=setInterval(function () {
                       this.authenticateUser();
                   }.bind(this), 120000);
                   this.set('intervalHandler', intervalHandler);
                }
                else{
                    if(this.get('intervalHandler')){
                        clearInterval(this.get('intervalHandler'));
                    }
                }

            }.observes('user'),


            authenticateUser:function(){
                khelkundutils.ajax.get({
                    url: 'Services/LoginService.svc/Authenticate',

                }, $.proxy(function (apiResult) {
                    if (!khelkundutils.ajax.validateApiResult(apiResult)) {
                        this.set('showLoginPopUp', true);
                        this.sendAction('getUserInfoSuccess', null);
                        this.set('user', null);
                    }

                }, this), $.proxy(function (err) {
                    this.set('showLoginPopUp', true);
                    this.send('getUserInfoSuccess', null);
                    this.set('user', null);
                },this));
            },






            actions: {
                logout: function () {

                    var logoutStatus = khelkundutils.ajax.get({
                        url: 'Services/LoginService.svc/Logout',
                       
                    },
                                   $.proxy(function (apiResult) {
                                       if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                           this.sendAction('getUserInfoSuccess', null);
                                           khelkundutils.redirect({url:'Home.aspx'})
                                           

                                       }
                                   }, this),
                                   $.proxy(function (err) {
                                       console.log('Failure');
                                   }, this));
                    this.get('ajaxCalls.logout').setPromise(logoutStatus);

                    
                }



            }

        });






    });