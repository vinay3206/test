{
"Error":null,
"Status":0,
"Series":[
{
"Id":"53138934715253191",
"Matches":[
{
"AwayTeamName":"Royal Challengers Bangalore",
"AwayTeamShortName":"RCB",
"Deadline_Date":"2014\/03\/17",
"Deadline_Time":"14:30:00",
"HomeTeamName":"Rajasthan Royals",
"HomeTeamShortName":"RR",
"Id":"53138854480314795",
"MatchName":null,
"Match_Date":"17 Mar",
"Match_Time":"14:30",
"Type":0,
"Venue":"Jaipur"
},
{
"AwayTeamName":"Delhi Daredevils",
"AwayTeamShortName":"DD",
"Deadline_Date":"2014\/03\/15",
"Deadline_Time":"10:30:00",
"HomeTeamName":"Chennai Super Kings",
"HomeTeamShortName":"CSK",
"Id":"53138700440306238",
"MatchName":null,
"Match_Date":"15 Mar",
"Match_Time":"10:30",
"Type":0,
"Venue":"Chennai"
}
],
"Name":"IPL-2014",
"Schedule":{
"EndDate":"2014-04-09T18:30:00.0000000",
"StartDate":"2014-03-11T18:30:00.0000000"
},
"SeriesType":1
}
],
"UpComingMatches":[
{
"AwayTeamName":"Royal Challengers Bangalore",
"AwayTeamShortName":"RCB",
"Deadline_Date":"2014\/03\/17",
"Deadline_Time":"14:30:00",
"HomeTeamName":"Rajasthan Royals",
"HomeTeamShortName":"RR",
"Id":"53138854480314795",
"MatchName":null,
"Match_Date":"17 Mar",
"Match_Time":"14:30",
"Type":0,
"Venue":"Jaipur"
},
{
"AwayTeamName":"Delhi Daredevils",
"AwayTeamShortName":"DD",
"Deadline_Date":"2014\/03\/15",
"Deadline_Time":"10:30:00",
"HomeTeamName":"Chennai Super Kings",
"HomeTeamShortName":"CSK",
"Id":"53138700440306238",
"MatchName":null,
"Match_Date":"15 Mar",
"Match_Time":"10:30",
"Type":0,
"Venue":"Chennai"
}
]
}