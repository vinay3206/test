;
require.config({
    baseUrl: 'Scripts',
    paths: {
        ember: 'Resources/Ember/emberLoader',
        embercore: 'Resources/Ember/ember-1.3.1.min',
        jquery: 'Resources/Jquery/jquery-1.10.2.min',
        /*jquery_migrate: 'Resources/jquery-migrate-1.2.1',*/
        handlebars: 'Resources/Handlerbars/handlebars-1.1.2.min',
        bootstrap: 'Resources/Bootstrap/bootstrap.min',
        bootstrap3: 'Resources/Bootstrap/bootstrap3.min',
        docs: 'Resources/Bootstrap/docs.min',
        urldecoder: 'Resources/UrlDecoder/jquery.urldecoder.min',
        validationmodel: 'Resources/EmberValidationModel/ember-validations.min',
        d3: 'Resources/D3/d3.min',
        moment: 'Resources/Moment/moment.min',

        avatar: 'Resources/Avatar/avatars.io.min',
        responsivetab: 'Resources/ResponsiveLibs/easyResponsiveTabs.min',
        customscrollbar: 'Resources/ResponsiveLibs/jquery.mCustomScrollbar.min',
        mousewheel: 'Resources/ResponsiveLibs/jquery.mousewheel',
        emberextensions: 'Resources/EmberExtensions/ember-extensions.min',




        //utils
        khelkundutils: 'Resources/Utils/khelkund-utils.min',
        ajaxcallstatushandler: 'Resources/Utils/AjaxCallsStatusHandlerMixin.min',
        eventregister: 'Resources/Utils/EventRegisterMixin.min',
        paginationcontrollermixin: 'Resources/Utils/PaginationControllerMixin.min',
        paginationviewmixin: 'Resources/Utils/PaginationViewMixin.min',
        logger: 'Resources/Utils/Logger.min',
        fbauth: 'Resources/Utils/Fb-auth.min',
        
        optionsmixin: 'Resources/Mixin/OptionsMixin.min',
        scrollmixin: 'Resources/Utils/ScrollMixin.min',
        authenticationmixin: "Resources/Utils/AuthenticationMixin.min",
        holder: 'Resources/Utils/holder.min',




        // bootstrap extensions
        bs_dropdownview: 'Resources/BSExtensions/DropDown/DropdownView.min',
        bs_buttondropdownview: 'Resources/BSExtensions/DropDown/DropdownButtonView.min',
        bs_splitbuttondropdownview: 'Resources/BSExtensions/DropDown/DropdownSplitButtonView.min',
        bs_modalpopupheaderview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpHeaderView.min',
        bs_modalpopupfooterview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpFooterView.min',
        bs_modalpopupbodyview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpBodyView.min',
        bs_modalpopupview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUPView.min',
        bs_messageview: 'Resources/BSExtensions/MessageView/MessageView.min',
        bs_floatingmessageview: 'Resources/BSExtensions/MessageView/FloatingMessageView.min',



        // app file

        khelkundapp: 'KhelkundApp.min',



        ////extensions
        //logger: 'customResources/Logger',

        ////settings
        //gamesettings: 'customResources/gameSettings',
        //dependencyresolver: 'customResources/DependencyResolver',
        //dependencies: 'customResources/dependencies',
        //rulesmixin: 'customResources/RulesMixin',

        //common modules
        userchallengemodel: 'CommonModules/UserChallenge.min',
        challenge: 'CommonModules/Challenge.min',
        tournamentmodel: 'CommonModules/Tournament.min',
        match: 'CommonModules/MatchModel.min',
        team: 'CommonModules/Team.min',
        player: 'CommonModules/Player.min',
        teamplayer: 'CommonModules/TeamPlayer.min',
        detailedpoints: 'CommonModules/DetailedPoints.min',
        userteam: 'CommonModules/UserTeam.min',
        personalinfo: 'CommonModules/UserPersonalInfo.min',
        accountinfo: 'CommonModules/UserAccountInfo.min',
        user: 'CommonModules/User.min',
        userstats: 'CommonModules/UserStats.min',
        userteamdetails: 'CommonModules/UserTeamDetails.min',
        pointsbreakup: 'CommonModules/PointsBreakup.min',
        scorecard: 'CommonModules/Scorecard.min',
        headercomponent: 'HeaderComponent.min',
        loginpopupview:'CommonModules/LoginPopUpView.min',
        timercomponent: 'CommonModules/TimerComponent.min',
        progressbarcomponent: 'CommonModules/ProgressBarComponent.min',



        //guidelinestepsview: 'views/GuidelineStepsView',
        //stepsconfig: 'customResources/StepsConfig'


        ////////
        /////local files //
        ////////

        //apps
        //userchallengesapp
        userchallengesconfig: 'Apps/UserChallenges/userChallengesConfig.min',
        //homeapp
        homeconfig: 'Apps/HomeApp/homeConfig.min',
        //editteamapp
        editconfig: 'Apps/EditTeam/editConfig.min',

        //components
        //tournaments
        tournamentscontroller: 'Components/Tournaments/TournamentsController.min',
        tournamentsview: 'Components/Tournaments/TournamentsView.min',
        matchitemview: 'Components/Tournaments/MatchItemView.min',

        //upcommingmatches
        upcommingmatchescontroller: 'Components/UpcommingMatches/UpcommingMatchesController.min',
        upcommingmatchesview: 'Components/UpcommingMatches/UpcommingMatchesView.min',
        upcommingmatchview: 'Components/UpcommingMatches/UpcommingMatchView.min',
        upcommingmatchcontroller: 'Components/UpcommingMatches/UpcommingMatchController.min',

        //activechallenges
        activechallengescontroller: 'Components/ActiveChallenges/ActiveChallengesController.min',
        activechallengesview: 'Components/ActiveChallenges/ActiveChallengesView.min',
        activechallengeview: 'Components/ActiveChallenges/ActiveChallengeView.min',
        activechallengecontroller: 'Components/ActiveChallenges/ActiveChallengeController.min',

        //playerlist
        playerslistcontroller: 'Components/PlayersList/PlayersListController.min',
        playerslistview: 'Components/PlayersList/PlayersListView.min',
        playercontroller: 'Components/PlayersList/PlayerController.min',
        playerview: 'Components/PlayersList/PlayerView.min',

        //userteam
        userteamcontroller: 'Components/UserTeam/UserTeamController.min',
        userteamview: 'Components/UserTeam/UserTeamView.min',
        teamplayerview: 'Components/UserTeam/TeamPlayerView.min',

        //userchallengestatus
        userchallengestatuscontroller: 'Components/UserChallengeStatus/UserChallengeStatusController.min',
        userchallengestatusview: 'Components/UserChallengeStatus/UserChallengeStatusView.min',

        //userchallengeteam
        userchallengeteamcontroller: 'Components/UserChallengeTeam/UserChallengeTeamController.min',
        userchallengeteamview: 'Components/UserChallengeTeam/UserChallengeTeamView.min',

        //scorecard
        scorecardcontroller: 'Components/Scorecard/ScorecardController.min',
        scorecardview: 'Components/Scorecard/ScorecardView.min',



        // Login
        login: 'Apps/Login/Login.min',
        landinglogin: 'Apps/Login/LandingLogin.min',
        loginroute: 'Apps/Login/Route/LoginRoute.min',
        logincontroller: 'Apps/Login/Controller/LoginController.min',
        loginview: 'Apps/Login/View/LoginView.min',
        loginmodel: 'Apps/Login/Model/LoginModel.min',


        //Match
        //match: 'Apps/Match/Match.min',
        matchroute: 'Apps/Match/Route/MatchRoute.min',
        matchcontroller: 'Apps/Match/Controller/MatchController.min',
        matchview: 'Apps/Match/View/MatchView.min',
        customchallenge: 'Apps/Match/Model/CustomChallengeModel.min',
        invitefriendsmodel: 'Apps/Match/Model/InvitationModel.min',
        joinChallengeModel: "Apps/Match/Model/JoinChallengeModel.min",
        seriesmatchselectorview: 'Apps/Match/View/SeriesMatchSelectorView.min',
        matchdetailview: 'Apps/Match/View/MatchDetailView.min',
        challengesview: 'Apps/Match/View/ChallengesView.min',
        displayuserteamview: 'Apps/Match/View/DisplayUserTeamView.min',
        userteamview: 'Components/UserTeam/UserTeamView.min',
        customchallengeview: "Apps/Match/View/CustomChallengeView.min",
        createcustomchallengeview: "Apps/Match/View/CreateCustomChallengeView.min",
        openchallenge: 'Apps/Match/View/OpenChallengeView.min',
        joinchallengepopupview: 'Apps/Match/View/JoinChallengePopView.min',

        //payment
        //payment: "Apps/Payment/Payment.min",

        //Referral
        //referral: "Apps/Referral/Referral.min",
        referralroute: 'Apps/Referral/Route/ReferralRoute.min',
        referralcontroller: 'Apps/Referral/Controller/ReferralController.min',
        referralview: 'Apps/Referral/View/ReferralView.min',
        referralmodel: 'Apps/Referral/Model/ReferralModel.min',
        referralinviteview: 'Apps/Referral/View/ReferralInviteView.min',

        //Registration
        //registration: "Apps/Registration/Registration.min",
        registrationroute: 'Apps/Registration/Route/RegistrationRoute.min',
        registrationcontroller: 'Apps/Registration/Controller/RegistrationController.min',
        registrationview: 'Apps/Registration/View/RegistrationView.min',
        registrationmodel: 'Apps/Registration/Model/RegistrationModel.min',

        //ResetPassword

        //resetpassword: "Apps/Resetpassword/Resetpassword.min",
        resetpasswordroute: 'Apps/Resetpassword/Route/ResetpasswordRoute.min',
        resetpasswordcontroller: 'Apps/Resetpassword/Controller/ResetpasswordController.min',
        resetpasswordmodel: 'Apps/Resetpassword/Model/ResetpasswordModel.min',

        //User-profile

        //userprofile: "Apps/UserProfile/UserProfile.min",
        userprofileroute: 'Apps/UserProfile/Route/UserProfileRoute.min',
        userprofilecontroller: 'Apps/UserProfile/Controller/UserProfileController.min',
        userprofileview: 'Apps/UserProfile/View/UserProfileView.min',
        withdrawamountmodel: 'Apps/UserProfile/Model/WithDrawModel.min',
        addamountmodel: 'Apps/UserProfile/Model/AddAmountModel.min',
        accountdetailsview: 'Apps/UserProfile/View/AccountDetailsView.min',
        addwithdrawview: 'Apps/UserProfile/View/AddWithdrawView.min',
        personalinfoview: 'Apps/UserProfile/View/PersonalInfoView.min',
        statisticsview: 'Apps/UserProfile/View/StatisticsView.min',
        accountdetailseditview: 'Apps/UserProfile/View/AccountsEditView.min',
        accountsview: 'Apps/UserProfile/View/AccountsView.min',
        personaleditview: 'Apps/UserProfile/View/PersonalInfoEditView.min',
        personalview: 'Apps/UserProfile/View/PersonalView.min',
        addUserTeamView: 'Apps/UserProfile/View/AddUserTeamView.min'



        //Verification
        //verification: "Apps/Verification/Verification.min"










    },

    waitSeconds: 0,
    shim: {
        /*'jquery_migrate': ['jquery'],*/
        'embercore': ['jquery', 'handlebars'],
        'bootstrap': ['jquery'],
        'urldecoder': ['jquery'],
        'logger': ['jquery'],
        'responsivetab': ['jquery'],
        'mousewheel': ['jquery'],
        'customscrollbar': ['jquery', 'mousewheel'],
        'docs': ['jquery', 'bootstrap3'],

        'emberextensions': ['embercore'],
        'validationmodel': {
            deps: ['ember'],
            exports: 'validationmodel',
            init: function () {
                return Em.Object.extend(Ember.Validations);

            }

        }


    },
    callback: function () {
        window.logerror = function () {
            var i;
            var messages = [];
            for (i = 0; i < arguments.length; i++) {
                messages.push(arguments[i]);
            }
            console.error.apply(console, messages);

        };
        require(['jquery', 'bootstrap', 'khelkundutils', 'urldecoder', 'logger', 'validationmodel', 'responsivetab', 'customscrollbar', 'fbauth', 'moment'], function () {
            require(['khelkundapp']);
        });

    }
});

/*require(['teamapp'],
    function (teamapp) {
        //Application name should start with capital letter.
        var TeamApp = teamapp.create({
            
        });
        //to enable the custom loggings.
        window.loggingEnabled = true;
    });*/