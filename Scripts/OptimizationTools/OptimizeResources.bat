::Syntax:
::node <path-to-optimizer> -o <path-to-config-file>
::example- node Resources/Tools/r.js -o Resources/Tools/build.js
::here r.js is the require js optimizer that we are using and the next path is the
::path of the config file which is to be used for optimization for specific module

::includes all the library files i.e. plugins used globally
node r.js -o ../build.js