﻿;
define(['bs_modalpopupview', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview', 'khelkundutils', 'ajaxcallstatushandler', 'khelkundutils'],
    function (bs_modalpopupview, bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview, khelkundutils, ajaxcallstatushandler, khelkundutils) {




        var loginPopUpView = bs_modalpopupview.extend(ajaxcallstatushandler, {


            init: function () {
                this._super();
                this.set('handler.parent', this);

            },

            ajaxCallsToRegister: ['saveTeamName'],

            classNames: ['trans_background', 'match-bdr1'],

            headerView: bs_modalpopupheaderview.extend({
                template: null,
                classNames: ['hide']

            }),


            bodyView: bs_modalpopupbodyview.extend({

                //template: null,


                templateName: 'loginPopUp',
                classNames: ['modal-body'],

               



            }),

            footerView: bs_modalpopupfooterview.extend({

                template: null,
                classNames: ['hide']
            }),

            didInserElement: function () {
                this._super();
                //this.show();

            },

            handler: {
                show: function () {
                    this.parent.popup();
                },
                hide: function () {
                    this.parent.hide();

                }
            },



          

            actions: {

                close: function () {
                    this.hide();


                },

                gotoLogin: function () {
                    this.hide();

                    // need to change it as this is a hack to change the route. need to do it properly
                    window.location.hash = "login";
                    

                }

                

            },

            


        });








        return loginPopUpView;
    });