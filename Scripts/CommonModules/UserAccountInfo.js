﻿define(['ember', 'validationmodel'], function (ember, validationmodel) {
    var userAccountInfoModel = validationmodel.extend({
        //members
        ifscCode: '',
      
        accountNumber: '',


        validations: {

          

            accountNumber: {
                presence: {
                    controlName: 'Account Number',
                },

                numericality:{
                    controlName:'Account Number',

                },


            },


            ifscCode: {
                presence: {
                    controlName: 'IFSC Code',
                },
                
            },

            

           

            

        }


    });






    return userAccountInfoModel;


});