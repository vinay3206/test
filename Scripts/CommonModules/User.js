define(['ember', 'khelkundutils', 'personalinfo', 'accountinfo'], function (ember, khelkundutils, personalinfo, accountinfo) {
    var UserModel = Em.Object.extend({
        //members
        id: '',
        username: '',
        amount:'', // move it to account info later
        personalInfo: null, // model of personalInfo.js,
        personalEditInfo:null, // model of personalInfo.js
        accountInfo: null, // model of accountInfo.js
        stats:null, // modelo of userStats.js
        

    });

    UserModel.reopen({
        updateUserProfile: function () {
            return this._updateUserProfile();

        },


        _updateUserProfile: function () {
            var updatePromise = null;
            var requestObj = {
                url: 'Services/ProfileService.svc/SaveProfile',
                data: this.toServiceModel()
            };
            updatePromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                khelkundutils.ajax.post(requestObj,
                         //success handler
                         $.proxy(function (apiResult) {
                             if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                 var user = UserModel.createProfileToLocalModel(apiResult);


                                 resolve({
                                     data: user,
                                     Status: apiResult.Status

                                 });
                             }
                             else {
                                 reject(apiResult);
                             }
                         }, this),
                         //failure handler
                         $.proxy(function (err) {
                             //registration failed
                             reject(err);
                         }, this)
                     );

            }));
            return updatePromise;


        },


        toServiceModel: function () {
            var serivceObj = {
                FirstName: this.get('personalEditInfo.firstName'),
                LastName: this.get('personalEditInfo.lastName'),
                ImageUrl: this.get('personalEditInfo.avatar'),
                PhoneNumber: this.get('personalEditInfo.phoneNumber'),
                AccountNumber: this.get('accountEditInfo.accountNumber'),
                IFSC_Code: this.get('accountEditInfo.ifscCode')

            };
            return serivceObj;
        }

    });

    UserModel.reopenClass({
        apiUrl: 'Services/CricManagerService.svc/Series',

        userProfileApiUrl: 'Services/ProfileService.svc/GetProfile',
        loggedInUserApiUrl: 'Services/LoginService.svc/LoggedIn',

        updateProfileApiUrl: 'Services/ProfileService.svc/SaveProfile',

       

        
        createLocalModel: function (user) {
            return UserModel.create({
                id: user.UserId,
                username: user.UserName,
                amount:user.Amount,
                personalInfo: personalinfo.create({
                    firstName: user.FirstName,
                    lastName: user.LastName,
                    teamName: user.TeamName,
                    avatar: user.ProfileImageUrl,
                    phoneNumber:user.PhoneNumber
                }),

                

            });
        },

        createProfileToLocalModel: function (userProfile) {
            return UserModel.create({
                
                amount: userProfile.PersonalInformation.Amount,
               

                personalInfo: personalinfo.create({
                    firstName: userProfile.PersonalInformation.FirstName,
                    lastName: userProfile.PersonalInformation.LastName,
                    teamName: userProfile.PersonalInformation.TeamName,
                    emailId: userProfile.PersonalInformation.Email,
                    state: userProfile.PersonalInformation.State,
                    avatar: userProfile.PersonalInformation.ProfileImageUrl,
                    phoneNumber:userProfile.PersonalInformation.PhoneNumber

                }),
                personalEditInfo: personalinfo.create({
                    firstName: userProfile.PersonalInformation.FirstName,
                    lastName: userProfile.PersonalInformation.LastName,
                    teamName: userProfile.PersonalInformation.TeamName,
                    emailId: userProfile.PersonalInformation.Email,
                    state: userProfile.PersonalInformation.State,
                    avatar: userProfile.PersonalInformation.ProfileImageUrl,
                    phoneNumber: userProfile.PersonalInformation.PhoneNumber

                }),

                accountInfo: accountinfo.create({
                   
                    accountNumber: userProfile.AccountInformation.AccountNumber,
                    ifscCode: userProfile.AccountInformation.IFSCCode

                }),

                accountEditInfo: accountinfo.create({
                   
                    accountNumber: userProfile.AccountInformation.AccountNumber,
                    ifscCode: userProfile.AccountInformation.IFSCCode

                }),


            });
        },
        
        

        fetchLoggedInUser: function (forceFreshCall) {
            return this._fetchLoggedInUser(forceFreshCall);
        },

        fetchUserProfile: function (forceFreshCall) {

            return this._fetchUserProfile(forceFreshCall);
        },

       



        _fetchLoggedInUserPromise:null,
        _fetchLoggedInUser: function (forceFreshCall) {
            var fetchPromise = null;
            if (this._fetchLoggedInUserPromise && !forceFreshCall) {
                return this._fetchLoggedInUserPromise;
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.loggedInUserApiUrl
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        var user = this.createLocalModel(apiResponse.User);
                        resolve({
                            data: user,
                            Status: apiResponse.Status

                        });
                    }
                    else {
                        reject(apiResponse)
                    }
                    
                    
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            //save promise for next or future use
            this._fetchLoggedInUserPromise = fetchPromise;

            //return promise
            return fetchPromise;

        },


       

        _fetchUserProfilePromise: null,
        _fetchUserProfile: function (forceFreshCall) {
            var fetchPromise = null;
            if (this._fetchUserProfilePromise && !forceFreshCall) {
                return this._fetchUserProfilePromise;
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.userProfileApiUrl
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse


                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        var user = this.createProfileToLocalModel(apiResponse);


                        resolve({
                            data: user,
                            Status: apiResponse.Status

                        });
                    }
                    else {
                        reject(apiResponse);
                    }
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            //save promise for next or future use
            this._fetchUserProfilePromise = fetchPromise;

            //return promise
            return fetchPromise;

        },

        createDummyProfile: function () {
            return UserModel.create({
                amount: 200,
                personalInfo: personalinfo.create({
                    firstName: "vinay",
                    lastName: "pandey",
                    teamName: "Tuskers11",
                    emailId: "vinay21feb@gmail.com",
                    state:  "Bihar"

                }),
                personalEditInfo: personalinfo.create({
                    firstName: "vinay",
                    lastName: "pandey",
                    teamName: "Tuskers11",
                    emailId: "vinay21feb@gmail.com",
                    state: "Bihar"

                }),

            

                accountInfo: accountinfo.create({
                    
                }),

                accountEditInfo: accountinfo.create({

                }),
                history:null,

            });

        },

       
       


    });

    //also register this model to store for global access.

    return UserModel;
});