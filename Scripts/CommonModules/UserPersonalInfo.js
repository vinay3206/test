﻿define(['ember', 'validationmodel'], function (ember, validationmodel) {
    var userPersonalInfoModel = validationmodel.extend({
        //members
        emailId: '',
        firstName: '',
        lastName: '',
        teamName: '',
        avatar: 'Css/img/default_profile_image.jpg',
        phoneNumber: '',
        state:'',


        validations: {
            

            firstName: {
                presence: {
                    controlName: 'First Name',
                },


            },

            lastName: {
                presence: {
                    controlName: 'Last Name',
                },

            },

            phoneNumber: {
                presence: {
                    controlName: 'Mobile Number',
                },

               
                numericality: {
                    controlName: 'Mobile Number',
                },

                length: {
                    controlName: 'Mobile Number',
                    is: 10,
                }
            }

        }
        

    });


  
        return userPersonalInfoModel;


});