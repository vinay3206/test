﻿
require.config({
    paths: {


    }
});


define(['ajaxcallstatushandler', 'khelkundutils', 'd3'],


    function (ajaxcallstatushandler, khelkundutils, d3) {

        return Em.Component.extend(Khelkund.Mixins.AjaxCallsHandler, {


            width: 960,
            height : 500,
            radius : Math.min(width, height) / 2,
            
            endDate: new Date().toString(),

            deadLine: function () {

                return new Date(this.get('endDate')).getTime();
            }.property('deadLine').readOnly(),
            

       
            total: 100,  // total value can be total budget allowed in our case
            progress: 50, // progress value to show on progress bar


            makeProgressBar: function () {
                if (!(this.get('state') === 'inDOM')) {
                    return;
                }
                var width = this.get('width'),
                height = this.get('height'),
                twoPi = 2 * Math.PI,
                progress = this.get('progress'),
                total = this.get('total'),
                formatPercent = d3.format(".0%");

                var arc = d3.svg.arc()
                .startAngle(0)
                .innerRadius(180)
                .outerRadius(240);

                var svg = d3.select('#' + this.elementId).append("svg")
                    .attr("width", width)
                    .attr("height", height)
                  .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var meter = svg.append("g")
                    .attr("class", "progress-meter");

                meter.append("path")
                    .attr("class", "background")
                    .attr("d", arc.endAngle(twoPi));

                var foreground = meter.append("path")
                    .attr("class", "foreground");

                var text = meter.append("text")
                    .attr("text-anchor", "middle")
                    .attr("dy", ".35em");

                var angle = (this.get('progress') / this.get('total'));

                foreground.attr("d", arc.endAngle(angle * twoPi));
                text.text(this.get('progress'));





            }.observes('total').on('didInsertElement')



        });




    });