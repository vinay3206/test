;
define(['ember', 'challenge', 'khelkundutils'], function (ember, challenge, khelkundutils) {
	var UserChallenge = challenge.extend({
		rank: 0,
		lastRank: 0,
        points: 0,
        /**
         * Overriding base object field
         * @type {String}
         */
        status: null,
        /**
         * Computed property to find if user rank has improved or he has fallen behind.
         * @return {Boolean} `true` if rank is same or less than lastRank. `false` otherwise.
         */
		isProgressing: function () {
			return ((this.get('lastRank') - this.get('rank')) >= 0);
		}.property('rank', 'lastRank').readOnly()
	});

	

	UserChallenge.reopenClass({

        apiUrl: '',
        joinedChallengeApiUrl: 'Services/ChallengeService.svc/GetJoinedChallenges',

        /**
         * Basic function to delegate tasks to respective functions based on 
         * input params provided.
         * @param  {String} userId         Id of the user for whom to fetch challenges
         * @param  {String} challengeId    challenge_id to fetch specific challenge.
         * @param  {Boolean} forceFreshCall Boolean value to decide wether to make call to store or service.
         *                                  True value makes fresh call to service.
         * @return {RSVP.Promise}                Promise to keep track of call status.
         */
		fetch: function (userId, challengeId, forceFreshCall, apiUrl) {
			if(challengeId === undefined || challengeId ===  null) {
				return this._fetchChallengeList(userId, forceFreshCall, apiUrl);
			} else {
				return this._fetchChallengeById(userId, challengeId, forceFreshCall, apiUrl);
			}
		},
        /**
         * Finds list of joined challenges by the user.
         * @param  {String/Number} [match_id]        UserId for which joined challenges are to be found.
         *                                          Optional parameter because joined challenges are searched
         *                                          for the logged in user thus this parameter is not required.
         *                                          If not providing then null should be provided
         *                                          as a parameter in its place.
         * @param  {Boolean} forceFreshCall Controls wether the api call should be made to server or cache store.
         * @return {RSVP.Promise}                Promise to control the loaders or other async handling.
         */
        fetchJoinedChallenges: function (match_id,forceFreshCall) {
            return this._fetchJoinedChallenges(match_id, forceFreshCall);
        },
        /**
         * Searches for a list of challengers for the provided match_id and challenge_id.
         * @param  {String/Number} [user_id]        UserId for which challenge details are to be found.
         *                                          Optional parameter because challenge details are to be found for
         *                                          logged in user. If not providing this parameter null should
         *                                          be provided as a parameter in its place.
         * @param  {String/Number} match_id       MatchId for which challengers are to be found.
         * @param  {String/Number} challenge_id   ChallengeId for which challengers are to be found.
         * @param  {Boolean} forceFreshCall Controls wether the api call should be made to server or cache store.
         * @return {RSVP.Promise}                Promise to control the loaders or other async handling.
         */
        fetchChallengers: function (user_id, match_id, challenge_id, apiUrl, forceFreshCall) {

        },
        /**
         * Fetches challenge details for the logged in user for provided match_id and challenge_id.
         * @param {Object} inputParams Object containing the filter values.
         * @param  {String/Number} [inputParams.user_id]        UserId for which challenge details are to be found.
         *                                          Optional parameter because challenge details are to be found for
         *                                          logged in user. If not providing this parameter null should
         *                                          be provided as a parameter in its place.
         * @param  {String/Number} inputParams.match_id       MatchId for which challenge details are to be found.
         * @param  {String/Number} inputParams.challenge_id   ChallengeId for which challenge details are to be found.
         * @param {String} [inputParams.apiUrl] Url to use for making an ajax call. Optional because if no url
         *                                      is provided then default apiUrl in the object is used(if any).
         * @param  {Boolean} forceFreshCall Controls wether the api call should be made to server or cache store.
         * @return {RSVP.Promise}                Promise to control the loaders or other async handling.
         */
        fetchChallenge: function (inputParams, forceFreshCall) {
            var user_id = inputParams.user_id, match_id = inputParams.match_id, challenge_id = inputParams.challenge_id,
                apiUrl = inputParams.apiUrl || this.apiUrl;
            var storedPromise = null,
            //object which uniquely identifies store
            storeIdentifier = {
                method: 'fetchChallenge',
                user_id: user_id,
                match_id: match_id,
                challenge_id: challenge_id,
                apiUrl: apiUrl
            };
            //find current criteria in store.
            if(this._store && !forceFreshCall) {
                storedPromise = this._findInStore(storeIdentifier);
                
                if(storedPromise) {
                    //return the stored promise if found.
                    return storedPromise.promise;
                }
            }
            
            var fetchPromise = null;
            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                khelkundutils.ajax.get({
                    url: apiUrl.replace('{user_id}', user_id).replace('{match_id}', match_id)
                                .replace('{challenge_id}', challenge_id)
                }, $.proxy(function (apiResponse) {
                    if(khelkundutils.ajax.validateApiResult(apiResponse)) {
                        //generate client side mode with apiResponse
                        var userChallenge = this.createLocalModel(apiResponse.Challenge);

                        resolve({
                            data: userChallenge,
                            Status: apiResponse.Status
                        });
                    } else {
                        //also remove it from the store as failed calls should not be cached
                        var storeItem = this._findInStore(storeIdentifier);
                        if(storeItem) {
                            this._store.removeObject(storeItem);
                        }
                        reject(apiResponse);
                    }
                }, this), $.proxy(function (err) {
                    //also remove it from the store as failed calls should not be cached
                    var storeItem = this._findInStore(storeIdentifier);
                    if(storeItem) {
                        this._store.removeObject(storeItem);
                    }
                    //network failure.
                    reject(err);
                    console.error('Network failure while fetching challenge info for user.');
                }, this));
            }, this));

            if(!this._store) {
                //initialize store for the first call from this module.
                this._store = [];
            }
            //if its a forced call then we need to remove the current save promise first
            if(forceFreshCall === true) {
                storedPromise = this._findInStore(storeIdentifier);

                if(storedPromise){
                    this._store.removeObject(storedPromise);
                }
            }
            this._store.pushObject({
                method: 'fetchChallenge',
                user_id: user_id,
                match_id: match_id,
                challenge_id: challenge_id,
                promise: fetchPromise,
                apiUrl: apiUrl
            });
            return fetchPromise;
        },
        /**
         * @private
         * Store to be used for caching api calls.
         * @type {Array}
         * @example
         *         //sample for each object.
         *         var sampleStoreObj = {
         *             method: 'methodName', //required since these methods have same signature.
         *             user_id: user_id,
         *             match_id: match_id,
         *             challenge_id: challenge_id,
         *             promise: RSVP.Promise
         *         };
         */
        _store: null,
        /**
         * @private
         * Helper function for store. Used to find weather the given params item is
         * present in store or not. If found returns the found store item.
         * @param  {Object} params Contains all the required params used to uniquely define
         *                         a store item.
         *                         @example
         *                             //sample object with full params
         *                                 {
         *                                     method: 'methodName',
         *                                     user_id: 'user_id',
         *                                     match_id: 'match_id',
         *                                     challenge_id: 'challenge_id',
         *                                     apiUrl: apiUrl
         *                                 }
         * @return {StoreItem}        Store item if found else null/undefined.
         */
        _findInStore: function (params) {
            var storeItem = this._store.find(function (storeItem, index) {
                if(storeItem.method !== params.method) {
                    return false;
                }
                if(storeItem.user_id !== params.user_id) {
                    return false;
                }
                if(storeItem.match_id !== params.match_id) {
                    return false;
                }
                if(storeItem.challenge_id !== params.challenge_id) {
                    return false;
                }
                if(storeItem.apiUrl !== params.apiUrl){
                    return false;
                }
                //if all above tests are passed then we have found our item
                return true;
            });
            return storeItem;
        },
        /**
         * @private
         * Generic function to fetch object. It considers both the store and api calls. i.e. if found in store
         * uses the store else makes an api call.
         * @param  {Object} reqObj         Object which contains the url and data to be posted if callType is post/put.
         * @param {String} reqObj.url Url to use make api calls
         * @param {Object} reqObj.data Data to use to post/put it to service.
         * @param  {Function} parser         Callback function to parse the apiResponse if call is success.
         * @param  {Boolean} forceFreshCall Controls wether the api call should be made to server or cache store.
         * @param  {String} [callType='get']       Determines the call type. Optional parameter.
         * @return {RSVP.Promise}                Promise to control the loaders or other async handling.
         */
        _makeCall: function (reqObj, parser, forceFreshCall, callType) {

        },

        /*fetch: function (inputParams, forceFreshCall) {
            var match_id, challenge_id, user_id;
            if(typeof(inputParams) === 'object') {
                match_id = inputParams.match_id;
                challenge_id = inputParams.challenge_id;
                user_id = inputParams.user_id;
                apiUrl = inputParams.apiUrl;
            }

            //1. if challenge_id is provided then _fetchChallengeById for the logged in user.
            //2. if challenge_id is not provided then if user_id is provided then fetch
            //  challenge for that user.
            

        }, */


        _fetchJoinedChallengesStore:null,
        _fetchJoinedChallengesPromise:null,
        _fetchJoinedChallenges: function (matchId, forceFreshCall) {

            if (this._fetchJoinedChallengesStore && !forceFreshCall) {
                var storedPromise = null;
                storedPromise = this._fetchJoinedChallengesStore.find(function (promiseItem, index) {
                    if (matchId === promiseItem.match_id) {
                        return true;
                    }
                    return false;
                }, this);
                if (storedPromise) {
                    return storedPromise.promise;
                }
            }


            var joinedChallengePromise = null;
            
            var url = this.joinedChallengeApiUrl;
            joinedChallengePromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: url + '/' + matchId
                },$.proxy( function (apiResponse) {
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        var challenges = [];
                        if (apiResponse.Challenges) {
                            $.each(apiResponse.Challenges, $.proxy(function (index, val) {
                                challenges.pushObject(this.createLocalModel(val));
                            }, this));
                        }
                        resolve({
                            data: challenges,
                            Status:apiResponse.Status
                            });
                    }
                    else {
                        reject(apiResponse);
                    }
                },this), function (err) {
                    //network error.
                    reject(err);
                });
            },this));

            if (!this._fetchJoinedChallengesStore) {
                this._fetchJoinedChallengesStore = [];
            }

            //if forceFreshCall is true then find if this id is already present then remove it.
            if (forceFreshCall === true) {
                var savedPromise = this._fetchJoinedChallengesStore.findBy('match_id', matchId);
                if (savedPromise) {
                    this._fetchJoinedChallengesStore.removeObject(savedPromise);
                }
            }

            //save promise for caching of next call
            this._fetchJoinedChallengesStore.pushObject({
                match_id: matchId,
                promise: joinedChallengePromise
            });

           

            //return promise
            return joinedChallengePromise;

        },

		//private function
		_fetchChallengeList: function (userId, forceFreshCall, apiUrl) {
			var fetchPromise = null, storedPromise = null, user_id = null, match_id = null, challenge_id = null;
            apiUrl = apiUrl || this.apiUrl;

            var storeIdentifier = {
                method: '_fetchChallengeList',
                user_id: user_id,
                match_id: match_id,
                challenge_id: challenge_id,
                apiUrl: apiUrl
            };

            if(this._store && !forceFreshCall) {
                storedPromise = this._findInStore(storeIdentifier);

                if(storedPromise) {
                    //return stored promise if found.
                    return storedPromise.promise;
                }
            }            

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: apiUrl
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        var challenges = [];
                        if(Em.isArray(apiResponse.Challenges)){
                            apiResponse.Challenges.forEach(function (challengeItem) {
                        /*if(Em.isArray(apiResponse.ChallengeDetails)){
                            apiResponse.ChallengeDetails.forEach(function (challengeItem) {*/
                                challenges.pushObject(this.createLocalModel(challengeItem));
                            }, this);
                        }

                        resolve({
                            data: challenges,
                            Status: apiResponse.Status
                        });
                    } else {
                        //also remove it from the store as failed calls should not be cached
                        var storeItem = this._findInStore(storeIdentifier);
                        if(storeItem) {
                            this._store.removeObject(storeItem);
                        }
                        reject(apiResponse);
                    }
                    
                }, this), $.proxy(function (err) {
                    //also remove it from the store as failed calls should not be cached
                    var storeItem = this._findInStore(storeIdentifier);
                    if(storeItem) {
                        this._store.removeObject(storeItem);
                    }
                    //network error.
                    reject(err);
                }, this));
            }, this));

            if(!this._store) {
                //initialize store for the first call from this module.
                this._store = [];
            }
            //if its a forced call then we need to remove the current save promise first
            if(forceFreshCall === true) {
                storedPromise = this._findInStore(storeIdentifier);
                if(storedPromise){
                    //if this promise is already stored remove it.
                    this._store.removeObject(storedPromise);
                }
            }
            this._store.pushObject({
                method: '_fetchChallengeList',
                user_id: user_id,
                match_id: match_id,
                challenge_id: challenge_id,
                promise: fetchPromise,
                apiUrl: apiUrl
            });            

            //return promise
            return fetchPromise;
		},

		_fetchChallengePromise: null,
        //private function
        _fetchChallengeById:  function (challengeId, forceFreshCall, apiUrl) {
            var fetchPromise = null;
            if(this._fetchChallengePromise && !forceFreshCall) {
                return this._fetchChallengePromise;
            }
            apiUrl = apiUrl || this.apiUrl;
            fetchPromise = new Em.RSVP.Promise(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: apiUrl + '/' + challengeId
                }, function (apiResponse) {
                    //validate apiResponse
                    resolve(apiResponse);
                }, function (err) {
                    //network error.
                    reject(err);
                });
            });

            //save promise for next or future use
            this._fetchChallengePromise = fetchPromise;

            //return promise
            return fetchPromise;
        },

        createLocalModel: function (challenge) {
            var userChallenge = UserChallenge.create(this._super(challenge), {
                rank: Em.get(challenge, 'CurrentUserTeamDetails.CurrentRank'),
                lastRank: Em.get(challenge, 'CurrentUserTeamDetails.PreviousRank'),
                points: Em.get(challenge, 'CurrentUserTeamDetails.Points')
            });
            userChallenge.set('status', challenge.MatchChallengeStatus);
            return userChallenge;
        },
	});

	return UserChallenge;
});