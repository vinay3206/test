;
/**
 * @module pointsbreakup
 */
define(['ember'], function(ember) {
	var PointsBreakup = Em.Object.extend({

	});

	PointsBreakup.reopenClass({
                createLocalModel: function (statistics) {
                        return PointsBreakup.create({
                                //unused property
                                //ballsFaced: statistics.BallsFaced,
                                fours: statistics.FoursPoints,
                                sixes: statistics.SixesPoints,
                                strikeRate: statistics.StrikeRatePoints,
                                battingBonus: statistics.BatsmenBonusPoints,
                                //unused property
                                //oversBowled: statistics.OversBowled,
                                wickets: statistics.WicketsPoints,
                                maidens: statistics.MaidensPoints,
                                //unused property
                                //runsGiven: statistics.RunsGiven,
                                economyRate: statistics.EconomyRatePoints,
                                bowlingBonus: statistics.BowlerBonusPoints,
                                runsScored: statistics.RunsScoredPoints,
                                catches: statistics.CatchesPoints,
                                runOuts: statistics.RunOutsPoints,
                                
                                //unused property
                                //outOnDuck: statistics.OutOnDuck,
                                //unused property
                                //stumpings: statistics.Stumpings,
                                manOfTheMatch: statistics.ManOfTheMatchPoints,
                                totalPoints: statistics.TotalPoints
                        });
                }
        });

	return PointsBreakup;
});