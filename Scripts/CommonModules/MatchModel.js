﻿;
/**
 * @module match
 */
define(['ember', 'khelkundutils', 'team'], function (ember, khelkundutils, team) {
    var MatchModel =  Em.Object.extend({
        //members
        id:'',
        date: '',
        deadline: '',
        homeTeam: null,//model of Team.js
        awayTeam: null,//model of Team.js
        venue: null,//address model,
        status: "queued", //"inProgress", "closed", "abandoned"
        /**
         * Object containing small info on series
         * @type {Object}
         * @example
         * //sample seriesInfo object
         * seriesInfo: {
         *     id: 'series id',
         *     name: 'series name'
         * }
         */
        //TODO: it should be a model of tournaments model but because of its dependency
        //on match(this) model it creates circular dependency thus unable to achieve it.
        seriesInfo: null,


        //translators

       /* toLocalModel: function (serviceModel) {
            return {
                id: 100,
                date: new Date('03/06/14'),
                deadLine: new Date(new Date('03/06/14').getTime() - (1000 * 60 * 60)),
                name: 'IND-VS-SA',
                homeTeamName: 'IND',
                awayTeamName: 'SA',
                venue:'Bangalore'
            };
        }*/
    });

    MatchModel.reopenClass({
        apiUrl: 'Services/CricManagerService.svc/Series',

        /*
        @param: string/object. String= matchId to find for, object= filter to pass for filtering calls.
        @param: boolean. Decide weather to make a fresh call or return previous stored promise if already present.
        */
        fetch: function(matchId, forceFreshCall){
            if(matchId === undefined || matchId === null) {
                return this._fetchMatchesList();
            } else {
                //todo: also add support for filtering also.
                var inputParams = {}, apiUrl = this.apiUrl + '/{match_id}';
                if(typeof(matchId) === 'object') {
                    inputParams = matchId;
                    matchId = inputParams.match_id;
                    apiUrl = inputParams.apiUrl || apiUrl;
                }
                return this._fetchMatchById(matchId, apiUrl, forceFreshCall);
            }
        },

        _fetchListPromise: null,
        //private function
        _fetchMatchesList: function (forceFreshCall) {
            
            var fetchPromise = null;
            if(this._fetchListPromise && !forceFreshCall) {
                return this._fetchListPromise;
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.apiUrl
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse
                    if(khelkundutils.ajax.validateApiResult(apiResponse)){
                        var matches = [];
                        //correct upcomming spelling
                        if(apiResponse.UpComingMatches){
                            apiResponse.UpComingMatches.forEach($.proxy(function (matchItem, index, arr) {
                                matches.pushObject(this.createLocalModel(matchItem));
                            }, this));
                        }
                        resolve({
                            data: matches,
                            Status: apiResponse.Status
                        });
                    } else {
                        reject(apiResponse);
                    }
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            //save promise for next or future use
            this._fetchListPromise = fetchPromise;

            //return promise
            return fetchPromise;
        },
        createLocalModel: function (match) {
            return MatchModel.create({
                id: match.Id,
                //real implementation
                //date: new Date(match.Deadline_Date),
                //temp implementation
                date: match.Match_Date,
                deadline: match.Deadline_Date,
                homeTeam: team.create({
                    name: match.HomeTeamName,
                    shortName: match.HomeTeamShortName,
                    iconUrl:match.HomeTeamImageUrl
                }),
                awayTeam: team.create({
                    name: match.AwayTeamName,
                    shortName: match.AwayTeamShortName,
                    iconUrl: match.AwayTeamImageUrl,
                }),
                venue: match.Venue,
                seriesInfo: {
                    id: match.SeriesId,
                    name: match.SeriesName
                }
            });
        },
        /**
         * @private
         * This property will be caching the apiresults responses with
         * some basic parameters as keys to seperate between different calls.
         * @type {Array}
         * @example
         * //sample store object.
         * _storeItem : {
         *     match_id: 'some long string', //match id if present else null
         *     method: 'methodName',    //name of method in which call is made.
         *     apiUrl: 'apiUrl',    //url at which call was made.
         *     promise: cachedPromise   //cached promise which will be returned if above
         *                             //three properties mathces for another call.
         * }
         */
        _store: null,
        /**
         * @private
         * Helper function to search items in array based on provided
         * params value. Params object is checked for 'match_id', 'method'
         * and 'apiUrl'. If all three properties matches with the stored value in
         * array then that item is returned.
         * @param  {Object} params Object whose properties are matched with stored
         *                         promise.
         * @return {Object}        StoreItem is retuned if params satishfies
         *                                   required conditions else null.
         */
        _findInStore: function (params) {
            params = params || {};
            var foundItem = this._store.find(function (storeItem, index) {
                if(storeItem.method !== params.method) {
                    return false;
                }
                if(storeItem.match_id !== params.match_id) {
                    return false;
                }
                if(storeItem.apiUrl.toLowerCase() !== params.apiUrl.toLowerCase()) {
                    return false;
                }
                //if all above are not true then we have found our item
                return true;
            });
            return foundItem;
        },
        /**
         * @private
         * Fetches the match details as per provided matchId.
         * @param  {String} matchId        Match id for which details are to be fetched.
         * @param  {String} forceFreshCall url on which api call will be made.
         * @return {Em.RSVP.Promise}                Promise object which can be used to
         *                                                  handle/show loaders while the
         *                                                  call is in progress.
         */
        _fetchMatchById:  function (matchId, apiUrl, forceFreshCall) {
            var fetchPromise = null, storedPromise = null;
            /**
             * Identifier object to search inside the store.
             * @type {Object}
             */
            var storeIdentifier = {
                match_id: matchId,
                apiUrl: apiUrl,
                method: '_fetchMatchById'
            };

            if(this._store && !forceFreshCall) {
                storedPromise = this._findInStore(storeIdentifier);
                if(storedPromise) {
                    //return stored promise if found
                    return storedPromise.promise;
                }
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: apiUrl.replace('{match_id}', matchId)
                }, $.proxy(function (apiResponse) {
                    if(khelkundutils.ajax.validateApiResult(apiResponse)){
                        //validate apiResponse                        
                        resolve({
                            data: this.createLocalModel(apiResponse),
                            Status: apiResponse.Status
                        });
                    } else {
                        //remove from _store if call is failed
                        storedPromise = this._findInStore(storeIdentifier);
                        if(storedPromise) {
                            this._store.removeObject(storedPromise);
                        }
                        reject(apiResponse);
                    }
                }, this), $.proxy(function (err) {
                    //remove from _store if call is failed
                    storedPromise = this._findInStore(storeIdentifier);
                    if(storedPromise) {
                        this._store.removeObject(storedPromise);
                    }
                    //network error.
                    reject(err);
                }, this));
            }, this));

            if(!this._store) {
                //create store if not found
                this._store = [];
            }
            if(forceFreshCall === true) {
                //remove promise first if call is forced and stored promise is 
                //present
                storedPromise = this._findInStore(storeIdentifier);
                if(storedPromise) {
                    this._store.removeObject(storedPromise);
                }
            }

            //save promise for later use of caching.
            this._store.pushObject({
                match_id: matchId,
                method: '_fetchMatchById',
                apiUrl: apiUrl,
                promise: fetchPromise
            });           

            //return promise
            return fetchPromise;
        }
    });

    MatchModel.reopen({
        displayName: function () {
            return this.get('homeTeam.shortName') + ' vs ' + this.get('awayTeam.shortName');
        }.property('homeTeam', 'awayTeam').readOnly()
    });

    //also register this model to store for global access.

    return MatchModel;
});