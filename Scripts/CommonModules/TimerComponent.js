﻿
require.config({
    paths: {


    }
});


define(['ajaxcallstatushandler', 'khelkundutils'],


    function (ajaxcallstatushandler, khelkundutils) {

        return Em.Component.extend(Khelkund.Mixins.AjaxCallsHandler, {

            // template present in home.aspx

            endDate: new Date().toString(), // to be given through template

            currentDate: new Date(),

            deadLine: function () {

                return new Date(this.get('endDate'));
            }.property('endDate').readOnly(),

            timeFormat: 'hh:mm:ss',  // format is readed along with seprator   
            // time format should be given through template


            showHour: function () {
                if (this.get('timeFormat').indexOf('hh') != -1) {
                    return true;
                }
                return false;

            }.property('timeFormat').readOnly(),

            showMinute: function () {
                if (this.get('timeFormat').indexOf('mm') != -1) {
                    return true;
                }
                return false;

            }.property('timeFormat').readOnly(),

            showSecond: function () {
                if (this.get('timeFormat').indexOf('ss') != -1) {
                    return true;
                }
                return false;

            }.property('timeFormat').readOnly(),

            timeSeperator: ':',



            time: function () {

                var deadLine = this.get('deadLine').toString();
                var currentDate = this.get('currentDate').toString();

                //var diff = this.get('deadLine') - this.get('currentDate');
                var diff = moment.utc(deadLine).valueOf() - moment.utc(currentDate).valueOf();
                var date = new Date(diff);
                
                var duration = moment.duration(diff);

                var formatted = '';
                if (diff > 0) {
                    var diffSeconds = diff / 1000;
                    var HH = Math.floor(diffSeconds / 3600);
                    var MM = Math.floor((diffSeconds % 3600) / 60);
                    var SS = Math.floor(diffSeconds % (60));
                    
                    if (this.get('showHour')) {
                        formatted += ((HH < 10) ? ("0" + HH) : HH);
                    }
                    if (this.get('showMinute')) {
                        formatted += ":" + ((MM < 10) ? ("0" + MM) : MM);
                    }
                    if (this.get('showSecond')) {
                        formatted += ":" + ((SS < 10) ? ("0" + SS) : SS);
                    }
                }
                else {
                    if (this.get('showHour')) {
                        formatted += "00";
                    }
                    if (this.get('showMinute')) {
                        formatted += ":" + "00";
                    }
                    if (this.get('showSecond')) {
                        formatted += ":" + "00";
                    }

                }
                

                return formatted;


            }.property('deadLine', 'currentDate', 'showDay', 'showHour', 'showMinute', 'showSecond').readOnly(),

            startTimer: function () {

                var timerHandler = setInterval(function () {
                    this.set('currentDate', new Date());
                }.bind(this), 1000);
                this.set('timerHandler', timerHandler);

            }.on('didInsertElement'),

            stopTimer:function(){
               
                clearInterval(this.get('timerHandler'));

            }.on('willDestroyElement'),


        });






    });