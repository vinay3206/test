;
/**
 * @module userteamdetails
 */
define(['ember'], function () {
	var UserTeamDetails = Em.Object.extend({
		rank: 0,
		lastRank: 0,
		points: 0,
		/**
		 * Computed property to find if user rank has improved or he has fallen behind.
		 * @return {Boolean} `true` if rank is same or less than lastRank. `false` otherwise.
		 */
		isProgressing: function () {
			return ((this.get('lastRank') - this.get('rank')) >= 0);
		}.property('rank', 'lastRank').readOnly(),
		/**
		 * UserName or team name.
		 * @type {String}
		 */
		teamName: null
	});

	UserTeamDetails.reopenClass({
		createLocalModel: function (apiModel) {
			var userTeamDetails = UserTeamDetails.create({
				rank: apiModel.CurrentRank,
				lastRank: apiModel.PreviousRank,
				points: apiModel.Points,
				teamName: apiModel.UserTeamName
			});
			return userTeamDetails;
		}
	});

	return UserTeamDetails;
});