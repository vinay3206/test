;
/**
 * @module tournamentsmodel
 */
define(['ember', 'match', 'khelkundutils'], function (ember, match, khelkundutils) {
    var TournamentModel = Em.Object.extend({
        id: null,
        name: null,
        format: null,//"ODI","T20",etc
        deadLine: null,
        matches: null//array of matchmodel
    });

    TournamentModel.reopenClass({
        apiUrl: 'Services/CricManagerService.svc/Series',


        /*
		@params: tournamentId. [optional], tournamentId to fetch specific tournament.
		@params: boolean. User this key to force the ajax call rahther than checking for previous promise for same call if any.
		*/
        fetch: function (tournamentId, forceFreshCall) {
            if (tournamentId === undefined) {
                return this._fetchTournaments(forceFreshCall, this.apiUrl);
            } else {
                return this._fetchTournamentById(tournamentId, forceFreshCall);
            }
        },

        fetchAll: function (tournamentId, forceFreshCall) {
            if (tournamentId === undefined) {
                return this._fetchTournaments(forceFreshCall, this.apiUrl + "/All");
            } else {
                return this._fetchTournamentById(tournamentId, forceFreshCall);
            }

        },


        _fetchListPromise: null,
        //private function
        _fetchTournaments: function (forceFreshCall, apiUrl) {
            var fetchPromise = null;
            if (this._fetchListPromise && !forceFreshCall) {
                return this._fetchListPromise;
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: apiUrl
                }, $.proxy(function (apiResponse) {
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        //validate apiResponse
                        var tournaments = [];
                        if (apiResponse.Series) {
                            apiResponse.Series.forEach($.proxy(function (tournamentItem, index, arr) {
                                tournaments.pushObject(this.createLocalModel(tournamentItem));
                            }, this));
                        }
                        resolve({
                            data: tournaments,
                            Status: apiResponse.Status
                        });
                    } else {
                        reject(apiResponse);
                    }
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            //save promise for next or future use
            this._fetchListPromise = fetchPromise;

            //return promise
            return fetchPromise;
        },
        createLocalModel: function (tournament) {
            var model = TournamentModel.create({
                id: tournament.Id,
                name: tournament.Name,
                format: tournament.SeriesType,
                deadLine: tournament.Schedule.EndDate
            });
            //add matches
            if (tournament.Matches) {
                model.set('matches', []);
                tournament.Matches.forEach(function (matchItem, index, arr) {
                    model.matches.pushObject(match.createLocalModel(matchItem));
                });
            }
            return model;
        },

        _fetchTournamentPromise: null,
        //private function
        _fetchTournamentById: function (tournamentId, forceFreshCall) {
            var fetchPromise = null;
            if (this._fetchTournamentPromise && !forceFreshCall) {
                return this._fetchTournamentPromise;
            }

            fetchPromise = new Em.RSVP.Promise(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.apiUrl + '/' + tournamentId
                }, function (apiResponse) {
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        //validate apiResponse
                        resolve({
                            data: apiResponse,
                            Status: apiResponse.Status
                        });
                    } else {
                        reject(apiResponse);
                    }
                }, function (err) {
                    //network error.
                    reject(err);
                });
            });

            //save promise for next or future use
            this._fetchTournamentPromise = fetchPromise;

            //return promise
            return fetchPromise;
        }
    });

    return TournamentModel;
});