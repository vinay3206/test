;
/*
Default model for team information. e.g. IndiaTeam
*/
define(['ember'], function (ember) {
	var TeamModel = Em.Object.extend({
		id: null,
		name: null,
		shortName: null,
		iconUrl: null
	});

	TeamModel.reopenClass({
		createLocalModel: function (team){
			team = team || {};
			return TeamModel.create({
				name: team.name,
				shortName: team.shortName,
                iconUrl:team.iconUrl
			});
		}
	});

	return TeamModel;
});