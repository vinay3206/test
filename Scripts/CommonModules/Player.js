;
define(['ember', 'team', 'detailedpoints', 'khelkundutils'], function (ember, team, detailedpoints, khelkundutils) {
	var PlayerModel = Em.Object.extend({
		id: null,	//unique identifier for player object
		firstName: null,
		lastName: null,
		shortName: null,
        _maxNameLength:7,
		displayName:function(){
            var firstName = this.get('firstName')?this.get('firstName'):'';
            var lastName = this.get('lastName')?this.get('lastName'):'';

            var firstShort = firstName.charAt(0) + " " + lastName;
            var lastShort = firstName + " " + lastName.charAt(0);
            
            var firstLastShort = firstName ? firstName.charAt(0) : "ZZZZZZZZZZZZZZZZZ" + " " + lastName ? lastName.charAt(0) : "ZZZZZZZZZZZZZZZZZZ";
            if (lastName && (lastName.length <= this.get('_maxNameLength'))) {
                return lastName;
            }

            if (firstName && (firstName.length <= this.get('_maxNameLength'))) {
                return firstName;
            }

            if (firstShort && (firstShort.length <= this.get('_maxNameLength'))) {
                return firstShort;
            }
            if (lastShort && (lastShort.length <= this.get('_maxNameLength'))) {
                return lastShort;
            }

          
            return firstLastShort;
          }.property('firstName','lastName'),
		category: null,//categories based on game type like batsman for cricket
		team: null,//model of Team.js
		value: 0,
		points: 0,
		pointsBreakup: null,//DetailedPoints model,
        avatarImgUrl: null  //avatar image of player
	});
	PlayerModel.reopenClass({
        apiUrl: 'Services/CricManagerService.svc/Players/{match_id}',

        /*
        @param: object. filter to pass for filtering calls. Possible params: seriesId, matchId, playerId
        @param: boolean. Decide weather to make a fresh call or return previous stored promise if already present.
        */
        fetch: function(filter, forceFreshCall){
            if(!filter || !filter.match_id) return; //call cant be made

            if(filter === undefined || filter === null || filter.playerId === undefined || filter.playerId === null) {
                return this._fetchPlayersList(filter.match_id, forceFreshCall);
            } else {
                //todo: also add support for filtering also.
                return this._fetchPlayerById(filter.playerId);
            }
        },
        /**
         * Store which will store filter vs promise mapping and returns those mappings.
         * @type {Array}
         */
        _fetchListStore: null,
        //private function
        _fetchPlayersList: function (match_id, forceFreshCall) {
            
            var fetchPromise = null;
            if(this._fetchListStore && !forceFreshCall) {
                var storedPromise = this._fetchListStore.findBy('match_id', match_id);
                if(storedPromise){
                    return storedPromise.promise;
                }
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.apiUrl.replace('{match_id}', match_id)
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse
                    var players = [];
                    
                    //correct upcoming spelling
                    if(khelkundutils.ajax.validateApiResult(apiResponse)){
                        apiResponse.Players.forEach($.proxy(function (playerItem, index, arr) {
                            players.pushObject(this.createLocalModel(playerItem));
                        }, this));
                    }
                    resolve({
                        data: players,
                        Status: apiResponse.Status
                    });
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            if(!this._fetchListStore){
                this._fetchListStore = [];
            }
            //save promise for next or future use
            this._fetchListStore.pushObject({
                match_id: match_id,
                promise: fetchPromise
            });

            //return promise
            return fetchPromise;
        },
        createLocalModel: function (player) {
            return PlayerModel.create({
                id: player.Id,
                firstName: player.FirstName,
                lastName: player.LastName,
                shortName: player.ShortName,
                category: player.Type,
                team: team.createLocalModel({
                    name: player.TeamName,
                    shortName: player.ShortTeamName,
                    iconUrl: player.TeamImageUrl
                }),
                value: player.Price,
                points: player.Points,
                pointsBreakup: detailedpoints.create(player.DetailedPoints),
                avatarImgUrl: player.ImageUrl
            });
        },
        _fetchPlayerPromise: null,
        //private function
        _fetchPlayerById:  function (playerId, forceFreshCall) {
            var fetchPromise = null;
            if(this._fetchPlayerPromise && !forceFreshCall) {
                return this._fetchPlayerPromise;
            }

            fetchPromise = new Em.RSVP.Promise(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.apiUrl + '/' + playerId
                }, function (apiResponse) {
                    //validate apiResponse
                    resolve(apiResponse);
                }, function (err) {
                    //network error.
                    reject(err);
                });
            });

            //save promise for next or future use
            this._fetchPlayerPromise = fetchPromise;

            //return promise
            return fetchPromise;
        }
    });

    //mock players
    var mock_players = [
        {
            FirstName: 'wassup'
        },
        {
            FirstName: 'how are u'
        }
    ];

	//also add this model to store

	return PlayerModel;

});