;
/**
 * @module challenge
 */
define(['ember','khelkundutils', 'userteamdetails', 'match'], function (ember,khelkundutils, userteamdetails, match) {
	var challengeModel= Em.Object.extend({
		id: null,
		name: null,
		
		type: 'open',//"open","joined","closed"
		date: null,//date object
		deadline: null,//date object
		//status of challenge. {"Open", "InProgress", "Closed"}
		status: null,
		capacity: 0,
		occupiedCount: function(){
			return this.get('capacity')-this.get('vacancy');
		}.property('capacity','vacancy'),
		joinedUsers: null,//model of user.js
		vacancy: 0,
		bounty: null,
		winningAmount: null,
		hasVacancy: function () {
			return !!this.get('vacancy');
		}.property('vacancy'),
		/**
		 * List of userTeams which are present in the challenge.
		 * @type {UserTeamDetails}
		 */
		challengerTeams: null,  //array of UserTeamDetails.js model.

		/**
		 * Contains the info about the match to which the challenge belongs to.
		 * @type {Object}
		 */
		matchInfo: null	//model of MatchModel.js
	});


	challengeModel.reopenClass({
		apiUrl: 'Services/ChallengeService.svc/GetOpenChallenges',
		/*
		@params: userId. Id of user for whom to fetch challenges.
		@params: challengeId. [optional], challengeId to fetch specific challenge.
		@params: boolean. User this key to force the ajax call rahther than checking for previous promise for same call if any.
		*/
		fetch: function (matchId,forceFreshCall) {
			return this._fetchOpenChallengeList(matchId, forceFreshCall);
		},

		_fetchListPromise: null,
		//private function
		_fetchOpenChallengeList: function (matchId, forceFreshCall) {
			var fetchPromise = null;
			if (this._fetchListPromise && !forceFreshCall) {
				return this._fetchListPromise;
			}
		
			fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
			
				khelkundutils.ajax.get({
					url: this.apiUrl+"/"+matchId,
				}, $.proxy(function (apiResponse) {
					//validate apiResponse
					if (khelkundutils.ajax.validateApiResult(apiResponse)) {
						var challenges = [];
						$.each(apiResponse.Challenges, $.proxy(function (index,val) {
							challenges.pushObject(this.createLocalModel(val));
						},this));
						resolve({
							data: challenges,
							Status: apiResponse.Status
						});
					}
					else {
						reject(apiResponse);
					}
			
				},this), function (err) {
					//network error.
					reject(err);
				});
			}, this));
			
			//save promise for next or future use
			this._fetchListPromise = fetchPromise;
			
			//return promise
			return fetchPromise;
		},


		createLocalModel: function (challenge) {
			var localChallenge = challengeModel.create({
				id: challenge.Id,
				name: challenge.Name,
				capacity: challenge.Capacity || 0,
				vacancy: challenge.SlotsAvailable || 0,
				bounty: challenge.PerTeamAmount || 0,
				winningAmount: challenge.TotalBounty || 0,
				status: challenge.ChallengeStatus,
				challengerTeams: [],
				matchInfo:  match.createLocalModel(challenge.MatchDetails || {})
			});
			//add challengerTeams also.
            if(Em.isArray(challenge.UserTeamDetails)){
                challenge.UserTeamDetails.forEach(function (teamDetailItem, index) {
                    localChallenge.get('challengerTeams').pushObject(userteamdetails.createLocalModel(teamDetailItem));
                });
            }
            return localChallenge;
		},
	});

	return challengeModel;

});