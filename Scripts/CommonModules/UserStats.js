﻿define(['ember', 'khelkundutils', 'userchallengemodel'], function (ember, khelkundutils, userchallengemodel) {
    var UserStatsModel = Em.Object.extend({
        //members
       
        wins: 0,
        losses: 0, 
        gamesPlayed: 0, 
        totalWinnings: 0, 
        challenges: null, // model of userChallenge.js
       

    });

   
    UserStatsModel.reopenClass({
        apiUrl: 'Services/ProfileService.svc/GetUserStats',

        



        createLocalModel: function (stats) {
            var statsModel= UserStatsModel.create({
                wins: stats.Wins,
                losses: stats.Losses,
                gamesPlayed: stats.GamesPlayed,
                totalWinnings: stats.TotalWinnings,
                challenges:[],
            });

            $.each(stats.Challenges, $.proxy(function (index, val) {
                statsModel.challenges.pushObject(userchallengemodel.createLocalModel(val));
            }));

            return statsModel;
        },


        fetchUserStats: function (forceFreshCall) {
            return this._fetchUserStats(forceFreshCall);
        },





        _fetchUserStatsPromise: null,
        _fetchUserStats: function (forceFreshCall) {
            var fetchPromise = null;
            if (this._fetchUserStatsPromise && !forceFreshCall) {
                return this._fetchUserStatsPromise;
            }

            fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {

                khelkundutils.ajax.get({
                    url: this.apiUrl
                }, $.proxy(function (apiResponse) {
                    //validate apiResponse
                    if (khelkundutils.ajax.validateApiResult(apiResponse)) {
                        var userStats = this.createLocalModel(apiResponse);
                        resolve({
                            data: userStats,
                            Status: apiResponse.Status

                        });
                    }
                    else {
                        reject(apiResponse)
                    }

                    
                }, this), function (err) {
                    //network error.
                    reject(err);
                });
            }, this));

            //save promise for next or future use
            this._fetchUserStatsPromise = fetchPromise;

            //return promise
            return fetchPromise;

        },








    });

    //also register this model to store for global access.

    return UserStatsModel;
});