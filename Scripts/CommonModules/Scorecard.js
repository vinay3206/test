;
define(['ember', 'teamplayer', 'khelkundutils', 'team'], function (ember, teamplayer, khelkundutils, team) {
	var Scorecard = Em.Object.extend({
		/**
		 * List of players in the match.
		 * This property is array of teamplayermodel
		 * @type {Array}
		 */
		players: null,
		homeTeam: null,	//model of Team.js model
		awayTeam: null	//model of Team.js model
	});

	Scorecard.reopenClass({
		/**
		 * Url on which ajax call is made.
		 * @type {String}
		 */
		apiUrl: 'Services/CricManagerService.svc/Scorecard/{match_id}/{team_id}',
		/**
		 * Public function to be used to fetch scorecard.
		 * @param  {Object} inputParams    The filter parameters which are to be used
		 *                                 for making an ajax call.
		 * @param {String} inputParams.team_id Required parameter which need to be present in inputParams.
		 * @param {String} inputParams.apiUrl The url on which the ajax call will be made. If this
		 *                                    parameter is provided then the default apiUrl of the model
		 *                                    is not                                                                 
		 * @param  {Boolean} forceFreshCall This parameter decides wether the cached promise is used
		 *                                  or fresh call is made to service.
		 * @return {Em.RSVP.Promise}                Promise object whch can be used to show or handle
		 *                                                  loaders
		 */
		fetch: function (inputParams, forceFreshCall) {
			if (typeof(inputParams) === 'object') {
				//check if team_id exists
				if(!inputParams.team_id || !inputParams.match_id) {
					console.error('Team Id and match_id are required parameter for fetching scorecard');
					return Em.RSVP.reject({
						Status: 1,
						message: 'Team Id and match_id are required parameter for fetching scorecard'
					});
				}
			}

			return this._fetchScorecard(inputParams, forceFreshCall);

		},
		/**
         * @private
         * Store to be used for caching api calls.
         * @type {Array}
         * @example
         * //sample for each object.
         * var sampleStoreObj = {
         *		apiUrl: apiUrl,
         *      method: 'methodName', //required since these methods have same signature.
         *      team_id: team_id
         *      promise: RSVP.Promise
         * };
         */
		_store: null,
		/**
         * @private
         * Helper function for store. Used to find weather the given params item is
         * present in store or not. If found returns the found store item.
         * @param  {Object} params Contains all the required params used to uniquely define
         *                         a store item.
         *                         @example
         *                             //sample object with full params
         *                                 {
         *                                     method: 'methodName',
         *                                     team_id: 'team_id',
         *                                     match_id: 'match_id',
         *                                     apiUrl: apiUrl
         *                                 }
         * @return {StoreItem}        Store item if found else null/undefined.
         */
		_findInStore: function (params) {
			params = params || {};
			var foundItem = this._store.find(function (storeItem, index) {
				if(storeItem.apiUrl !== params.apiUrl) {
					return false;
				}
				if(storeItem.method !== params.method) {
					return false;
				}
				if(storeItem.team_id !== params.team_id) {
					return false;
				}
				if(storeItem.match_id !== params.match_id) {
					return false;
				}
				//if all above are not satishfied then we have found our promise.
				return true;
			});
			return foundItem;
		},
		/**
		 * Private function called by the fetch function to get the scorecard.
		 * @param  {Object} inputParams    Filter which will be used to make an apiCall
		 * @param  {Boolean} forceFreshCall Key to switch the cache and cache burstmode.
		 * @return {Em.RSVP.Promise}                Promise for the ajax call. Can be used
		 *                                                  to handle the loaders.
		 */
		_fetchScorecard: function (inputParams, forceFreshCall) {
			var team_id = null, match_id = null,
				apiUrl = this.apiUrl,
				storedPromise = null;
			if(typeof(inputParams) === 'object') {
				team_id = inputParams.team_id;
				match_id = inputParams.match_id;
				apiUrl = inputParams.apiUrl || this.apiUrl;
			} else {
				console.error('improper format for inputParams');
			}

			var storeIdentifier = {
				method: '_fetchScorecard',
				apiUrl: apiUrl,
				team_id: team_id,
				match_id: match_id
			};

			if(this._store && !forceFreshCall) {
				storedPromise = this._findInStore(storeIdentifier);
				if(storedPromise) {
					//return stored promise if found
					return storedPromise.promise;
				}
			}

			var fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
				khelkundutils.ajax.get({
					url: apiUrl.replace('{team_id}', team_id).replace('{match_id}', match_id)
				}, $.proxy(function (apiResponse) {
					if(khelkundutils.ajax.validateApiResult(apiResponse)) {
						resolve({
							data: this.createLocalModel(apiResponse),
							Status: apiResponse.Status
						});
					} else {
						var storedPromise = this._findInStore(storeIdentifier);
						if(storedPromise) {
							this._store.removeObject(storedPromise);
						}
						reject(apiResponse);
					}
				}, this), $.proxy(function (err) {
					var storedPromise = this._findInStore(storeIdentifier);
					if(storedPromise) {
						this._store.removeObject(storedPromise);
					}
					reject(err);
					console.error('Network failure in scorecard call');
				}, this));
			}, this));

			if(!this._store) {
				this._store = [];
			}
			if(forceFreshCall === true) {
				storedPromise = this._findInStore(storeIdentifier);
				if(storedPromise) {
					//remove already stored promise to add a new fresh one.
					this._store.removeObject(storedPromise);
				}
			}

			this._store.pushObject({
				method: '_fetchScorecard',
				apiUrl: apiUrl,
				team_id: team_id,
				match_id: match_id,
				promise: fetchPromise
			});

			//return promise
			return fetchPromise;
		},

		createLocalModel: function (apiResponse) {
			var scorecard = Scorecard.create({
				players: [],
				homeTeam: team.createLocalModel({
					name: apiResponse.HomeTeamName,
					shortName: apiResponse.HomeTeamShortName,
					iconUrl: apiResponse.HomeTeamImageUrl
				}),
				awayTeam: team.createLocalModel({
					name: apiResponse.AwayTeamName,
					shortName: apiResponse.AwayTeamShortName,
					iconUrl: apiResponse.AwayTeamImageUrl
				})
			});

			if(apiResponse.PlayerDetails) {
				apiResponse.PlayerDetails.forEach(function (playerItem, index) {
					scorecard.get('players').pushObject(teamplayer.createLocalModel(playerItem));
				});
			}
			return scorecard;
		}
	});

	return Scorecard;
});