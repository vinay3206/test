;
/**
 * @module teamplayer
 */
define(['ember', 'player', 'pointsbreakup'], function (ember, player, pointsbreakup) {
	var TeamPlayerModel = player.extend({
		contributionPoints: 0,
		/**
		 * Breakup of points with all detailed events and
		 * points fetched there.
		 * @type {Object}
		 * Model of points breakup model.
		 */
		pointsBreakup: null,
		isCaptain: false
	});

	TeamPlayerModel.reopenClass({
		createLocalModel: function (player) {
			var teamPlayer = TeamPlayerModel.create(this._super(player), {
				contributionPoints: player.Points
			});
			if(player.PlayerMatchStats) {
				teamPlayer.set('pointsBreakup', pointsbreakup.createLocalModel(player.PlayerMatchStats));
			}

			return teamPlayer;
		}
	});
	return TeamPlayerModel;
});