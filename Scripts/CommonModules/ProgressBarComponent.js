﻿
require.config({
    paths: {


    }
});


define(['ajaxcallstatushandler', 'khelkundutils', 'd3', 'emberextensions'],


    function (ajaxcallstatushandler, khelkundutils,d3, emberextensions) {

        return Em.Component.extend(Khelkund.Mixins.AjaxCallsHandler, {


            width: 41,  // will be provided by template
            height: 41, // will be provided by template
            total: 100,  // total value can be total budget allowed in our case
            progress: 50, // progress value to show on progress bar


            makeProgressBar: function () {
                if (!(this.get('state') === 'inDOM')) {
                    return;
                }
                var width = this.get('width')===undefined?0:this.get('width'),
                height = this.get('height')===undefined?0:this.get('height'),
                twoPi = 2 * Math.PI,
                progress = this.get('progress')===undefined?0:this.get('progress'),
                total = this.get('total')===undefined?0:this.get('total'),
                formatPercent = d3.format(".0%");

                var arc = d3.svg.arc()
                .startAngle(0)
                .innerRadius(21)
                .outerRadius(28);

                this.$().html('');

                var svg = d3.select('#' + this.elementId).append("svg")
                    .attr("width", width)
                    .attr("height", height)
                  .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var meter = svg.append("g")
                    .attr("class", "progress-meter");

                meter.append("path")
                    .attr("class", "background")
                    .attr("d", arc.endAngle(twoPi));

                var foreground = meter.append("path")
                    .attr("class", "foreground");

                var text = meter.append("text")
                    .attr("text-anchor", "middle")
                    .attr("dy", ".35em");

                var angle = (this.get('progress') / this.get('total'));

                foreground.attr("d", arc.endAngle(angle * twoPi));
                text.text(Khelkund.Globals.helpers.consiseNumber(this.get('progress')));





            }.observes('total', 'progress', 'width', 'height').on('didInsertElement')



        });




    });