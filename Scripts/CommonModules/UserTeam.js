;
define(['ember', 'khelkundutils', 'teamplayer'], function(ember, khelkundutils, teamplayer) {
	var UserTeamModel =  Em.Object.extend({
		id: null,	//unique identifier of userTeam
		selectedFormation: null,
		allowedFormations: null,
		/**
		 * Name of userTeam
		 * @type {String}
		 */
		name: null,
		/**
		 * Points userTeam has aquired in a match.
		 * @type {Number}
		 */
		points: 0,
		/**
		 * This key is made true when service call fails and we create empty team.
		 * @type {Boolean}
		 */
		isEmptyTeam: false,

		captainId: function () {
			var capId = null;
			if(Em.isArray(this.get('players'))) {
				var captain = this.get('players').findBy('isCaptain', true);
				if(captain !== undefined) {
					capId = captain.get('id');
				}
			}
			return capId;
		}.property('players.@each.isCaptain').readOnly(),
		/**
		 * Total budget which can be used to include players in team.
		 * @type {Number} Default value is 1 Crore
		 */
		totalBudget: 10000000,
		_remBudget: 0,
		/**
		 * Computed Property which calculates itself when players are added on the team.
		 * @return {number}
		 */
		remBudget: function (key, value) {
			if(value === undefined) {
				//getter
				//calculate based on totalBudget and players in team
				var affordableBudget = this.get('totalBudget');
				if(this.get('players')){
					this.get('players').forEach(function (playerItem, index, playersArr) {
						//check if player is dummy or real one. Ths handling is added to cover the scenario where we wanted
						//to add dummy players to show empty players in use team.
						if(Em.get(playerItem, 'isBlankValue') !== true) {
							affordableBudget -= Em.get(playerItem, 'value');
						}
						if(affordableBudget <=0) {
							affordableBudget= 0;
							return false;
						}

					}, this);
				}
				this.set('_remBudget', affordableBudget);
				return affordableBudget;
			} else {
				//setter
				this.set('_remBudget', +value);
				return this.get('_remBudget');
			}
		}.property('players', 'players.@each', 'totalBudget', '_remBudget'),
		players: null,//array of teamplayer but may change depends on game type
		batsmans: Em.computed.filterBy('players', 'category', 'Batsman'),
		bowlers: Em.computed.filterBy('players', 'category', 'Bowler'),
		allRounders: Em.computed.filterBy('players', 'category', 'AllRounder'),
		keepers: Em.computed.filterBy('players', 'category', 'WicketKeeper'),
	});



	UserTeamModel.reopenClass({
		apiUrl: 'Services/UserTeamService.svc/UserTeam/{match_id}',
		
		/**
		 * @param match_id(Required)[string/int]: the match id for which user team is to be searched.
		 * @param: boolean. Decide weather to make a fresh call or return previous stored promise if already present.
		 */
		/**
		 * Delegates the fetching of userTeam based on input params.
		 * @param  {String/int/Object} match_id       If string or int then treated as match id and team for that
		 *                                            match_id is fetched. This is used while creating or editing team.
		 *                                            Another use-case for for a challenge in a match based on 
		 *                                            match_id and challenge_id. This will be used in cases where
		 *                                            only visibility of team is available.
		 *                                            
		 * @param  {boolean} forceFreshCall Decides weather a call is passed through cache handler or
		 *                                  directely asks the api to get userTeam.
		 * @return {RSVP.Promise}                Returns a promise for any loader or other async handling.
		 */
		fetch: function (match_id, forceFreshCall) {
			if(match_id === undefined || match_id === null || match_id === 'null'){
				console.error('Match id is required parameter to get UserTeam');
				return;	//call cant be made	
			}
			return this._fetchUserTeam(match_id, forceFreshCall);

		},
		/**
		 * Store which will store the promises vs match_id mapping to save promising for caching.
		 * Initialized the store with common array in KhelkundGlobals._store. For inter routes
		 * caching.
		 * @type {Array}
		 * @example
		 * //Sample store object
		 * _storeItem  =  {
		 *		match_id: "somelargestring",
		 *		challenge_id: "someanotherlargestring",
		 *		apiUrl: "url/for/which/call/was/made",
		 *		method: 'function_name_which_was_making_call'
		 * }
		 */
		_store: KhelkundGlobals._store,
		_findInStore: function (params) {
			params = params || {};
			var foundItem = this._store.find(function (storeItem, index) {
				if(storeItem.method !== params.method) {
					return false;
				}
				if(storeItem.match_id !== params.match_id) {
					return false;
				}
				if(storeItem.challenge_id !== params.challenge_id) {
					return false;
				}
				if(storeItem.apiUrl.toLowerCase() !== params.apiUrl.toLowerCase()) {
					return false;
				}
				//if all above are not true then we have found our item
				return true;
			});
			return foundItem;
		},
		_fetchUserTeam: function(match_id, forceFreshCall) {
			var filterObj = null, challenge_id = null, apiUrl = this.apiUrl;
			
			//some initialization.
			if(typeof(match_id) === 'object'){
				//i.e. challenge id and match id is provided.
				filterObj = match_id;
				match_id = filterObj.match_id;
				challenge_id = filterObj.challenge_id;
				if(filterObj.apiUrl) {
					apiUrl = filterObj.apiUrl;
				}
			}
			
			//create an object to uniquely identify store item
			var storeIdentifier = {
				match_id: match_id,
				challenge_id: challenge_id,
				method: '_fetchUserTeam',
				apiUrl: apiUrl
			};

			if(this._store && !forceFreshCall) {
				var storedPromise = null;
				storedPromise = this._findInStore(storeIdentifier);
				if(storedPromise) {
					//return stored promise if found
					return storedPromise.promise;
				}
			}
			var fetchPromise = null;
			fetchPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
				//var callUrl = apiUrl.replace('{match_id}', match_id).replace('{challenge_id}', challenge_id);
				khelkundutils.ajax.get({
				    url: apiUrl.replace('{match_id}', match_id).replace('{challenge_id}', challenge_id)
				}, $.proxy(function (apiResponse) {
					//success
					if(khelkundutils.ajax.validateApiResult(apiResponse)){
						resolve({
							data: this.createLocalModel(apiResponse),
							Status: apiResponse.Status
						});
					} else {
						var emptyModel = UserTeamModel.create({
							isEmptyTeam: true
						});
						//initialize some required fields
						emptyModel.set('allowedFormations', ['4_3_3_1', '4_4_2_1', '5_4_1_1', '5_3_2_1', '6_4_0_1', '6_3_1_1']);
						emptyModel.set('players', []);
						emptyModel.set('selectedFormation', emptyModel.get('allowedFormations')[0]);
						resolve({
							data: emptyModel,
							Status: 0
						});
						//but remove the cached value as the call is actuall failed
						var storedPromise = this._findInStore(storeIdentifier);
						if(storedPromise) {
							this._store.removeObject(storedPromise);
						}
					}
				}, this), $.proxy(function (err) {
					//network failure
					reject(err);
					//remove the cached value since the call is failure
					var storedPromise = this._findInStore(storeIdentifier);
					if(storedPromise) {
						this._store.removeObject(storedPromise);
					}
					console.error('Network failure:Failed getting user team.', err);
				}, this));
			}, this));

			if(!this._store){
				this._store = [];
			}

			//if forceFreshCall is true then find if this id is already present then remove it.
			if(forceFreshCall === true) {
				var savedPromise = this._findInStore(storeIdentifier);
				if(savedPromise) {
					this._store.removeObject(savedPromise);
				}
			}
			//save promise for caching of next call
			this._store.pushObject({
				match_id: match_id,
				challenge_id: challenge_id,
				method: '_fetchUserTeam',
				apiUrl: apiUrl,
				promise: fetchPromise
			});

			//return promise
			return fetchPromise;
		},
		createLocalModel: function (userTeam) {
			var model = UserTeamModel.create({
				id: userTeam.Id,
				selectedFormation: userTeam.Formation,
				points: userTeam.TotalPoints,
				name: userTeam.Name,
				allowedFormations: ['4_3_3_1', '4_4_2_1', '5_4_1_1', '5_3_2_1', '6_4_0_1', '6_3_1_1']
			});
			


			//userTeam.Players = [];
			//caluclate players
			if(!userTeam.Players){
				userTeam.Players = [];
			}
			model.set('players', []);
			var totalBudget = userTeam.Balance;
			userTeam.Players.forEach(function (playerItem, index, arr) {
				totalBudget += playerItem.Price;
				var teamPlayer = teamplayer.createLocalModel(playerItem);
				teamPlayer.set('isCaptain', teamPlayer.get('id') === userTeam.CaptainId);
				model.get('players').pushObject(teamPlayer);
			});
			model.set('totalBudget', totalBudget);

			if(!model.get('selectedFormation')) {
				model.set('selectedFormation', model.allowedFormations[0]);
			}

			return model;
		}

	});

	//save functionalities
	UserTeamModel.reopen({
		saveUrl: 'Services/UserTeamService.svc/saveTeam/{match_id}',
		save: function (match_id) {
			if(this.get('id')){
				return this._saveTeam(match_id);
			} else {
				//return this._createNewTeam();
				////currenty using same both places but ideally different versions should be used.
				return this._saveTeam(match_id);
			}
		},
		/**
		 * Promise to keep track of userTeam save call. Everytime new save is made this variable is reset.
		 * @type {Promise}
		 */
		_saveTeamPromise: null,
		/**
		 * Saves the already created team. Identified if id is present for userTeam instance.
		 * @return {promise} Promise can be used to show loaders. Return object will contain two properties.
		 * Data: data returned by the api converted to local model.
		 * Status: Showing result of apiCall 0=success.
		 */
		_saveTeam: function (match_id) {
			var savePromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
				khelkundutils.ajax.post({
					url: this.get('saveUrl').replace('{match_id}', match_id),
					data: this.toServiceModel()
				}, function (apiResponse) {
					//success
					if(khelkundutils.ajax.validateApiResult(apiResponse)) {
						var storedPromise = UserTeamModel._findInStore({
							match_id: match_id,
							challenge_id: null,
							method: '_fetchUserTeam',
							apiUrl: UserTeamModel.apiUrl
						});
						if(storedPromise) {
							UserTeamModel._store.removeObject(storedPromise);
						}
						UserTeamModel._store.pushObject({
							match_id: match_id,
							challenge_id: null,
							method: '_fetchUserTeam',
							apiUrl: UserTeamModel.apiUrl,
							promise: Em.RSVP.resolve({
								data: UserTeamModel.createLocalModel(apiResponse),
								Status: apiResponse.Status
							})
						});
						resolve({
							Status: apiResponse.Status
						});
					} else {
						reject(apiResponse);
					}
				}, function (err) {
					//network error
					reject({
						Status: 1,
						err: err
					});
				});
			}, this));
			
			//replace earlier promise with the new one.
			this.set('_saveTeamPromise',savePromise);
			return savePromise;
		},
		/**
		 * User this function if we have to create a new user team. It is differenciated from save team is 
		 * that it dosen't have userTeam id.
		 * @return {promise} Promise can be used to show loaders. Return object will contain two properties.
		 * Data: data returned by the api converted to local model.
		 * Status: Showing result of apiCall 0=success.
		 */
		_createNewTeam: function (match_id) {

		},
		/**
		 * Creates local mode to service model.
		 * @return {object} Converts camelCase to CapitalCase.
		 */
		toServiceModel: function () {
			var playerIds = this.get('players').mapBy('id');
			return {
				CaptainId: this.get('captainId'),
				Formation: this.get('selectedFormation'),
				PlayerIds: playerIds
			};
		}
	});

	var mock_userteam = {
		Balance: 0,
		CaptainId: null,
		Error: null,
		Formation: null,
		Id: null,
		Name: null,
		Players: null,
		Status: 1,
		TotalPoints: 0,
		TransfersLeft: 0
	};
	//mock players
    var mock_players = [
        {
            FirstName: 'wassup',
            Type: 'Batsman'
        },
        {
            FirstName: 'how are u',
            Type: 'Bowler'
        }
    ];


	return UserTeamModel;

});