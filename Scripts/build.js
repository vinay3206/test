({
	//create a relative path for base url
	appDir: '../Scripts',
	//creates a relative path for dependencies
	baseUrl: '../../',
	//relative to the build file.
	dir: '../Scripts-optimized',

	//file to be used for configuration of paths
	//relative to build file
	mainConfigFile: '../Scripts/Core/main-build.js',

	//set this to true to remove the combined files from the source directory
	removeCombined: false,
	paths: {},
	deps: [],
	modules: [],
	optimize: 'uglify2',
	preserveLicenseComments: false
})

/*modules: [
	{
		name: ['core'],
		exclude: [],
		include: [

			//tplibs

			//commonmodules
			'challenge', 'detailedpoints', 'match',
			'player', 'progressbarcomponent', 'progressbartimercomponent',
			'team', 'teamplayer', 'timercomponent', 'tournament',
			'user', 'userchallengemodel', 'userteam'
		],
		create: true
	},
	//add all apps to create seperate bundle for each app.
	{	name: 'EditTeam',
		exclude: [],
		include: ['EditTeamApp'],
		//create: true
	},
	{	name: 'HomeApp',
		exclude: [],
		include: ['HomeApp'],
		//create: true
	},
	{	name: 'Login',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	{	name: 'Match',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	{	name: 'Registration',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	{	name: 'ResetPassword',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	{	name: 'UserProfile',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	{	name: 'Verification',	//todo: to be created by pandu
		exclude: [],
		include: [],
		create: true
	},
	//add all components created, here to create a bundle for each component
	{	name: 'ActiveChallenges',
		exclude: [],
		include: ['ActiveChallengesController', 'ActiveChallengesView',
					'ActiveChallengeController', 'ActiveChallengeView'],
		create: true
	},
	{	name: 'UpcommingMatches',
		exclude: [],
		include: ['UpcommingMatchesController', 'UpcommingMatchesView',
					'UpcommingMatchController', 'UpcommingMatchView'],
		create: true
	},
	{	name: 'Tournaments',
		exclude: [],
		include: ['TournamentsController', 'TournamentsView',
					'MatchView'],
		create: true
	},
]*/