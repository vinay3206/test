﻿;

//require.config({
//    paths: {
//        loginmodel:'Apps/Login/Model/LoginModel'
//    }
//});
define(['ember','khelkundutils','loginmodel','fbauth','user'],
    function (ember, khelkundutils,loginmodel,fbauth,user) {
        
        return Em.Route.extend({


            beforeModel:function(transition){
                var user = this.controllerFor('application').get('userInfo');
                if (user) {
                    transition.abort();
                    this.transitionTo('home');
                }

            },


            model: function () {

                return {};
            },

            setupController:function(controller,model){
                this._super(controller, model);
                controller.set('content', loginmodel.create());

            },

            redirectTo: function () {
                //this.transitionTo('home');
                var transition = this.controllerFor('application').get('transition');
                if (transition) {
                    transition.retry();
                }
                else {
                    window.location = "Home.aspx#/";
                }
                
            },

            actions: {
                login: function (data) {
                    var loginStatus = khelkundutils.ajax.post({
                        url: 'Services/LoginService.svc/Login',
                        data: data
                    },
                                   $.proxy(function (apiResult) {
                                       if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                           var currentUser = user.createLocalModel(apiResult.User);
                                           this.controllerFor('application').set('userInfo', currentUser);
                                           $('body').trigger('userLoggedIn', [currentUser]);
                                           this.redirectTo();

                                       }
                                   }, this),
                                   $.proxy(function (err) {
                                       console.log('Failure');
                                   }, this));
                    this.get('controller.ajaxCalls.userLogin').setPromise(loginStatus);

                },


                loginWithFacebook:function(){

                  

                        FB.login($.proxy(function (response) {
                            if (response.authResponse) {
                                
                                var accessToken = FB.getAuthResponse()['accessToken'];
                                
                                    FB.api('/me', $.proxy(function (response) {
                                       
                                     
                                        khelkundutils.ajax.post({
                                            url: "Services/LoginService.svc/RegisterFacebook",
                                            data: {
                                                AccessToken: accessToken,
                                                Id: response.id,
                                                State: response.location?response.location.name:"",
                                                
                                            }


                                        }, $.proxy(function (apiResult) {
                                            if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                                var currentUser = user.createLocalModel(apiResult.User);
                                                this.controllerFor('application').set('userInfo', currentUser);
                                                $('body').trigger('userLoggedIn', [currentUser]);
                                                this.redirectTo();
                                            }

                                        }, this), $.proxy(function (err) {
                                            console.log('Failure');
                                        }, this));
                                        

                                    },this));
                               
                              
                            }

                        },this), { scope: 'email' });

                
                   

                   

                  

                },
            }
         
        });
    });