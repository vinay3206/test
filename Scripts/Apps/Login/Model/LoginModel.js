﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members
        email: '',

        password: '',


        //translators

        toServiceModel: function () {
            var serviceModel = {
               
                "Email":this.get('email'),
                "Password": this.get('password'),
                
            };
            return serviceModel;
        },

        reset: function () {
            this.set('email', '');
            this.set('password', '');

        },

        //validations
        validations: {
            email: {
                presence: {
                    controlName: 'Email',
                },
                format: {
                    value: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                    controlName: 'Email',
                }

            },



            password: {
                presence: {
                    controlName: 'Password',
                },


            },

        }
    });
});