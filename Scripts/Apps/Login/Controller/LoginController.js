﻿;

//require.config({
//    paths: {
       
//    }
//});

define(['ajaxcallstatushandler', 'khelkundutils'], function (ajaxcallstatushandler, khelkundutils) {
    

    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

       
        ajaxCallsToRegister: ['userLogin'],

        init: function () {
            this._super();
            this.registerAjaxCalls();



        },


        actions: {

            login: function () {

                if(this.get('content').validate()){
                    this.get('target').send('login', this.get('content').toServiceModel());
                }
            },

            register: function () {
                khelkundutils.redirect({url:"Home.aspx#/registration"});

            },

        }











    });

});