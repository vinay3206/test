﻿;

//require.config({
//    paths: {
//        loginroute: 'Apps/Login/Route/LoginRoute',
//        logincontroller: 'Apps/Login/Controller/LoginController',
//        loginview: 'Apps/Login/View/LoginView'
//    }
//});


define(['loginroute', 'logincontroller', 'loginview'], function (loginroute, logincontroller, loginview) {

    /**
        LoginRoutemap
        LoginRouter
        LoginView
        LoginController
        LoginModel
        LoginDataFixture
        LoginTemplates    
    */
    return {

        loginRoute: loginroute,
        loginView: loginview,
        loginController: logincontroller,

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.route('login', {path:'/'});//, {path:'/login'});
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Login';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.loginRoute;
            appInstance[viewName] = this.loginView;
            appInstance[controllerName] = this.loginController;
        }
    }


});