﻿;

//require.config({
//    paths: {
//        withdrawamountmodel: 'Apps/UserProfile/Model/WithDrawModel',
//        addamountmodel: 'Apps/UserProfile/Model/AddAmountModel',
//    }
//});

define(['ajaxcallstatushandler', 'withdrawamountmodel', 'addamountmodel'], function (ajaxcallstatushandler, withdrawamountmodel, addamountmodel) {
    

    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

        needs: ['application'],

        userInfoBinding:'controllers.application.userInfo',
       
        ajaxCallsToRegister: ['getProfile', 'updatePersonalInfo', 'updateAccountInfo', 'getUserStats','withdraw','pay'],

        init: function () {
            this._super();
            this.registerAjaxCalls();
            this.set('addAmount', addamountmodel.create());
            this.set('withdrawAmount', withdrawamountmodel.create());


        },


        // used while third party payment
        showLoader: function () {
            if (this.get('controller.ajaxCalls.pay.inProgress') || this.get('controller.ajaxCalls.pay.isSuccess')) {
                return true;
            }
            return false;

        }.property('controller.ajaxCalls.pay.status'),

        stats:null,

        updateUserProfileImage:function(){
            this.set('userInfo.personalInfo.avatar', this.get('personalInfo.avatar'));

        },

        updateUserBalance: function () {
            this.set('userInfo.amount', this.get('userInfo.amount') - parseInt(this.get('withdrawAmount.amount')));
            this.set('amount', this.get('userInfo.amount'));

        },


        checkBalance: function () {
            var balance = this.get('userInfo.amount');
            var withdrawAmount = this.get('withdrawAmount.amount');
            if (withdrawAmount > balance) {
                var error = [];
                error.push("Insufficient balance in account");
                this.get('withdrawAmount.validationErrors.allMessages').pushObject(error);
                return false;
            }
            return true;
        },

        checkAccountInfo: function () {
            if (this.get('accountInfo.accountNumber')) {
                return true;
            }
            else {
                var error = [];
                error.push("Please enter your pay pal account details in account section. we need your pay pal account email address to transfer money to your account");
                this.get('withdrawAmount.validationErrors.allMessages').pushObject(error);
                return false;
            }

        },
        checkPhoneNumber:function(){
            if(this.get('personalInfo.phoneNumber')){
                return true;
            }
            else {
                var error = [];
                error.push("Please enter your mobile number in personal information section.A 4 digit PIN will be send to your mobile during payment");
                this.get('addAmount.validationErrors.allMessages').pushObject(error);
                return false;
            }

        },

        onWithdrawSucess:function(){
            this.updateUserBalance();

        },

       

        actions: {

            saveAccountInfo: function () {
                var accountInfo = this.get('accountEditInfo');
                if (accountInfo.validate()) {
                    var promise = this.get('content').updateUserProfile();

                    this.get('ajaxCalls.updateAccountInfo').setPromise(promise);
                    promise.then($.proxy(function (apiResult) {
                        this.set('content', apiResult.data);
                    }, this));


                }

            },

            savePersonalInfo: function () {
                var personalInfo = this.get('personalEditInfo');
                if (personalInfo.validate()) {
                    var promise = this.get('content').updateUserProfile();
                    this.get('ajaxCalls.updatePersonalInfo').setPromise(promise);
                    promise.then($.proxy(function (apiResult) {
                        this.set('content', apiResult.data);
                        this.updateUserProfileImage();
                    }, this));
                }
            },

            gotoChallenge: function (matchId, challengeId) {
                this.get('target').transitionTo('userChallenges', { match_id: matchId, challenge_id: challengeId });
            },

            pay: function () {
                if (this.get('addAmount').validate()) {
                    this.get('target').send('pay', this.get('addAmount').toServiceModel());
                }
            },

            withdraw: function () {
                if (this.get('withdrawAmount').validate() && this.checkBalance() && this.checkAccountInfo()) {
                    var promise = this.get('withdrawAmount').withdraw();
                    this.get('ajaxCalls.withdraw').setPromise(promise);
                   
                    promise.then($.proxy(function (apiResult) {
                        this.onWithdrawSucess();
                    }, this));

                }
            }

           

        }











    });

});