﻿;

//require.config({
//    paths: {
        
//    }
//});

define(['ember', 'khelkundutils', 'user', 'userstats', 'authenticationmixin'],

    function (ember, khelkundutils, user, userstats, authenticationmixin) {
        
        return Em.Route.extend(authenticationmixin,{


            model: function () {

               
                return {};
            },
           
            setupController: function (controller,model) {
                this._super(controller, model);
                var url = $.url.parse(window.location.href);
                if (url.params && url.params.pstatus) {
                    controller.set('showPaymentPopUp', true);
                    controller.set('paymentStatus', url.params.pstatus);
                }
                this.getUserProfile();
                this.getUserStats();
            },


            getUserProfile:function(){
                //this.get('controller').set('content', user.createDummyProfile());

                var promise = user.fetchUserProfile();
                this.get('controller').get('ajaxCalls.getProfile').setPromise(promise);
                this.send('registerPromiseforLoader', promise);
                promise.then($.proxy(function (apiResult) {
                    this.get('controller').set('content', apiResult.data);
                }, this));

            },
            
            getUserStats:function(){
                var promise = userstats.fetchUserStats();
                this.get('controller').get('ajaxCalls.getUserStats').setPromise(promise);
                promise.then($.proxy(function (apiResult) {
                    this.get('controller').set('stats', apiResult.data);
                }, this));
            },

          
            //getUserProfile: function () {

            //    var promise = user.fetchUserProfile();
            //    this.get('controller').get('ajaxCalls.getProfile').setPromise(promise);
            //    promise.then($.proxy(function (user) {
            //        this.set('user', user);
                    
            //    }, this));



            //}

            actions: {
                pay: function (data) {
                    var requestObj = {
                        url: 'Services/PaymentService.svc/Authorize',
                        data: data
                    };
                    var autherizeStatus = khelkundutils.ajax.post(requestObj,
                        //success handler
                        $.proxy(function (apiResult) {
                            
                                console.log(apiResult);
                            // directly picking settleRequest from api
                            //window.open(apiResult.SettleRequest);
                                window.location = apiResult.SettleRequest;
                                //khelkundutils.redirect({url:apiResult.SettleRequest});
                           
                        }, this),
                        //failure handler
                        $.proxy(function (apiResult) {
                            //registration failed
                        }, this)
                    );
                    this.get('controller.ajaxCalls.pay').setPromise(autherizeStatus);

                },

        }

        });
    });