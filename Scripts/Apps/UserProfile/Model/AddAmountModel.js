﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
       amount:'',

        //translators

        toServiceModel: function () {
            var serviceModel = {
                OrderAmount: {
                    Amount: this.get('amount'),
                    Currency: "USD",
                    
                }
                

            };
            return serviceModel;
        },
        
      

        //validations
        validations: {
            amount: {
                presence: {
                    controlName: 'Amount',
                },
                numericality: {
                    controlName: 'Amount',
                    greaterThanOrEqualTo: 1
                },

            },



        }
    });
});