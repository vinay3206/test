﻿define(['validationmodel', 'khelkundutils'], function (validationmodel, khelkundutils) {
    return validationmodel.extend(Ember.Validations, {
       amount:'',

        //translators

       


        withdraw:function(){
            return this._withdraw();

        },

        _withdraw: function () {
            var withdrawPromise = null;
            var requestObj = {
                url: 'Services/ProfileService.svc/RedeemAmount',
                data: this.toServiceModel()
            };
            withdrawPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                khelkundutils.ajax.post(requestObj,
                         //success handler
                         $.proxy(function (apiResult) {
                             if (khelkundutils.ajax.validateApiResult(apiResult)) {
                               

                                 resolve(apiResult);
                             }
                             else {
                                 reject(apiResult);
                             }
                         }, this),
                         //failure handler
                         $.proxy(function (err) {
                             //registration failed
                             reject(err);
                         }, this)
                     );

            }));
            return withdrawPromise;


        },


        toServiceModel: function () {
            var serivceObj = {
                Amount:this.get('amount')

            };
            return serivceObj;
        },

      

        //validations
        validations: {
            amount: {
                presence: {
                    controlName: 'Withdraw Amount',
                },
                numericality: {
                    controlName: 'Withdraw Amount',
                    greaterThanOrEqualTo:5,

                },

            },



        }
    });
});