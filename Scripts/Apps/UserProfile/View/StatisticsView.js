﻿
//require.config({
//    paths: {


//    }
//});


define(['scrollmixin'],


    function (scrollmixin) {

        return Em.View.extend(scrollmixin,{

            templateName: 'statistics',

            scrollElement: '.scroll_custom',

            autoApply: false,


            onPromiseSuccess: function () {
                if (this.get('controller.ajaxCalls.getUserStats.isSuccess') === true) {
                    Em.run.next(this, function () {
                        this.updateScroll();
                    });
                }


            }.observes('controller.ajaxCalls.getUserStats.status'),

          
            //setScroll: function () {
            //    Em.run.next(this, function () {
            //        this.updateScroll();
            //    });

            //}.on('didInsertElement'),

            //setTabs: function () {

               
            //    $('#ChildVerticalTab_1').easyResponsiveTabs({
            //        type: 'default',
            //        width: 'auto',
            //        fit: true,
            //        tabidentify: 'ver_1', // The tab groups identifier
            //        activetab_bg: '#B5AC5F', // background color for active tabs in this group
            //        inactive_bg: '#E0D78C', // background color for inactive tabs in this group
            //        active_border_color: '#9C905C', // border color for active tabs heads in this group
            //        active_content_border_color: '#9C905C' // border color for active tabs contect in this group so that it matches the tab head border
            //    });

            //}.on('didInsertElement'),

          

        });






    });