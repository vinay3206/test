﻿
//require.config({
//    paths: {


//    }
//});


define([],


    function () {

        return Em.View.extend({

            templateName: 'addWithdraw',
            isAddActive: true,
            isWithdrawActive:false,


            amountList: [100, 50, 25, 10,5,2,'Other'],

            selectedAmount: 100,

            onSelectedAmountChange: function () {
                if (this.get('selectedAmount') === "Other") {
                    this.set('controller.addAmount.amount', '');

                }
                else {
                    this.set('controller.addAmount.amount', this.get('selectedAmount'));
                }

            }.observes('selectedAmount').on('init'),

            showTextBox: function () {
                if (this.get('selectedAmount') == "Other") {
                    return true;
                }
                return false;

            }.property('selectedAmount'),

            actions: {
                    enablePay: function () {
                       
                        this.set('isWithdrawActive', false);
                        this.set('isAddActive', true);
                    },
                    enableWithdraw: function () {
                        this.set('isAddActive', false);
                        this.set('isWithdrawActive', true);
                       

                    },
             }

        });






    });