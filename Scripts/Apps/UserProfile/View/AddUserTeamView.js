﻿;
define(['bs_modalpopupview', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview', 'khelkundutils', 'ajaxcallstatushandler','khelkundutils'],
    function (bs_modalpopupview, bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview, khelkundutils, ajaxcallstatushandler, khelkundutils) {




        var addUserTeamView = bs_modalpopupview.extend(ajaxcallstatushandler,{


            init: function () {
                this._super();
                this.set('handler.parent', this);

            },

            ajaxCallsToRegister: ['saveTeamName'],

            classNames: ['trans_background', 'match-bdr1'],

            headerView: bs_modalpopupheaderview.extend({
                template: null,
                classNames: ['hide']

            }),


            bodyView: bs_modalpopupbodyview.extend({

                //template: null,


                templateName: 'addUserTeamPopUp',
                classNames: ['modal-body'],



            }),

            footerView: bs_modalpopupfooterview.extend({

                template: null,
                classNames: ['hide']
            }),

            didInserElement:function(){
                this._super();
                //this.show();

            },

            handler: {
                show: function () {
                    this.parent.popup();
                },
                hide: function () {
                    this.parent.hide();

                }
            },

           

            updateTeamName:function(){
                var updateStatus = khelkundutils.ajax.get({
                    url: 'Services/ProfileService.svc/UpdateTeamName/' + this.get('teamName')

                },
                                    $.proxy(function (apiResult) {
                                        
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                            this.set('showClose', true);

                                        }
                                        
                                    }, this),
                                    $.proxy(function (err) {
                                        console.log('Failure');
                                    }, this));
                this.get('ajaxCalls.saveTeamName').setPromise(updateStatus);

            },
            

            actions: {

                close: function () {
                    this.hide();


                },

                saveTeamName: function () {
                    if (this.validateTeamName()) {
                        this.updateTeamName();
                    }
                    else {
                        this.set('showErrorMsg', true);
                    }

                },

               
            },

            showErrorMsg:false,

            validateTeamName: function () {
                if (this.get('teamName')) {
                    return true;
                }
                return false;

            },

            teamName: '',

            emptyTeamErrorMsg:"Enter team name",



        });








        return addUserTeamView;
    });