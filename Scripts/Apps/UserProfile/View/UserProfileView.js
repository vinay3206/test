﻿
//require.config({
//    paths: {

//        accountdetailsview: 'Apps/UserProfile/View/AccountDetailsView',
//        addwithdrawview: 'Apps/UserProfile/View/AddWithdrawView',
//        personalinfoview: 'Apps/UserProfile/View/PersonalInfoView',
//        statisticsview: 'Apps/UserProfile/View/StatisticsView',
        

//    }
//});


define(['accountdetailsview', 'addwithdrawview', 'personalinfoview', 'statisticsview'],


    function (accountdetailsview, addwithdrawview, personalinfoview, statisticsview) {
        
        return Em.View.extend({

            templateName:'user-profile',

            accountDetailsView:accountdetailsview,

            addWithdrawView: addwithdrawview,

            personalInfoView: personalinfoview,

            statisticsView: statisticsview,

           


            setProfileTabs:function(){
                $('#parentHorizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    tabidentify: 'hor_1', // The tab groups identifier
                    activate: function (event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#nested-tabInfo');
                        var $name = $('span', $info);

                        $name.text($tab.text());

                        $info.show();
                    }
                });

            }.on('didInsertElement'),

          

            // hack to remove facebook div
            removeFacebookDiv: function () {
                $('#fb-root').remove();

            }.on('didInsertElement'),
        });






    });