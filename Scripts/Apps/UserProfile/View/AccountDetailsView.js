﻿
//require.config({
//    paths: {
//        accountdetailseditview: 'Apps/UserProfile/View/AccountsEditView',
//        accountsview: 'Apps/UserProfile/View/AccountsView'

//    }
//});


define(['accountsview', 'accountdetailseditview'],


    function (accountsview, accountdetailseditview) {

        return Em.View.extend({

            templateName: 'accountDetails',

            isEditView:false,

            accountsDetailsEditView: accountdetailseditview,

            accountsView: accountsview,

            onUpdatePersonalInfoSuccess: function () {
                if (this.get('controller').get('ajaxCalls.updateAccountInfo.isSuccess')) {
                    this.set('isEditView', false);
                }

            }.observes('controller.ajaxCalls.updateAccountInfo.status'),

            actions: {
                edit: function () {
                    this.toggleProperty('isEditView');

                }
            }
        });






    });