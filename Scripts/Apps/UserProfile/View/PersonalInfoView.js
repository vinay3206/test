﻿
//require.config({
//    paths: {
//        personaleditview: 'Apps/UserProfile/View/PersonalInfoEditView',
//        personalview: 'Apps/UserProfile/View/PersonalView',

//    }
//});


define(['personaleditview', 'personalview'],


    function (personaleditview, personalview) {

        return Em.View.extend({

            templateName: 'personalInfo',

            isEditView: false,

            personalEditView: personaleditview,

            personalView: personalview,

            onUpdatePersonalInfoSuccess: function () {
                if (this.get('controller').get('ajaxCalls.updatePersonalInfo.isSuccess')) {
                    this.set('isEditView', false);
                }

            }.observes('controller.ajaxCalls.updatePersonalInfo.status'),

            actions: {

                edit: function () {
                    this.toggleProperty('isEditView');

                }
            }



        });






    });