﻿;

//require.config({
//    paths: {
//        userprofileroute: 'Apps/UserProfile/Route/UserProfileRoute',
//        userprofilecontroller: 'Apps/UserProfile/Controller/UserProfileController',
//        userprofileview: 'Apps/UserProfile/View/UserProfileView'
//    }
//});

/**
*   @module userprofileapp
*   @exports UserProfileRoute
*/
define(['userprofileroute', 'userprofilecontroller', 'userprofileview'], function (userprofileroute, userprofilecontroller, userprofileview) {

    /**
        UserProfileRoutemap
        UserProfileRouter
        UserProfileView
        UserProfileController
        UserProfileModel
        UserProfileDataFixture
        UserProfileTemplates    
    */
    return {
        
        userprofileRoute: userprofileroute,
        userprofileView: userprofileview,
        userprofileController: userprofilecontroller,
        

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.route('user-profile');
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'UserProfile';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.userprofileRoute;
            appInstance[viewName] = this.userprofileView;
            appInstance[controllerName] = this.userprofileController;


           




            
        }
    }


});