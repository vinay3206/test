﻿;

//require.config({
//    paths: {

//    }
//});


define(['khelkundutils', 'ajaxcallstatushandler', 'authenticationmixin'], function (khelkundutils, ajaxcallstatushandler, authenticationmixin) {


    return {

        paymentRoute: Em.Route.extend(authenticationmixin,{

            model: function () {
                var url = $.url.parse(window.location.href);
                var status = false;

                if (url && url.params && url.params.status) {
                    status = true;
                }
                return {
                    paymentStatus:status
                };

            }

        }),

        paymentView: Em.View.extend(),
        paymentController: Em.ObjectController.extend(ajaxcallstatushandler, {
           
            ajaxCallsToRegister: ['payment'],

            init: function () {
                this._super();
                this.registerAjaxCalls();



            },

            onPaymentSuccess: function () {
                if (this.get('paymentStatus')) {
                    Em.run.later(this, function () {
                        khelkundutils.removeQueryString();
                        this.get('target').transitionTo('user-profile');
                    }, 2000);
                }

            }.observes('paymentStatus').on('init')


        }),

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.route('payment');//, {path:'/payment'});
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Payment';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.paymentRoute;
            appInstance[viewName] = this.paymentView;
            appInstance[controllerName] = this.paymentController;
        }
    }


});