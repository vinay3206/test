﻿;

//require.config({
//    paths: {
       
//    }
//});

define(['ajaxcallstatushandler'], function () {
    

    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

        needs: ['application'],

       

        userInfoBinding:'controllers.application.userInfo',

        ajaxCallsToRegister: ['getTournaments', 'getOpenChallenges', 'getUserTeam', 'getJoinedChallenges', 'joinChallenge', 'createCustomChallenge', 'inviteFriends', 'getChallengeDetails'],

       // all tournaments to show,
       tournaments: [],

       // current selected match Id will be set through route params 
       //matchId: null,

       maxMatchCount:5,

       // selected value of tournament 
       selectedTournament:null,

       // selectedMatch 
      // selectedMatch: null, set in model hook of route

        
        // will change the route here to get details aboout that match
       onSelectedMatchChange:function(){
           // transition to new route for the match
           this.get('target').transitionTo('match', this.get('selectedMatch'));
          

       }.observes('selectedMatch'),

        // list of tournaments for dropdown
       tournamentNameList:function(){
           var tournaments = this.get('tournaments');
           var finalList = [];
           $.each(tournaments, function (index,val) {
               var obj = {
                   text: val.name,
                   value: val.id,
               };
               finalList.pushObject(obj);
           });
           return finalList;

       }.property('tournaments'),


        // list of matches for dropdown
       matchNameList:function(){
           var tournaments = this.get('tournaments');
           var selectedTournament = tournaments.findProperty('id', this.get('selectedTournament'));
           if (tournaments && !selectedTournament) {
               selectedTournament = tournaments[0];
           }
           var finalList = [];
           if (selectedTournament && selectedTournament.matches.length) {
               for (i = 0; i < selectedTournament.matches.length ; i++) {
                   var val = selectedTournament.matches[i];
                   var obj = {
                       text: val.homeTeam.shortName + " Vs " + val.awayTeam.shortName,
                       value: val.id,
                   };
                   finalList.pushObject(obj);
               }
              
               
           }
           return finalList;

       }.property('selectedTournament', 'tournaments'),


        // onmatch name list change set selectedmatch as 0th 
       OnMatchNameListChange:function(){
           var list = this.get('matchNameList');
           if (list.length > 0) {
               this.set('selectedMatch', list[0].value);
           }

       }.observes('matchNameList'),

        // current selected match data will be set through observer
       match: null,

        // match id observer will set selectedTournament,SelectedMatch, and Match to the controller
       onMatchIdChange:function(){
           var matchId = this.get('matchId');
           var tournaments = this.get('tournaments');
           
           $.each(tournaments, $.proxy(function (index, val) {
               var selectedMatch = val.matches.findProperty('id', matchId);
               if (selectedMatch) {
                   this.set('selectedTournament', val.id);
                   this.set('selectedMatch',selectedMatch.id);
                   this.set('match', selectedMatch);
               }
           },this));

       }.observes('matchId','tournaments'),

        

       openChallenges: [],

        challengeToJoin:null, // will be set when user clicks on open

       joinedChallenges: [],

       userTeam: null,



       updateUserBalance:function(amount){
           var balance = this.get('userInfo.amount');
           balance = balance - amount;
           this.get('userInfo').set('amount', balance);

       },

        
       validateUserBalance:function(){
           var balance = this.get('userInfo.amount');
           if (balance < parseInt(this.get('customChallenge.perHeadAmount'))) {
               var error = [];
               error.push("You dont have enough balance to create this challenge");
               this.get('customChallenge.validationErrors.allMessages').pushObject(error);
               return false;
           }
           return true;

       },

       isUserTeam:function(){
           if (this.get('userTeam') && this.get('userTeam.players.length')) {
               return true;
           }
           return false;

       }.property('userTeam','userTeam.players'),


        
       actions: {
           joinChallenge: function () {
               var serviceObj = {
                   ChallengeId: this.get('challengeToJoin.id'),
                   UserTeamId:this.get('userTeam.id')

               }
               this.get('target').send('joinChallenge', serviceObj);

           },

           createCustomChallenge: function () {
               if (this.get('customChallenge').validate() && this.validateUserBalance()) {
                   if (!this.get('isUserTeam')) {
                       var error = [];
                       error.push("create your team first");
                       this.get('customChallenge.validationErrors.allMessages').pushObject(error);
                   }
                   else {
                       var requestObject = this.get('customChallenge').toServiceModel();
                       requestObject.MatchId = this.get('matchId');
                       this.get('target').send('createChallenge', requestObject);
                   }

               }

           },

           shareOnFacebook: function () {
               FB.ui({
                   method: 'feed',
                   link: window.location.href,
                   caption: 'Join my league and play with me. My pass code is ' + this.get('challengePwd'),
               }, function (response) { });

           },


           inviteFriends: function () {
               if (this.get('inviteFriends').validate()) {
                   var reqObject = this.get('inviteFriends').toServiceModel();
                   reqObject.MatchId = this.get('matchId');
                   reqObject.ChallengeId = this.get('challengePwd');
                   this.get('target').send('inviteFriends', reqObject);
               }

           },

           getChallengeDetails: function () {
               if (this.get('joinChallenge').validate()) {
                   this.get('target').send('getChallengeDetails', this.get('joinChallenge.passCode'));
               }

           },



       },











    });

});