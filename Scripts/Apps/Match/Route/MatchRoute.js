﻿;

//require.config({
//    paths:{
//        customchallenge: 'Apps/Match/Model/CustomChallengeModel',
//        invitefriendsmodel: 'Apps/Match/Model/InvitationModel',
//        joinChallengeModel:"Apps/Match/Model/JoinChallengeModel"
//    }
//});
define(['ember', 'khelkundutils', 'tournamentmodel', 'challenge', 'userteam', 'userchallengemodel', 'authenticationmixin', 'customchallenge', 'invitefriendsmodel', 'joinChallengeModel'],
    function (ember, khelkundutils, tournamentmodel, challenge, userteam, userchallengemodel, authenticationmixin, customchallenge, invitefriendsmodel, joinChallengeModel) {
        
        return Em.Route.extend(authenticationmixin,{

            model: function (params) {
                
                //this.getTournaments();
                //this.getOpenChallenges(params.match_id);
                //this.getUserTeam(params.match_id);
                //this.controllerFor('match').set('matchId', params.match_id);
                return {
                    matchId: params.match_id,
                    selectedMatch:params.match_id
                };
                
            },
            
        

            setupController:function(controller,model){
                this._super(controller, model);
                var matchId = controller.get('matchId');
                this.getTournaments();
                this.getOpenChallenges(matchId);
                this.getJoinedChallenges(matchId);
                this.getUserTeam(matchId);
                this.setCustomChallengeModel();
                this.setInviteFriendsModel();
                this.setJoinChallengeModel();



            },

            setCustomChallengeModel:function(){
                this.controllerFor('match').set('customChallenge', customchallenge.create());

            },

            setInviteFriendsModel: function () {
                this.controllerFor('match').set('inviteFriends', invitefriendsmodel.create());

            },

            setJoinChallengeModel:function(){

                this.controllerFor('match').set('joinChallenge', joinChallengeModel.create());
            },

            getTournaments: function () {
                //fetch tournaments
                var promise = tournamentmodel.fetch();
                this.get('controller').get('ajaxCalls.getTournaments').setPromise(promise);
                this.send('registerPromiseforLoader', promise);
                promise.then($.proxy(function (apiResult) {
                    this.controllerFor('match').set('tournaments', apiResult.data);
                }, this));

            },
           

            getMatchDetails:function(){


            },

            getUserTeam:function(matchId){
                var promise = userteam.fetch(matchId,true);
                this.get('controller').get('ajaxCalls.getUserTeam').setPromise(promise);
                this.send('registerPromiseforLoader', promise);
                promise.then($.proxy(function (apiResult) {
                    this.controllerFor('match').set('userTeam', apiResult.data);
                }, this));

            },

            getOpenChallenges:function(matchId){
                var promise = challenge.fetch(matchId,true);
                this.get('controller').get('ajaxCalls.getOpenChallenges').setPromise(promise);
                this.send('registerPromiseforLoader', promise);
                promise.then($.proxy(function (apiResult) {
                    this.controllerFor('match').set('openChallenges', apiResult.data);
                }, this));
            },

            getJoinedChallenges:function(matchId){
                var promise = userchallengemodel.fetchJoinedChallenges(matchId, true);
                this.get('controller').get('ajaxCalls.getJoinedChallenges').setPromise(promise);
                promise.then($.proxy(function (apiResult) {
                    this.controllerFor('match').set('joinedChallenges', apiResult.data);
                }, this));


            },

           

            onJoinChallengeSuccess: function (apiResult,amount) {
                // to update open challenges in case of success from service side
                this.get('controller').updateUserBalance(amount);
                this.getOpenChallenges(this.get('controller.matchId'));
                this.getJoinedChallenges(this.get('controller.matchId'));
                // update joined challenges once gulli writes model for active challenges


            },

            onJoinChallengeFailure:function(){
                // to update open challenges in case of failure from service side
                this.getOpenChallenges(this.get('controller.matchId'));

            },

            actions: {
                joinChallenge: function (data) {
                    var reqObj = {
                        url: 'Services/ChallengeService.svc/JoinChallenge',
                        data: data
                    }
                    var joinChallengePromise = khelkundutils.ajax.post(reqObj,
                                    //Success xhr
                                    $.proxy(function (apiResult) {
                                        //validate 
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                            this.onJoinChallengeSuccess(apiResult,this.get('controller.challengeToJoin.bounty'));
                                        } else {
                                            //reject promise
                                            this.onJoinChallengeFailure(apiResult);
                                            
                                        }
                                    }, this),
                                    //failure xhr
                                    $.proxy(function (err) {

                                    }, this));
                    this.get('controller').get('ajaxCalls.joinChallenge').setPromise(joinChallengePromise);
                    
                },

                updateOpenChallenges: function () {
                    this.getOpenChallenges(this.get('selectedMatch'));
                },

                createChallenge: function (data) {
                    var reqObj = {
                        url: 'Services/ChallengeService.svc/CreateCustomChallenge',
                        data: data
                    }
                    var customChallengePromise = khelkundutils.ajax.post(reqObj,
                                    //Success xhr
                                    $.proxy(function (apiResult) {
                                        //validate 
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                            this.get('controller').set('challengePwd', apiResult.ChallengePwd);
                                            this.onJoinChallengeSuccess(apiResult, this.get('controller.customChallenge.perHeadAmount'));
                                           // set selected challenge id
                                        } else {
                                            //reject promise
                                          
                                        }
                                    }, this),
                                    //failure xhr
                                    $.proxy(function (err) {

                                    }, this));
                    this.get('controller').get('ajaxCalls.createCustomChallenge').setPromise(customChallengePromise);

                },

                inviteFriends: function (data) {
                    var reqObj = {
                        url: 'Services/ChallengeService.svc/InviteFriends',
                        data: data
                    }
                    var invitePromise = khelkundutils.ajax.post(reqObj,
                                    //Success xhr
                                    $.proxy(function (apiResult) {
                                        //validate 
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                            
                                            // set selected challenge id
                                        } else {
                                            //reject promise

                                        }
                                    }, this),
                                    //failure xhr
                                    $.proxy(function (err) {

                                    }, this));
                    this.get('controller').get('ajaxCalls.inviteFriends').setPromise(invitePromise);

                },


                getChallengeDetails: function (challengeId) {

                  var detailsPromise=  khelkundutils.ajax.get({
                      url: 'Services/ChallengeService.svc/GetMiniLeage/' + challengeId,

                    }, $.proxy(function (apiResult) {
                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                            $('body').trigger('joinChallenge', [challenge.createLocalModel(apiResult)]);
                        }

                    }, this), $.proxy(function (err) {
                        
                    }, this));

                  this.get('controller').get('ajaxCalls.getChallengeDetails').setPromise(detailsPromise);
                },
            }
         
        });
    });