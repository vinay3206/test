﻿
//require.config({
//    paths: {
       
//        activechallengeview: 'Components/ActiveChallenges/ActiveChallengeView',
//        openchallenge: 'Apps/Match/View/OpenChallengeView',
//        joinchallengepopupview:'Apps/Match/View/JoinChallengePopView'

//    }
//});


define(['activechallengeview', 'openchallenge', 'joinchallengepopupview', 'scrollmixin', 'eventregister'],


    function (joinedchallenge, openchallenge, joinchallengepopupview, scrollmixin, eventregister) {

        return Em.View.extend(scrollmixin,{

            templateName: 'challenges',
            
            toggleChallengesView: true,

            scrollElement: '.scroll_custom',

            autoApply: false,

            onPromiseSuccess: function () {
                if (this.get('controller.ajaxCalls.getOpenChallenges.isSuccess') === true || this.get('controller.ajaxCalls.getOpenChallenges.isSuccess')===true){
                    Em.run.next(this, function () {
                        this.updateScroll();
                    });
                }
                

            }.observes('controller.ajaxCalls.getOpenChallenges.status', 'controller.ajaxCalls.getJoinedChallenges.status'),

            openChallengesView: Em.View.extend(eventregister,{
                templateName:'openChallenges',
                openChallengeView: openchallenge,
                eventsToRegister: [{ eventName: 'joinChallenge' }],
                bindEvents: function () {
                    this._super();
                }.on('init'),

                onJoinChallenge: function (ev, challenge) {
                    this.get('controller.ajaxCalls.joinChallenge').resetPromise();
                    this.get('controller').set('challengeToJoin', challenge);
                    this.get('joinChallengePopUpHandler').show();
                },

                joinChallengePopUpHandler:{},
                joinChallengePopupView: joinchallengepopupview.extend({
                    init: function () {
                        this._super();
                        this.set('parentView.joinChallengePopUpHandler', this.get('handler'));
                    },

                }),

                actions: {
                    joinChallenge: function (challenge) {
                        this.get('controller.ajaxCalls.joinChallenge') && this.get('controller.ajaxCalls.joinChallenge').resetPromise();
                        this.get('controller').set('challengeToJoin', challenge) && this.get('controller').set('challengeToJoin', challenge);
                        this.get('joinChallengePopUpHandler').show();

                    },
                }

            }),

            joinedChallengesView: Em.View.extend({
                templateName: 'joinedChallenges',
                joinedChallengeView: joinedchallenge,

            }),

            actions: {
                changeView: function () {
                    this.toggleProperty('toggleChallengesView');
                },
            },

          

            setResponsiveTab: function () {
                $('#challenges').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion           
                    width: 'auto', //auto or any width like 600px
                    fit: true,   // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    activate: function (event) { // Callback function if tab is switched
                        var $tab = $(this);
                        var $info = $('#tabInfo');
                        var $name = $('span', $info);

                        $name.text($tab.text());

                        $info.show();
                    }
                });

                $('#verticalTab').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true
                });

            }.on('didInsertElement'),

            setScroll:function(){
                this.applyScroll();

            }.on('didInsertElement'),

            bindResponsiveTabEvents: function () {
                $($('.jChallengesTab'), this.$()).on('tabactivate', $.proxy(function (e) {
                    Em.run.next(this, function () {
                        this.updateScroll();
                    });
                }, this));
            }.on('didInsertElement'),

        });






    });