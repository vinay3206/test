﻿;
define(['bs_modalpopupview', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview', 'khelkundutils', 'ajaxcallstatushandler', 'khelkundutils', 'fbauth'],
    function (bs_modalpopupview, bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview, khelkundutils, ajaxcallstatushandler, khelkundutils, fbauth) {




        var createCustomChallengeView = bs_modalpopupview.extend( {


          
            init: function () {
                this._super();
                this.set('handler.parent', this);

            },


            classNames: ['trans_background', 'match-bdr1'],

            headerView: bs_modalpopupheaderview.extend({
                template: null,
                classNames: ['hide']

            }),


            bodyView: bs_modalpopupbodyview.extend({

                //template: null,


                templateName: 'createChallengePopUp',
                classNames: ['modal-body'],

                showClose: function () {
                    var cstatus = this.get('controller.ajaxCalls.createCustomChallenge');
                    var istatus = this.get('controller.ajaxCalls.inviteFriends');
                    if ((cstatus.get('isFailure') && istatus.get('isFailure')) || (cstatus.get('isSuccess') && istatus.get('isSuccess'))) {
                        return true;
                    }
                    return false;

                }.property('controller.ajaxCalls.createCustomChallenge.isSuccess', 'controller.ajaxCalls.createCustomChallenge.isFailure', 'controller.ajaxCalls.inviteFriends.isSuccess', 'controller.ajaxCalls.inviteFriends.isFailure'),

                showCancel: function () {
                    var cstatus = this.get('controller.ajaxCalls.createCustomChallenge');
                    var istatus = this.get('controller.ajaxCalls.inviteFriends');
                    if (cstatus.get('inProgress') || istatus.get('inProgress')) {
                        return false;
                    }
                    return true;


                }.property('controller.ajaxCalls.createCustomChallenge.isSuccess', 'controller.ajaxCalls.createCustomChallenge.isFailure', 'controller.ajaxCalls.inviteFriends.isSuccess', 'controller.ajaxCalls.inviteFriends.isFailure'),



            }),

            footerView: bs_modalpopupfooterview.extend({

                template: null,
                classNames: ['hide']
            }),

          
            actions: {

                close: function () {
                    this.hide();


                },

               


            },

            handler: {
                show: function () {
                    this.parent.popup();
                },
                hide: function () {
                    this.parent.hide();

                }
            },

        });








        return createCustomChallengeView;
    });