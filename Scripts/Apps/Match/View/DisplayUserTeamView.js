﻿
//require.config({
//    paths: {

      

//    }
//});


define([],


    function () {

        return Em.View.extend({

            templateName: 'displayUserTeam',

            teamPlayerView: Em.View.extend({
                templateName: 'displayTeamPlayer',
                player: null,
                

            }),

            showEditTeamOption: function () {
                var tournament = this.get('controller.tournaments').findProperty('id', this.get('controller.selectedTournament'));
                if (tournament) {
                    var match = tournament.matches.findProperty('id', this.get('controller.selectedMatch'));
                    if (match) {
                        var diff = new Date(match.get('deadline')) - this.get('currentDate');
                        if (diff <= 0) {
                            
                            return false;
                        }
                    }
                }
                return true;
               


            }.property('selectedTournamet', 'selectedMatch', 'currentDate'),


            onShowEditOptionChange:function(){
                if(!this.get('showEditOption')){
                    this.send('updateOpenChallenges');
                }

            }.observes('showEditOption'),

            currentDate:new Date(),

            

           startCheckOnTime: function () {

                    var timerHandler = setInterval(function () {
                        this.set('currentDate', new Date());
                    }.bind(this), 1000);
                    this.set('timerHandler', timerHandler);

              }.on('didInsertElement'),

           stopCheckOnTime: function () {

               clearInterval(this.get('timerHandler'));

           }.on('willDestroyElement'),



        });
    });