﻿
//require.config({
//    paths: {

//        createcustomchallengeview:"Apps/Match/View/CreateCustomChallengeView"
//    }
//});


define(['createcustomchallengeview', 'challenge','fbauth'],


    function (createcustomchallengeview, challenge, fbauth) {

        return Em.View.extend({

            templateName: 'customChallenge',

            
            createCustomChallengeView: createcustomchallengeview.extend({
                init: function () {
                    this._super();
                    this.set('parentView.popUpHandler', this.get('handler'));
                },

               

            }),

            

            actions: {
                createChallenge: function () {
                    this.get('controller.ajaxCalls.createCustomChallenge').resetPromise();
                    this.get('controller.ajaxCalls.inviteFriends').resetPromise();
                    this.get('popUpHandler').show();
                    
                },

               
            },


        });






    });