;
//require.config({
//    paths: {
//        seriesmatchselectorview: 'Apps/Match/View/SeriesMatchSelectorView',
//        matchdetailview: 'Apps/Match/View/MatchDetailView',
//        challengesview: 'Apps/Match/View/ChallengesView',
//        displayuserteamview: 'Apps/Match/View/DisplayUserTeamView',
//        userteamview: 'Components/UserTeam/UserTeamView',
//        customchallengeview: "Apps/Match/View/CustomChallengeView"



//    }
//});


define(['seriesmatchselectorview', 'matchdetailview', 'challengesview', 'userteamview', 'displayuserteamview', 'customchallengeview'],


    function (seriesmatchselectorview, matchdetailview, challengesview, userteamview, displayuserteamview, customchallengeview) {
        
        return Em.View.extend({

           
            templateName:'match',

            seriesMatchSelectorView: seriesmatchselectorview.extend(),
            matchDetailView: matchdetailview.extend(),
            customChallengeView:customchallengeview.extend(),
            challengesView: challengesview.extend(),
            userTeamView: displayuserteamview.extend(),

            //showLoader: function () {
                
            //    if ((this.get('controller.ajaxCalls.getTournaments.inProgress')) || (this.get('controller.ajaxCalls.getUserTeam.inProgress'))) {
            //        return true;
            //    }
            //    return false;

            //}.property('controller.ajaxCalls.getTournaments.inProgress', 'controller.ajaxCalls.getUserTeam.inProgress')
            


        });
    });

