﻿;
define(['bs_modalpopupview', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview', 'khelkundutils'],
    function (bs_modalpopupview, bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview, khelkundutils) {




        var joinChallengeModalPopUpView = bs_modalpopupview.extend({


            init: function () {
                this._super();
                this.set('handler.parent', this);
               
            },
            
            classNames:['trans_background' ,'match-bdr1'],

            headerView: bs_modalpopupheaderview.extend({
                template: null,
                classNames:['hide']
                
            }),


            bodyView: bs_modalpopupbodyview.extend({

                //template: null,

                
                templateName:'joinChallengePopUp',
                classNames: ['modal-body'],

                balanceToAdd:function(){
                    return parseInt(this.get('controller.challengeToJoin.bounty')) - parseInt(this.get('controller.userInfo.amount'));

                }.property('controller.userInfo','controller.challengeToJoin'),

                isAvailableBalance: function () {
                    var balance = this.get('controller.userInfo.amount');
                    var challengeToJoin = this.get('controller.challengeToJoin');
                    
                    if (challengeToJoin) {
                        if (balance >= challengeToJoin.bounty) {
                            return true;
                        }
                    }
                    return false;

                }.property('controller.userInfo', 'controller.challengeToJoin'),

                isUserTeam: function () {
                    if (this.get('controller.userTeam')&& this.get('controller.userTeam.players.length')) {
                        return true;
                    }
                    return false;

                }.property('controller.userTeam','controller.userTeam.players'),

                showJoin:function(){
                    return (this.get('isUserTeam') && this.get('isAvailableBalance'));

                }.property('isUserTeam','isAvailableBalance'),

                showCancel: function () {
                    return !(this.get('controller.ajaxCalls.joinChallenge.inProgress') || this.get('controller.ajaxCalls.joinChallenge.isFailure') || this.get('controller.ajaxCalls.joinChallenge.isSuccess'))

                }.property('controller.ajaxCalls.joinChallenge', 'controller.ajaxCalls.joinChallenge.isSuccess', 'controller.ajaxCalls.joinChallenge.isFailure'),

                isSuccessOrFailure: function () {
                    return this.get('controller.ajaxCalls.joinChallenge.isSuccess') || this.get('controller.ajaxCalls.joinChallenge.isFailure')

                }.property('controller.ajaxCalls.joinChallenge', 'controller.ajaxCalls.joinChallenge.isSuccess', 'controller.ajaxCalls.joinChallenge.isFailure')

                
                
            }),

            footerView: bs_modalpopupfooterview.extend({

                template: null,
                classNames: ['hide']
            }),

            handler: {
                show: function () {
                    this.parent.popup();
                },
                hide: function () {
                    this.parent.hide();

                }
            },

            actions: {

                close: function () {
                    this.hide();


                },

                join: function () {
                    
                    this.get('controller').send('joinChallenge');
                    
                },

                addBalance: function () {
                    this.hide();
                    this.get('controller.target').transitionTo('user-profile');
                }
            }

           

        });








        return joinChallengeModalPopUpView;
    });