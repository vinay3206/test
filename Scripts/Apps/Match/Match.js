﻿;

//require.config({
//    paths: {
//        matchroute: 'Apps/Match/Route/MatchRoute',
//        matchcontroller: 'Apps/Match/Controller/MatchController',
//        matchview:'Apps/Match/View/MatchView'
//    }
//});


define(['matchroute','matchcontroller','matchview'], function (matchroute,matchcontroller,matchview) {

    /**
        MatchRoutemap
        MatchRouter
        MatchView
        MatchController
        MatchModel
        MatchDataFixture
        MatchTemplates    
    */
    return {

        matchRoute : matchroute,
        matchView : matchview,
        matchController : matchcontroller,

        mapRoutes : function (appInstance) {
            appInstance.Router.map(function () {
                this.route('match', {path:'/match/:match_id'});//, {path:'/match'});
            });
        },

        initModule : function (appInstance, moduleName) {
           this.mapRoutes(appInstance);
            moduleName = moduleName || 'Match';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.matchRoute;
            appInstance[viewName] = this.matchView;
            appInstance[controllerName] = this.matchController;
        }
    }


});