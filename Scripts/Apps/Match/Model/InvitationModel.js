﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members
        emails: '',

        emailList:function(){
            return this.get('emails').split(',');

        }.property('email'),
        //translators

        toServiceModel: function () {
            var serviceModel = {

                ListOfEmails:this.get('emailList'),

            };
            return serviceModel;
        },



        //validations
        validations: {
            emails: {
                presence: {
                    controlName: 'email',
                },

            },

         
        }
    });
});