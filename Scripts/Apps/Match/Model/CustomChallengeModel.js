﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members
        selectedSize:2,

        perHeadAmount:2 ,

        size: [2, 3, 5],

        winningAmount: function () {
            var amount = this.get('selectedSize') * parseInt(this.get('perHeadAmount'));
            //return amount;
            if(isNaN(amount)){
                return 0;
            }
            else {
                var cut = (amount * 15) / 100;

                var winAmount = amount - cut;

                //var winAmount=Math.round(amount-cut);
                //var adjustment=winAmount % 10;
                //if(adjustment>2){
                //    winAmount=winAmount+5-adjustment;
                //}
                //else{
                //    winAmount=winAmount-(5-adjustment);
                //}
                if (winAmount < 0) {
                    return 0;
                }
                return winAmount;
            }

            


        }.property('selectedSize', 'perHeadAmount'),

        //translators

        toServiceModel: function () {
            var serviceModel = {

                "Capacity": this.get('selectedSize'),
                "Bounty": parseInt(this.get('perHeadAmount')),

            };
            return serviceModel;
        },

       

        //validations
        validations: {
            selectedSize: {
                presence: {
                    controlName: 'per user contribution',
                },
                
            },

            perHeadAmount: {

                presence: {
                    controlName: 'Per head contribution',
                },
                numericality: {
                    controlName: 'Per head contribution',
                    greaterThanOrEqualTo:1,
                }

            },

            winningAmount: {
                
                    presence: {
                        controlName: 'Winnning Amount',
                    },
                    numericality: {
                        controlName: 'Winnning Amount',
                        lessThanOrEqualTo: 1000,

                    },

            },

        }
    });
});