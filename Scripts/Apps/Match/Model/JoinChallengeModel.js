﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members
        passCode: '',

     
        toServiceModel: function () {
            var serviceModel = {

                ListOfEmails: this.get('emailList'),

            };
            return serviceModel;
        },



        //validations
        validations: {
            passCode: {
                presence: {
                    controlName: 'Pass code',
                },

            },


        }
    });
});