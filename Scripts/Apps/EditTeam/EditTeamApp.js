;
/*require.config({
    paths: {
        editconfig: 'Apps/EditTeam/editConfig'
    }
});*/
/** @module editteamapp */
define(['ember', 'authenticationmixin'], function (ember, authenticationmixin) {

    /**
        EditTeamRoute
    */
    return {

        editTeamIndexRoute: Em.Route.extend({
            model: function (param, transition) {
                /**
                 * Read the model of parent resource. As parent modl will be having all the information
                 * regarding the match_id and challenge_id.
                 */
                return this.modelFor(this.get('moduleName').camelize());
            },
            beforeModel: function () {
                var loadComponentsPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                    var appInstance = Em.Namespace.byName(window.KhelkundGlobals.AppName);
                    //read config
                    require(['editconfig'], $.proxy(function (config) {
                        this.set('config', config);
                        var dependencies = [];
                        for (var i = 0; i < config.leftHalf.dependencies.length; i++) {
                            dependencies.push(config.leftHalf.dependencies[i]);
                        }
                        for (var i = 0; i < config.rightHalf.dependencies.length; i++) {
                            dependencies.push(config.rightHalf.dependencies[i]);
                        }

                        /*var requirePaths = {};
                        //map the required dependencies to their paths using require.config
                        for(var i = 0; i < dependencies.length; i++){
                            
                            //moduleName is the real name on which module is to be registered.
                            //for registering modules on app use instanceModuleName property.
                            requirePaths[dependencies[i].moduleName.toLowerCase()] = dependencies[i].path;
                        }
                        //add these paths to require.config
                        require.config({
                            paths: requirePaths
                        });*/
                        //fetch the mapped paths.
                        var mappedDependencies = dependencies.map(function (item, index) {
                            return Em.get(item, 'moduleName').toLowerCase();
                        });
                        require(mappedDependencies, function () {
                            for (var i = 0; i < dependencies.length; i++) {
                                //register dependencies modules in app
                                //NOTE: their name should not be clashing.
                                //todo: handle the same name case also.
                                appInstance[dependencies[i].instanceModuleName || dependencies[i].moduleName] = arguments[i];
                            }
                            resolve("Components loaded Successfuly");
                        }, function (err) {
                            reject ('Unable to load Dependencies for components', err);
                        });
                    }, this), function (err) {
                        reject ('Unable to load config file for editTeam', err);
                    });


                }, this));
                return loadComponentsPromise;
            },
            renderTemplate: function (controller, model) {
                this._super(controller, model);

                var inputParams = model;

                var leftModuleName = this.get('config.leftHalf.moduleName').camelize();
                var leftController = this.controllerFor(leftModuleName);
                leftController.set('inputParams', inputParams);
                this.render(leftModuleName, {
                    outlet: 'leftHalf',
                    controller: leftController
                });
                var rightModuleName = this.get('config.rightHalf.moduleName').camelize();
                var rightController = this.controllerFor(rightModuleName);
                rightController.set('inputParams', inputParams);
                this.render(rightModuleName, {
                    outlet: 'rightHalf',
                    controller: rightController
                });
            }
            
        }),
        editTeamRoute: Em.Route.extend(authenticationmixin, {
            model: function (params) {
                return $.extend(params, {id: params.match_id});
            }
        }),
        //todo: need to handle multiple routes in a single app

        mapRoutes: function (appInstance, moduleName) {
            appInstance.Router.map(function () {
                this.resource('editTeam', { path: '/edit-team/:match_id' }, function () {
                });
            });
            moduleName = moduleName || 'EditTeam';
            appInstance[moduleName + 'Route'] = this.editTeamRoute.extend({
                moduleName: moduleName
            });
            appInstance[moduleName + 'IndexRoute'] = this.editTeamIndexRoute.extend({
                moduleName: moduleName
            });

        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance, moduleName);
            //moduleName = moduleName || 'EditTeam';

            //var routerName = moduleName + 'IndexRoute';
                /*viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';*/   //currently these two are not in use.

            //appInstance[routerName] = this.editTeamIndexRoute;
        }
    };


});