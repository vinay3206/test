;
define([], function () {
    return {
        leftHalf: {
            moduleName: 'PlayersList',
            dependencies: [
                {
                    moduleName: 'PlayersListController',
                    path: 'Components/PlayersList/PlayersListController'
                },
                {
                    moduleName: 'PlayersListView',
                    path: 'Components/PlayersList/PlayersListView'
                },
                {
                    moduleName: 'PlayerController',
                    path: 'Components/PlayersList/PlayerController'
                },
                {
                    moduleName: 'PlayerView',
                    path: 'Components/PlayersList/PlayerView'
                }
            ],
        },
        rightHalf: {
            moduleName: 'UserTeam',
            dependencies: [
                {
                    moduleName: 'UserTeamController',
                    path: 'Components/UserTeam/UserTeamController'
                },
                {
                    moduleName: 'UserTeamView',
                    path: 'Components/UserTeam/UserTeamView'
                },
                {
                    moduleName: 'TeamPlayerView',
                    path: 'Components/UserTeam/TeamPlayerView'
                }
            ]
        }
    };
});