﻿;
/*require.config({
    paths: {
        homeconfig: 'Apps/HomeApp/homeConfig'
    }
});*/

define([], function () {

    /**
        homeRoute
    */
    return {

        homeIndexRoute: Em.Route.extend({
            model: function () {
                console.log('homeRoute loaded');
                return {};
            },
            beforeModel: function () {
                var loadComponentsPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                    var appInstance = Em.Namespace.byName(window.KhelkundGlobals.AppName);
                    //read config
                    require(['homeconfig'], $.proxy(function (config) {
                        this.set('config', config);
                        var dependencies = [];
                        for (var i = 0; i < config.left.dependencies.length; i++) {
                            dependencies.push(config.left.dependencies[i]);
                        }
                        for (var i = 0; i < config.rightHalf1.dependencies.length; i++) {
                            dependencies.push(config.rightHalf1.dependencies[i]);
                        }
                        for (var i = 0; i < config.rightHalf2.dependencies.length; i++) {
                            dependencies.push(config.rightHalf2.dependencies[i]);
                        }

                        /*var requirePaths = {};
                        //map the required dependencies to their paths using require.config
                        for(var i = 0; i < dependencies.length; i++){
                            
                            //moduleName is the real name on which module is to be registered.
                            //for registering modules on app use instanceModuleName property.
                            requirePaths[dependencies[i].moduleName.toLowerCase()] = dependencies[i].path;
                        }
                        //add these paths to require.config
                        require.config({
                            paths: requirePaths
                        });*/
                        //fetch the mapped paths.
                        var mappedDependencies = dependencies.map(function (item, index) {
                            return Em.get(item, 'moduleName').toLowerCase();
                        });

                        require(mappedDependencies, function () {
                            for (var i = 0; i < dependencies.length; i++) {
                                //register dependencies modules in app
                                //NOTE: their name should not be clashing.
                                //todo: handle the same name case also.
                                appInstance[dependencies[i].instanceModuleName || dependencies[i].moduleName] = arguments[i];
                            }
                            resolve("Components loaded Successfuly");
                        }, function (err) {
                            reject ('Unable to load Dependencies for components', err);
                        });
                    }, this), function (err) {
                        reject ('Unable to load config file for home', err);
                    });


                }, this));
                return loadComponentsPromise;
            },
            renderTemplate: function (controller, model) {
                this._super(controller, model);
                var leftModuleName = this.get('config.left.moduleName').camelize();
                var leftController = this.controllerFor(leftModuleName);
                this.render(leftModuleName, {
                    outlet: 'left',
                    controller: leftController
                });
                var rightHalf1ModuleName = this.get('config.rightHalf1.moduleName').camelize();
                var rightHalf1Controller = this.controllerFor(rightHalf1ModuleName);
                this.render(rightHalf1ModuleName, {
                    outlet: 'rightHalf1',
                    controller: rightHalf1Controller
                });
                var rightHalf2ModuleName = this.get('config.rightHalf2.moduleName').camelize();
                var rightHalf2Controller = this.controllerFor(rightHalf2ModuleName);
                this.render(rightHalf2ModuleName, {
                    outlet: 'rightHalf2',
                    controller: rightHalf2Controller
                });
            }
            
        }),
        //todo: need to handle multiple routes in a single app

        mapRoutes: function (appInstance, moduleName) {
            appInstance.Router.map(function () {
                this.resource('home', { path: '/' }, function () {
                });
            });
            appInstance[ moduleName + "IndexRoute"] = this.homeIndexRoute;
        },

        initModule: function (appInstance, moduleName) {
            moduleName = moduleName || 'Home';
            this.mapRoutes(appInstance, moduleName);

            //additional handling for expanding views if user not logged in.
            appInstance[moduleName + "Controller"] = Em.ObjectController.extend({
                needs: ['application'],
                isUserLoggedIn: function () {
                    return this.get('controllers.application.userInfo') !== null;
                }.property('controllers.application.userInfo')
            });

        }
    };


});