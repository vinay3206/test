﻿;
define([], function () {
    return {
        left: {
            moduleName: 'Tournaments',
            dependencies: [
                {
                    moduleName: 'TournamentsController',
                    path: 'Components/Tournaments/TournamentsController'
                },
                {
                    moduleName: 'TournamentsView',
                    path: 'Components/Tournaments/TournamentsView'
                },
                {
                    moduleName: 'MatchItemView',
                    path: 'Components/Tournaments/MatchItemView'
                }
            ]
        },
        rightHalf1: {
            moduleName: 'UpcommingMatches',
            dependencies: [
                {
                    moduleName: 'UpcommingMatchesController',
                    path: 'Components/UpcommingMatches/UpcommingMatchesController'
                },
                {
                    moduleName: 'UpcommingMatchesView',
                    path: 'Components/UpcommingMatches/UpcommingMatchesView'
                },
                {
                    moduleName: 'UpcommingMatchView',
                    path: 'Components/UpcommingMatches/UpcommingMatchView'
                },
                {
                    moduleName: 'UpcommingMatchController',
                    path: 'Components/UpcommingMatches/UpcommingMatchController'
                }
            ]
        },
        rightHalf2: {
            moduleName: 'ActiveChallenges',
            dependencies: [
                {
                    moduleName: 'ActiveChallengesController',
                    path: 'Components/ActiveChallenges/ActiveChallengesController'
                },
                {
                    moduleName: 'ActiveChallengesView',
                    path: 'Components/ActiveChallenges/ActiveChallengesView'
                },
                {
                    moduleName: 'ActiveChallengeView',
                    path: 'Components/ActiveChallenges/ActiveChallengeView'
                },
                {
                    moduleName: 'ActiveChallengeController',
                    path: 'Components/ActiveChallenges/ActiveChallengeController'
                }
            ]
        }
    };
});