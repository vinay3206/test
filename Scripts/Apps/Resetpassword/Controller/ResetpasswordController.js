﻿;

//require.config({
//    paths: {
       
//    }
//});

define(['ajaxcallstatushandler'], function () {
    

    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

       
        ajaxCallsToRegister: ['resetPassword'],

        init: function () {
            this._super();
            this.registerAjaxCalls();



        },


        actions: {
            resetPassword: function () {
                if (this.get('model').validate()) {
                    var emailId = this.get('emailId');
                    this.get('target').send('resetPassword', emailId);
                }

            }
        }










    });

});