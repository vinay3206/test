﻿;

//require.config({
//    paths: {
//        resetpasswordmodel:'Apps/Resetpassword/Model/ResetpasswordModel'
//    }
//});
define(['ember','khelkundutils','resetpasswordmodel'],
    function (ember, khelkundutils,resetpasswordmodel) {
        
        return Em.Route.extend({

            model: function () {

                return resetpasswordmodel.create();
            },

            actions:  {
                resetPassword: function (emailId, successCallback, errorCallback) {
                    var requestObj = {
                        url: 'Services/LoginService.svc/ResetPassword/' + emailId
                    };
                    var availabilityCallStatus = khelkundutils.ajax.get(requestObj,
                        //success callback
                        $.proxy(function (apiResult) {
                            if (apiResult) {
                                successCallback && successCallback(apiResult);
                            } else {
                                errorCallback && errorCallback(apiResult);
                            }
                        }, this),
                        //failure callback
                        $.proxy(function (error) {
                            //runs only on network error
                            //should not happen
                            console.log("reset password faied. Network error.");
                        }, this)
                    );
                    this.get('controller.ajaxCalls.resetPassword').setPromise(availabilityCallStatus);
                },

            },
         
        });
    });