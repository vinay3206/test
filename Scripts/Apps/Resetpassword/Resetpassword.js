﻿;

//require.config({
//    paths: {
//        resetpasswordroute: 'Apps/Resetpassword/Route/ResetpasswordRoute',
//        resetpasswordcontroller: 'Apps/Resetpassword/Controller/ResetpasswordController',
//    }
//});


define(['resetpasswordroute','resetpasswordcontroller'], function (resetpasswordroute,resetpasswordcontroller) {

    /**
        ResetpasswordRoutemap
        ResetpasswordRouter
        ResetpasswordView
        ResetpasswordController
        ResetpasswordModel
        ResetpasswordDataFixture
        ResetpasswordTemplates    
    */
    return {

        resetpasswordRoute : resetpasswordroute,
        resetpasswordView : Em.View.extend(),
        resetpasswordController : resetpasswordcontroller,

        mapRoutes : function (appInstance) {
            appInstance.Router.map(function () {
                this.route('resetpassword');//, {path:'/resetpassword'});
            });
        },

        initModule : function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Resetpassword';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.resetpasswordRoute;
            appInstance[viewName] = this.resetpasswordView;
            appInstance[controllerName] = this.resetpasswordController;
        }
    }


});