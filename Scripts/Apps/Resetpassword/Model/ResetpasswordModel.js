﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members
       emailId:'',

        //translators

      

        //validations
        validations: {
            emailId: {
                presence: {
                    controlName: 'Email'
                },
                format: {
                    value: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                    controlName: 'Email',
                }
            },
        }
    });
});