﻿;
define(['bs_modalpopupview', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview', 'khelkundutils', 'ajaxcallstatushandler'],
    function (bs_modalpopupview, bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview, khelkundutils, ajaxcallstatushandler) {




        var inviteReferralView = bs_modalpopupview.extend({


            init: function () {
                this._super();
                this.set('handler.parent', this);

            },


            classNames: ['trans_background', 'match-bdr1'],

            headerView: bs_modalpopupheaderview.extend({
                template: null,
                classNames: ['hide']

            }),


            bodyView: bs_modalpopupbodyview.extend({

                //template: null,


                templateName: 'inviteReferralPopUp',
                classNames: ['modal-body'],

                showClose: function () {
                  
                    var istatus = this.get('controller.ajaxCalls.inviteFriends');
                    if ((istatus.get('isFailure')) || istatus.get('isSuccess')) {
                        return true;
                    }
                    return false;

                }.property('controller.ajaxCalls.inviteFriends.status'),

                showCancel: function () {
                  
                    var istatus = this.get('controller.ajaxCalls.inviteFriends');
                    if (istatus.get('inProgress')) {
                        return false;
                    }
                    return true;


                }.property('controller.ajaxCalls.inviteFriends.status')



            }),

            footerView: bs_modalpopupfooterview.extend({

                template: null,
                classNames: ['hide']
            }),

            handler: {
                show: function () {
                    this.parent.popup();
                },
                hide: function () {
                    this.parent.hide();

                }
            },


            actions: {

                close: function () {
                    this.hide();


                },




            },



        });








        return inviteReferralView;
    });