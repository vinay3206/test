﻿
//require.config({
//    paths: {
//        referralinviteview:'Apps/Referral/View/ReferralInviteView',

//    }
//});


define(['fbauth', 'khelkundutils', 'referralinviteview'],


    function (fbauth, khelkundutils, referralinviteview) {

        return Em.View.extend({

            referralInviteView: referralinviteview.extend({

                init: function () {
                    this._super();
                    this.set('parentView.popUpHandler', this.get('handler'));
                },

            }),
            

            actions: {

                referUsingFacebook: function () {

                    var url = window.location.href;
                    var hash = window.location.hash;
                    url=url.replace(hash, "#/registration");
                    url = khelkundutils.ajax.formatUrl({ url: url, params: { rid: this.get('controller.userInfo.id') } });

                    FB.ui({
                        method: 'feed',
                        link: url,
                        caption: 'Join Khelkund and win cash',
                    }, function (response) { });


                },

                referUsingMail: function () {
                    this.get('controller.ajaxCalls.inviteFriends').resetPromise();
        
                    this.get('popUpHandler').show();

                },

            }


        });






    });