﻿;

require.config({
    paths: {

    }
});

define(['ajaxcallstatushandler'], function () {


    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

        needs:['application'],

        ajaxCallsToRegister: ['inviteFriends', 'getReferralCount'],

        userInfo:function(){
            return this.get('controllers.application.userInfo');

        }.property('controllers.application.userInfo'),


        actions: {
            inviteFriends: function () {
                if (this.get('content').validate()) {
                    this.get('target').send('inviteFriends', this.get('content').toServiceModel());
                }

            },
        },


        init: function () {
            this._super();
            this.registerAjaxCalls();



        },


        









    });

});