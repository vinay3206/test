﻿;

//require.config({
//    paths: {
//        referralroute: 'Apps/Referral/Route/ReferralRoute',
//        referralcontroller: 'Apps/Referral/Controller/ReferralController',
//        referralview: 'Apps/Referral/View/ReferralView'
//    }
//});


define(['referralroute', 'referralcontroller', 'referralview'], function (referralroute, referralcontroller, referralview) {

    /**
        ReferralRoutemap
        ReferralRouter
        ReferralView
        ReferralController
        ReferralModel
        ReferralDataFixture
        ReferralTemplates    
    */
    return {

        referralRoute: referralroute,
        referralView: referralview,
        referralController: referralcontroller,

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.route('referral');//, {path:'/referral'});
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Referral';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.referralRoute;
            appInstance[viewName] = this.referralView;
            appInstance[controllerName] = this.referralController;
        }
    }


});