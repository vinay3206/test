﻿;

//require.config({
//    paths: {
//        referralmodel: 'Apps/Referral/Model/ReferralModel'
//    }
//});
define(['ember', 'khelkundutils', 'referralmodel', 'authenticationmixin'],
    function (ember, khelkundutils, referralmodel, authenticationmixin) {

        return Em.Route.extend(authenticationmixin, {

            model: function () {


                return referralmodel.create();

            },



            setupController: function (controller,model) {
                this._super(controller, model);
                var promise = this.getReferralCount();
                this.send('registerPromiseforLoader', promise);
                promise.then($.proxy(function (apiResult) {
                    controller.get('content').toLocalModel(apiResult);
                },this));
                controller.get('ajaxCalls.getReferralCount').setPromise(promise);
            },


            getReferralCount: function () {

                var promise = new Em.RSVP.Promise($.proxy(function (resolve,reject) {
                    khelkundutils.ajax.get({
                        url: "Services/ProfileService.svc/ReferralCount",
                    }, $.proxy(function (apiResult) {
                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                            resolve(apiResult);
                        }
                        else {
                            reject(apiResult);
                        }

                    }, this), $.proxy(function (err) {
                        reject(err);

                    }, this));


                }, this));

                return promise;
            },


            actions: {
                inviteFriends: function (data) {

                    var reqObj = {
                        url: 'Services/ProfileService.svc/ReferUsers',
                        data: data
                    }
                    var invitePromise = khelkundutils.ajax.post(reqObj,
                                    //Success xhr
                                    $.proxy(function (apiResult) {
                                        //validate 
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {

                                            // set selected challenge id
                                        } else {
                                            //reject promise

                                        }
                                    }, this),
                                    //failure xhr
                                    $.proxy(function (err) {

                                    }, this));
                    this.get('controller').get('ajaxCalls.inviteFriends').setPromise(invitePromise);


                },
            }
               
               

        });
    });