﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        //members

        refCount:0,

        msg:'',

        emails: '',

        emailList: function () {
            return this.get('emails').split(',');

        }.property('email'),
        //translators

        toServiceModel: function () {
            var serviceModel = {

                EmailIds: this.get('emailList'),
                Message:null,

            };
            return serviceModel;
        },


        toLocalModel:function(serviceObject){
            this.set('refCount', serviceObject.ReferralCount);


        },



        //validations
        validations: {
            emails: {
                presence: {
                    controlName: 'email',
                },

            },


           

        }
    });
});