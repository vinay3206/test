;
/**
 * @module userchallengesconfig
 * @return {JSONObject} object contains the components to load in the outlets defined by the app template.
 */
define([], function () {
	return {
		leftHalf: {
			moduleName: 'UserChallengeStatus',
			dependencies: [
				{
					instanceModuleName: 'UserChallengeStatusController',
					moduleName: 'UserChallengeStatusController',
					path: 'Components/UserChallengeStatus/UserChallengeStatusController'
				},
				{
					instanceModuleName: 'UserChallengeStatusView',
					moduleName: 'UserChallengeStatusView',
					path: 'Components/UserChallengeStatus/UserChallengeStatusView'
				}
			]
		},
		rightHalf: {
			moduleName: 'UserChallengeTeam',
			dependencies: [
				{
					instanceModuleName: 'UserChallengeTeamController',
					moduleName: 'UserChallengeTeamController',
					path: 'Components/UserChallengeTeam/UserChallengeTeamController'
				},
				{
					instanceModuleName: 'UserChallengeTeamView',
					moduleName: 'UserChallengeTeamView',
					path: 'Components/UserChallengeTeam/UserChallengeTeamView'
				},
				{
					//this module dont need instanceModuleName cuz it is not used in renderTemplate as it 
					//is not a main module.
					//instanceModuleName: '',
					moduleName: 'TeamPlayerView',
					path: 'Components/UserTeam/TeamPlayerView'
				}
			]
		},
		bottomFull: {
			moduleName: 'UserChallengeScorecard',
			dependencies: [
				{
					instanceModuleName: 'UserChallengeScorecardController',
					moduleName: 'ScorecardController',
					path: 'Components/Scorecard/ScorecardController'
				},
				{
					instanceModuleName: 'UserChallengeScorecardView',
					moduleName: 'ScorecardView',
					path: 'Components/Scorecard/ScorecardView'
				}
			]
		}
	};
});