;
/*require.config({
	paths: {
		userchallengesconfig: 'Apps/UserChallenges/userChallengesConfig'
	}
});*/
/** @module userchallengesapp */
define(['ember', 'authenticationmixin', 'tournamentmodel', 'userchallengemodel', 'ajaxcallstatushandler', 'match'],
	function (ember, authenticationmixin, tournamentmodel, userchallengemodel, ajaxcallstatushandler, match) {

		return {
			/** Overrides the model hook to return the params received to be used by lower routes. */
			userChallengesRoute: Em.Route.extend(authenticationmixin, {
				/** Model hook of ember. Recieves params provide from url and returns them as is. */
				model: function (params) {
					/**
					 * Return params received from the url. As those params will be used by 
					 * lower controllers for fetching their respective data.
					 * @example
					 *	params = {
					 *		match_id: 'somebignumber',
					 *		challenge_id: 'anotherbignumber'
					 *	}
					 */
					return params;
				}
			}),
			/**
			 * Customization of indexRoute to render the temlates and provide them their respective controller
			 * as per config file provided. Loads the required modules and renders them in the outlets.
			 */
			userChallengesIndexRoute: Em.Route.extend({
				model: function () {
					/**
					 * Read the model of parent resource. As parent modl will be having all the information
					 * regarding the match_id and challenge_id.
					 */
					return this.modelFor(this.get('moduleName').camelize());
				},
				/**
				 * Loads all the depedencies reading its config file.
				 * @return {RSVP.Promise} Returns the promise used to load all the depenencies.
				 */
				beforeModel: function () {
					/**
					 * Gets appInstance from ember Namespace.byName method. Although we can directely
					 * provide "KhelkundApp" as name but since name is customizable thus not using it directely.
					 * @type {EmberApplication}
					 */
					var loadComponentsPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
						var appInstance = Em.Namespace.byName(window.KhelkundGlobals.AppName);
						//read config file
						require(['userchallengesconfig'], $.proxy(function (config) {
							//save config for use by other functions of this router. e.g. renderTemplate
							this.set('config', config);

							//get dependencies to load
							var dependencies = [];
							dependencies = config.leftHalf.dependencies.concat(config.rightHalf.dependencies)
												.concat(config.bottomFull.dependencies);

							/*var requirePaths = {};
							//map the required dependencies to their paths using require.config
							for(var i = 0; i < dependencies.length; i++){
								
								//moduleName is the real name on which module is to be registered.
								//for registering modules on app use instanceModuleName property.
								//Convert them to loweCase cuz as per our convertion we register with 
								//all small alphabets
								requirePaths[dependencies[i].moduleName.toLowerCase()] = dependencies[i].path;
							}
							//add these paths to require.config
							require.config({
								paths: requirePaths
							});*/
							//fetch the mapped paths.
							var mappedDependencies = dependencies.map(function (item, index) {
								return Em.get(item, 'moduleName').toLowerCase();
							});
							require(mappedDependencies, function () {
								//arguments field will be used to fetch the dependencies.
								for (var i = 0; i < arguments.length; i++) {
									//using instanceModuleName for instanciating module.
									//but adding fall back in case instanceModuleName is not provided.	
									appInstance[dependencies[i].instanceModuleName || dependencies[i].moduleName] = arguments[i];
								}
								resolve("Components for userchallengesapp loaded successfuly.");
							}, function (err) {
								reject({
									error: err,
									message: 'Error loading components for userchallengesapp'
								});
							});
						}, this), function (err) {
							reject({
								error: err,
								message: "Error loading config file for userchallengesapp."
							});
						});
					}, this));

					return loadComponentsPromise;
				},
				setupController: function (controller, model) {
					this._super(controller, model);
				},
				/**
				 * Overrides the renderTemplate hook of ember router. Used to render the outlets
				 * present in the template using the config file.
				 * @param  {EmberObject} controller Controller instance of userChallengesIndexRoute.
				 * @param  {EmberObject} model      Model of current route/controller.
				 */
				renderTemplate: function (controller, model) {
					this._super(controller, model);
					/** @type {Object} The parameters received from url hash are provided here.*/
					var inputParams = model;

					//clear some default values
					controller.setProperties({
						selectChallenge: null,
						//reset selectedMatch also.
						_selectedMatch: null,
						//initialize selected challenge with the challenge id procided in url
						defaultSeriesId: '',
						defaultChallengeId: model.challenge_id,
						defaultMatchId: model.match_id
					});
					var indexControllerName = this.get('moduleName').camelize() + 'Index';


					var leftModuleName = this.get('config.leftHalf.moduleName');
					//validation check. If modulename is not given don't load it.
					if(leftModuleName){
						leftModuleName = leftModuleName.camelize();
						var leftController = this.controllerFor(leftModuleName);
						if(leftController){
							leftController.set('inputParams', inputParams);
							leftController.reopen({
								needs: [indexControllerName],
								selectedTournament: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedTournament'),
								selectedMatch: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedMatch'),
								selectedChallenge: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedChallenge'),
								areNoChallengesJoined: function () {
									if (this.get('controllers.' + indexControllerName + '.challengesList.length')) {
										return false;
									}
									return true;
								}.property('controllers.' + indexControllerName + '.challengesList.length')
							});
						}
						this.render(leftModuleName, {
							outlet: 'leftHalf',
							controller: leftController
						});
					}

					//validation check. If modulename is not given don't load it.
					var rightModuleName = this.get('config.rightHalf.moduleName');
					if(rightModuleName){
						rightModuleName = rightModuleName.camelize();
						var rightController = this.controllerFor(rightModuleName);
						if(rightController){
							rightController.set('inputParams', inputParams);
							rightController.reopen({
								needs: [indexControllerName],
								selectedTournament: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedTournament'),
								selectedMatch: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedMatch'),
								selectedChallenge: Em.computed.alias('controllers.' + indexControllerName + '.selectedChallenge'),
								areNoChallengesJoined: function () {
									if (this.get('controllers.' + indexControllerName + '.challengesList.length')) {
										return false;
									}
									return true;
								}.property('controllers.' + indexControllerName + '.challengesList.length')
							});
						}
						this.render(rightModuleName, {
							outlet: 'rightHalf',
							controller: rightController
						});
					}

					//validation check. If modulename is not given don't load it.
					var bottomModuleName = this.get('config.bottomFull.moduleName');
					if(bottomModuleName){
						bottomModuleName = bottomModuleName.camelize();
						var bottomController = this.controllerFor(bottomModuleName);
						if(bottomController){
							bottomController.set('inputParams', inputParams);
							bottomController.reopen({
								needs: [indexControllerName],
								selectedTournament: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedTournament'),
								selectedMatch: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedMatch'),
								selectedChallenge: Em.computed.oneWay('controllers.' + indexControllerName + '.selectedChallenge'),
								areNoChallengesJoined: function () {
									if (this.get('controllers.' + indexControllerName + '.challengesList.length')) {
										return false;
									}
									return true;
								}.property('controllers.' + indexControllerName + '.challengesList.length')
							});
						}
						this.render(bottomModuleName, {
							outlet: 'bottomFull',
							controller: bottomController
						});
					}
				},

				actions: {
					updateInputParams: function (inputParams) {
						var leftModuleName = this.get('config.leftHalf.moduleName');
						//validation check. If modulename is not given don't load it.
						if(leftModuleName){
							leftModuleName = leftModuleName.camelize();
							var leftController = this.controllerFor(leftModuleName);
							if(leftController){
								leftController.set('inputParams', inputParams);
							}
						}

						//validation check. If modulename is not given don't load it.
						var rightModuleName = this.get('config.rightHalf.moduleName');
						if(rightModuleName){
							rightModuleName = rightModuleName.camelize();
							var rightController = this.controllerFor(rightModuleName);
							if(rightController){
								rightController.set('inputParams', inputParams);
							}
						}

						//validation check. If modulename is not given don't load it.
						var bottomModuleName = this.get('config.bottomFull.moduleName');
						if(bottomModuleName){
							bottomModuleName = bottomModuleName.camelize();
							var bottomController = null;//this.controllerFor(bottomModuleName);
							if(bottomController){
								bottomController.set('inputParams', inputParams);
							}
						}
					}
				}
			}),

			/**
			 * Maps the required routes for this page hash. Creates a resource 'userChallenges'.
			 * It also assigns the route defination to appInstance.
			 * @method mapRoutes
			 * @param  {Object} appInstance KhelkundApp instance.
			 * @param  {String} moduleName  Name of the module to use.
			 */
			mapRoutes: function (appInstance, moduleName) {
				appInstance.Router.map(function () {
					//defines a route with parameters as match id and challenge id for that match.
					this.resource('userChallenges', {path: '/user-challenges/:match_id/:challenge_id'}, function () {});
				});

				appInstance[moduleName + "Route"] = this.userChallengesRoute;
				appInstance[moduleName + "IndexRoute"] = this.userChallengesIndexRoute.extend({
					/** @type {String} Provide moduleName to router to help him initialize outlets from configs. */
					moduleName: moduleName
				});
			},
			/**
			 * Initializes Joined Challenges page. This module is calle by appsloader.js.
			 * @method initModule
			 * @param  {Object} appInstance KhelkundApp instance.
			 * @param  {String} [moduleName='UserChallenges']  Name of the module to use.
			 */
			initModule: function (appInstance, moduleName) {
				moduleName = moduleName || 'UserChallenges';
				this.mapRoutes(appInstance, moduleName);

				appInstance[moduleName + "IndexController"] = Em.ObjectController.extend(ajaxcallstatushandler, {
					ajaxCallsToRegister: ['getChallengesList'],
					//initial value of challenge_id to be provided to the challenge list.
					defaultChallengeId: null,
					//default match id. Used for fetching match details.
					defaultMatchId: null,
					//propeties for use by breadcrumb
					/**
					 * Calculates itself from the selectedMatch object.
					 * @return {Object} Details for the selected tournament.
					 */
					selectedTournament: function () {
						if(this.get('selectedMatch.seriesInfo')) {
							return this.get('selectedMatch.seriesInfo');
						}
						return {};
					}.property('selectedMatch.seriesInfo').readOnly(),
					/*tournamentsList: function () {
						var tournamentsList = [];
						//fetch tournaments
						var promise = tournamentmodel.fetch();
						promise.then($.proxy(function (tournaments) {
							tournamentsList.pushObjects(tournaments.data);

							//temp code to be removed ASAP
							if(this.get('selectedTournament') === null && this.get('defaultMatchId')) {
								//provide default selection based on default selected match id
								var availableTournament = tournamentsList.find(function(item, index) {
									for (var i=0; i< item.get('matches').length; i++) {
										if(item.get('matches')[i].get('id') === this.get('defaultMatchId')) {
											return true;
										}
									}
									return false;
								}, this);

								if(availableTournament) {
									this.set('defaultSeriesId', availableTournament.get('id'));
								}
							}

						}, this));
						//register this call for loader display
						this.send('registerPromiseforLoader', promise);

						return tournamentsList;
					}.property('defaultMatchId').readOnly(),*/
					_selectedMatch: null,
					/**
					 * Makes an ajax call depending on the provided defaultMatchId.
					 * @return {Object} Object containing the match details as per
					 *                         defaultMatchId. It also contains some
					 *                         info about the series it belongs to.
					 */
					selectedMatch: function () {
						if(this.get('defaultMatchId') !== undefined &&
							this.get('defaultMatchId') !== null &&
							this.get('_selectedMatch.id') !== this.get('defaultMatchId')) {

							var promise = match.fetch({
								match_id: this.get('defaultMatchId'),
								apiUrl: 'Services/CricManagerService.svc/Match/{match_id}'
							});
							promise.then($.proxy(function (matchResponse) {
								this.set('_selectedMatch', matchResponse.data);
							}, this));
						}
						return this.get('_selectedMatch');
					}.property('defaultMatchId', '_selectedMatch').readOnly(),
					/*matchList: function () {
						var matchList = [];
						if(this.get('selectedTournament')) {
							this.get('tournamentsList').forEach(function (tournamentItem, index) {
								if(this.get('selectedTournament.id') === tournamentItem.get('id')) {
									matchList.pushObjects(tournamentItem.get('matches'));
								}
							},this);
						}
						return matchList;
					}.property('selectedTournament').readOnly(),*/
					selectedChallenge: null,
					challengesList: function () {
						var challengesList = [];
						if(this.get('selectedMatch.id')) {
							var promise = userchallengemodel.fetchJoinedChallenges(this.get('selectedMatch.id'));
							promise.then($.proxy(function (challenges) {
								challengesList.pushObjects(challenges.data);
							}, this), function (err) {
								//getchallenges call failed.
								console.log(err);
							});

							//register this ajaxcall for some other handling.
							this.get('ajaxCalls.getChallengesList').setPromise(promise);

							//show loader for challenges list fetching promise also
							this.send('registerPromiseforLoader', promise);
						}
						return challengesList;
					}.property('selectedMatch.id').readOnly(),
					onSelectedChallengeChange: function () {
						//run.next to make sure all the observers are fired before redirecting.
						Em.run.next(this, function () {
							if(this.get('ajaxCalls.getChallengesList.status') === 'isSuccess'){
								if(this.get('selectedChallenge') && this.get('selectedMatch')) {
									//do not transition if currently in same route.
									if(this.get('selectedMatch.id') && this.get('selectedMatch.id') === this.get('match_id')){
										
										if(this.get('selectedChallenge.id') && this.get('selectedChallenge.id') === this.get('challenge_id')){
											return false;
										}
									}

									Em.run.next(this, function () {
										this.transitionToRoute('userChallenges', {
											match_id: this.get('selectedMatch.id'),
											challenge_id: this.get('selectedChallenge.id')
										});
										/*this.send('updateInputParams', {
											match_id: this.get('selectedMatch.id') || this.get('match_id'),
											challenge_id: this.get('selectedChallenge.id') || this.get('challenge_id')
										});*/
									});
								}
							}
						});
					}.observes('selectedChallenge', 'selectedMatch', 'ajaxCalls.getChallengesList.status'),
					//proerties for breadcrumb ends
					
					actions: {
						declareRefreshEvent: function () {
							$('body').trigger('refreshAllData.user-challenge');
						}
					}
				});

				appInstance[moduleName + 'IndexView'] = Em.View.extend({
					/**
					 * Id to store the timer.
					 * Needed to clear timer in case of match refresh.
					 * @type {String}
					 */
					timerId: null,
					/**
					 * Sets timer for page refresh depending on match deadline time.
					 * The main use of this function is to refresh the page to update its all values and challenges status.
					 * As many key operations depends on challenge status.
					 */
					setupTimerForRefresh: function () {
						if(this.get('controller.selectedMatch')) {
							var matchDeadline = new Date(this.get('controller.selectedMatch.deadline'));
							var timeLeft = matchDeadline.getTime() - new Date().getTime();
							if(timeLeft > 0) {
								//add 5 seconds to make a call.
								timeLeft += 5000;
								//clear last timerId
								if(this.get('timerId')) {
									Em.run.cancel(this.get('timerId'));
								}
								//schedule the refresh event for the time difference.
								this.set('timerId', Em.run.later(this, function () {
									this.get('controller').send('declareRefreshEvent');
								}, timeLeft));
							}

							
						}
					}.observes('controller.selectedMatch'),

					/**
					 * TimerId for the autorefresh event
					 * @type {[type]}
					 */
					autoRefreshTimer: null,
					/**
					 * Interval in which autorefresh event will be called. Its unit is 
					 * in `seconds`.
					 * @type {Number}
					 */
					autoRefreshInterval: 300,
					/**
					 * Function to setup autorefresh timer after some waiting time after match starts.
					 */
					setupTimerForAutoRefresh: function () {
						var timeLeft = this.get('autoRefreshInterval') * 1000;
						if(timeLeft > 0) {
							//clear last autoRefreshTimer
							if(this.get('autoRefreshTimer')) {
								Em.run.cancel(this.get('autoRefreshTimer'));
							}
							//schedule the refresh event for the time difference.
							this.set('autoRefreshTimer', Em.run.later(this, function () {
								this.get('controller').send('declareRefreshEvent');
								//set timer again as it is an circular event
								Em.run.next(this, function () {
									this.setupTimerForAutoRefresh();
								});
							}, timeLeft));
						}
					},
					/**
					 * Delay give to setupTimerForAutoRefresh because of some delay in updating of API call.
					 * Its unit is in seconds
					 * @type {Number}
					 */
					autoRefreshDelay: 900,
					/**
					 * TimerId for the delay timer for autorefresh. This timer
					 * is used to setup timer for delay in setting up timer for
					 * autorefresh start event.
					 * @type {Number}
					 */
					autoRefreshDelayTimer: null,
					/**
					 * Observer to setup delay for autorefresh timer.
					 * Sets up timer as per match start date and autoRefreshDelayTimer
					 */
					setupAutoRefreshTimerDelay: function () {
						if(this.get('controller.selectedMatch')) {
							var matchStartTime = new Date(this.get('controller.selectedMatch.date'));
							var timeLeft = matchStartTime.getTime() - new Date().getTime() + (this.get('autoRefreshDelay') * 1000);
							if(timeLeft < 0) {
								timeLeft = 0;
							}
							
							//add delay to make a call.
							timeLeft += this.get('autoRefreshDelay');
							//clear last autoRefreshDelayTimer
							if(this.get('autoRefreshDelayTimer')) {
								Em.run.cancel(this.get('autoRefreshDelayTimer'));
							}
							//schedule the refresh event for the time difference.
							this.set('autoRefreshDelayTimer', Em.run.later(this, function () {
								this.setupTimerForAutoRefresh();
							}, timeLeft));
						}
					}.observes('controller.selectedMatch'),

					/**
					 * Clears all the pending timers if present setup by this view.
					 */
					clearPendingTimers: function () {
						if(this.get('timerId')){
							Em.run.cancel(this.get('timerId'));
						}
						if(this.get('autoRefreshTimer')) {
							Em.run.cancel(this.get('autoRefreshTimer'));
						}
						if(this.get('autoRefreshDelayTimer')) {
							Em.run.cancel(this.get('autoRefreshDelayTimer'));
						}
					}.on('willDestroyElement'),

					actions: {
						declareRefreshEvent: function () {
							//reset autorefresh timer first
							this.setupTimerForAutoRefresh();
							
							this.get('controller').send('declareRefreshEvent');
						}
					}
				});

			}
		};

	});