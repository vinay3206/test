﻿;
require.config({
    paths: {
        testconfig: 'Apps/HomeApp/homeConfig'
    }
});

define([], function () {

    /**
        TestRoute
    */
    return {

        testRoute: Em.Route.extend({
            model: function () {
                console.log('testRoute loaded');
                return {};
            },
            beforeModel: function () {
                var loadComponentsPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                    var appInstance = Em.Namespace.byName(window.KhelkundGlobals.AppName);
                    //read config
                    require(['testconfig'], $.proxy(function (config) {
                        this.set('config', config);
                        var dependencies = [];
                        for (var i = 0; i < config.left.dependencies.length; i++) {
                            dependencies.push(config.left.dependencies[i]);
                        }
                        for (var i = 0; i < config.right.dependencies.length; i++) {
                            dependencies.push(config.right.dependencies[i]);
                        }
                        require(dependencies.mapBy('path'), function () {
                            for (var i = 0; i < dependencies.length; i++) {
                                //register dependencies modules in app
                                //NOTE: their name should not be clashing.
                                //todo: handle the same name case also.
                                appInstance[dependencies[i].moduleName] = arguments[i];
                            }
                            resolve("Components loaded Successfuly");
                        }, function (err) {
                            reject ('Unable to load Dependencies for components', err);
                        });
                    }, this), function (err) {
                        reject ('Unable to load config file for test', err);
                    });


                }, this));
                return loadComponentsPromise;
            },
            renderTemplate: function (controller, model) {
                this._super(controller, model);
                var leftModuleName = this.get('config.left.moduleName').camelize();
                var leftController = this.controllerFor(leftModuleName);
                this.render(leftModuleName, {
                    outlet: 'left',
                    controller: leftController
                });
                var rightModuleName = this.get('config.right.moduleName').camelize();
                var rightController = this.controllerFor(rightModuleName);
                this.render(rightModuleName, {
                    outlet: 'right',
                    controller: rightController
                });
            }
            
        }),
        //todo: need to handle multiple routes in a single app

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.resource('test', { path: '/test' }, function () {
                });
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Test';

            var routerName = moduleName + 'IndexRoute',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.testRoute;
        }
    };


});