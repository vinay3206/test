﻿;
define([], function () {
    return {
        left: {
            moduleName: 'TestList',
            dependencies: [
                {
                    moduleName: 'TestListController',
                    path: 'Components/TestListDisplay/TestListController'
                },
                {
                    moduleName: 'TestListView',
                    path: 'Components/TestListDisplay/TestListView'
                }
            ],
        },
        right: {
            moduleName: 'ActiveChallenges',
            dependencies: [
                {
                    moduleName: 'ActiveChallengesController',
                    path: 'Components/ActiveChallenges/ActiveChallengesController'
                },
                {
                    moduleName: 'ActiveChallengesView',
                    path: 'Components/ActiveChallenges/ActiveChallengesView'
                },
                {
                    moduleName: 'ActiveChallengeView',
                    path: 'Components/ActiveChallenges/ActiveChallengeView'
                },
                {
                    moduleName: 'ActiveChallengeController',
                    path: 'Components/ActiveChallenges/ActiveChallengeController'
                }
            ]
        },
        leftHalf: {
            moduleName: 'Tournaments',
            dependencies: [
                {
                    moduleName: 'TournamentsController',
                    path: 'Components/Tournaments/TournamentsController'
                },
                {
                    moduleName: 'TournamentsView',
                    path: 'Components/Tournaments/TournamentsView'
                },
                {
                    moduleName: 'MatchView',
                    path: 'Components/Tournaments/MatchView'
                }
            ]
        },
        hidden: {

        }
    };
});