﻿define(['validationmodel'], function (validationmodel) {
    return validationmodel.extend(Ember.Validations, {
        
        // members


        playerId:null,
        playerType:null,
        ballsFaced:0,
        fours:0,
        sixes:0,
        strikeRate:0,
        economyRate:0,
        oversBowled:0,
        wickets:0,
        maidens:0,
        runsGiven:0,
        runsScored:0,
        catches:0,
        runOuts:0,
        directRunOuts:0,
        isOutOnDuck:false,
        stumping:0,
        isManOfTheMatch:false,





        toServiceModel:function(){
            var serviceObj={
                PlayerId: this.get('playerId'),
                PlayerType: this.get('playerType'),
                BallsFaced: this.get('ballsFaced'),
                Fours: this.get('fours'),
                Sixes: this.get('sixes'),
                StrikeRate: this.get('strikeRate'),
                EconomyRate: this.get('economyRate'),
                OversBowled: this.get('oversBowled'),
                Wickets: this.get('wickets'),
                Maidens: this.get('maidens'),
                RunsGiven: this.get('runsGiven'),
                RunsScored: this.get('runsScored'),
                Catches: this.get('catches'),
                RunOuts: this.get('runOuts'),
                DirectRunOuts:this.get('directRunOuts'),
                OutOnDuck: this.get('isOutOnDuck'),
                Stumpings: this.get('stumping'),
                ManOfTheMatch:this.get('isManOfTheMatch'),





            }
            return serviceObj;

        }



        
    });
});