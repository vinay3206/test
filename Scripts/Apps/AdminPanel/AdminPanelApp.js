;
require.config({
	paths: {
		adminpanelview: 'Apps/AdminPanel/AdminPanelView'
	}
});
/**
 * @module Apps/AdminPanel/AdminPanelApp
 */
define(['ember', 'bootstrap3', 'docs', 'adminpanelview','modifymatchroute','modifymatchview','modifymatchcontroller'],
	function (ember, bootstrap3, docsm, adminpanelview, modifymatchroute, modifymatchview, modifymatchcontroller) {
		return {
			addSeriesRoute: Em.Route.extend({
			}),
			modifyMatchRoute: modifymatchroute,
			modifyMatchController: modifymatchcontroller,
            modifyMatchView:modifymatchview,
			
			editScorecardRoute: Em.Route.extend({
				model: function (params) {
					return {matchId: params.match_id};
				}
			}),
			mapRoutes: function(appInstance, moduleName) {
				appInstance.Router.map(function () {
					this.route('addSeries', {path: '/add-series'});
					this.route('modifyMatch',{path: '/modify-match'});
					this.route('editScorecard', {path: '/edit-scorecard/:match_id'});
				});

				appInstance.AddSeriesRoute = this.addSeriesRoute;
				appInstance.ModifyMatchRoute = this.modifyMatchRoute;
				appInstance.ModifyMatchVie = this.modifyMatchView;
				appInstance.ModifyMatchController = this.modifyMatchController;
				appInstance.EditScorecardRoute = this.editScorecardRoute;
			},
			initModule: function (appInstance, moduleName) {
				moduleName = moduleName || 'AdminPanel';
				this.mapRoutes(appInstance, moduleName);

				appInstance.ApplicationView = adminpanelview;
			}
		};
	});