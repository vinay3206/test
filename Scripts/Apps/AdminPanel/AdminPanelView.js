;
/**
 * @module adminpanelview
 */
define(['ember', 'holder'],
	function (ember, holder) {
		return Em.View.extend({
			runPlaceHolderLoader: function () {
				holder.run();
			}.on('didInsertElement')
		});

	});