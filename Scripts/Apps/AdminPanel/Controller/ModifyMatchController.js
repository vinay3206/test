﻿;

//require.config({
//    paths: {

//    }
//});

define(['ajaxcallstatushandler', 'khelkundutils', 'player'], function (ajaxcallstatushandler, khelkundutils,player) {


    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {


        ajaxCallsToRegister: ['getTournaments', 'startMatch','completeMatch','updateScore'],

        init: function () {
            this._super();
            this.registerAjaxCalls();



        },


        // all tournaments to show,
        tournaments: [],

        // selected value of tournament 
        selectedTournament: null,

        // onmatch name list change set selectedmatch as 0th 
        OnMatchNameListChange: function () {
            var list = this.get('matchNameList');
            if (list.length > 0) {
                this.set('selectedMatch', list[0].value);
            }

        }.observes('matchNameList'),

        // current selected match data will be set through observer
        match: null,

        // list of tournaments for dropdown
        tournamentNameList: function () {
            var tournaments = this.get('tournaments');
            var finalList = [];
            $.each(tournaments, function (index, val) {
                var obj = {
                    text: val.name,
                    value: val.id,
                };
                finalList.pushObject(obj);
            });
            return finalList;

        }.property('tournaments'),


        // list of matches for dropdown
        matchNameList: function () {
            var tournaments = this.get('tournaments');
            var selectedTournament = tournaments.findProperty('id', this.get('selectedTournament'));
            if (tournaments && !selectedTournament) {
                selectedTournament = tournaments[0];
            }
            var finalList = [];
            if (selectedTournament && selectedTournament.matches.length) {
                for (i = 0; i < selectedTournament.matches.length ; i++) {
                    var val = selectedTournament.matches[i];
                    var obj = {
                        text: val.homeTeam.name + " Vs " + val.awayTeam.name,
                        value: val.id,
                    };
                    finalList.pushObject(obj);
                }


            }
            return finalList;

        }.property('selectedTournament', 'tournaments'),

       

        onSelectedMatchChange: function () {
            // transition to new route for the match
            if (this.get('selectedMatch')) {
                this.getPlayersForMatch(this.get('selectedMatch'));
            }


        }.observes('selectedMatch'),


        onSelectedPlayerChange: function () {
            // transition to new route for the match
            if (this.get('selectedPlayer')) {
                var player = this.get('playerList').findProperty('id', this.get('selectedPlayer'));
                this.get('selectedPlayerScoreCard').set('playerId', player.get('id'));
                this.get('selectedPlayerScoreCard').set('playerType', player.get('category'));
            }


        }.observes('selectedPlayer'),





        playerNameList: function () {
            var playerList = this.get('playerList');
            
            var finalList = [];
            if (playerList && playerList.length) {
                for (i = 0; i < playerList.length ; i++) {
                    var val = playerList[i];
                    var obj = {
                        text: val.firstName + ' ' + val.lastName + ' ' + val.category,
                        value: val.id,
                    };
                    finalList.pushObject(obj);
                }


            }
            return finalList;

        }.property('playerList','playerList.@each'),


        // playerList:null set by this function
        getPlayersForMatch: function (selectedMatch) {

           
            var promise = player.fetch({ match_id: selectedMatch });
            promise.then($.proxy(function (apiResult) {
                this.set('playerList', apiResult.data);
                
            },this));
        },







    });

});