﻿;

//require.config({
//    paths: {
//        loginmodel:'Apps/Login/Model/LoginModel'
//    }
//});
define(['ember', 'khelkundutils', 'tournamentmodel', 'footballplayerscoremodel'],
    function (ember, khelkundutils, tournamentmodel,footballplayerscoremodel) {

        return Em.Route.extend({

           


            model: function () {
                return {

                    selectedPlayerScoreCard: footballplayerscoremodel.create(),
                };

            },

            setupController: function (controller, model) {
                this._super(controller, model);
                this.getTournaments();
                

            },



            getTournaments: function () {
                //fetch tournaments
                var promise = tournamentmodel.fetchAll();
                this.get('controller').get('ajaxCalls.getTournaments').setPromise(promise);
                
                promise.then($.proxy(function (apiResult) {
                    this.get('controller').set('tournaments', apiResult.data);
                }, this));

            },


            actions: {

                startMatch: function () {

                    var startMatchStatus = khelkundutils.ajax.get({
                        url: 'Services/CricManagerService.svc/StartMatch/' + this.get('controller.selectedMatch'),
                       
                    },
                                   $.proxy(function (apiResult) {
                                       if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                          alert("DONE")
                                       }
                                       else {
                                           alert("Failure");
                                       }
                                   }, this),
                                   $.proxy(function (err) {
                                       console.log('Failure');
                                       alert("Failure");
                                   }, this));
                    this.get('controller.ajaxCalls.startMatch').setPromise(startMatchStatus);
                    
                },

                completeMatch: function () {

                    var completeMatchStatus = khelkundutils.ajax.get({
                        url: 'Services/CricManagerService.svc/CompleteMatch/'+this.get('controller.selectedMatch'),

                    },
                                    $.proxy(function (apiResult) {
                                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                            alert("DONE");
                                        }
                                        else {

                                            alert("Failure");
                                        }
                                    }, this),
                                    $.proxy(function (err) {
                                        console.log('Failure');
                                        alert("Failure")
                                    }, this));
                    this.get('controller.ajaxCalls.completeMatch').setPromise(completeMatchStatus);
                },

                updateScore: function () {
                    var updateStatus = khelkundutils.ajax.post({
                        url: 'Services/CricManagerService.svc/UpdatePlayerScore',
                        data: {
                            PlayerId: this.get('controller.selectedPlayerScoreCard.playerId'),
                            MatchId: this.get('controller.selectedMatch'),
                            PlayerStats: this.get('controller.selectedPlayerScoreCard').toServiceModel()
                        }
                    },
                                   $.proxy(function (apiResult) {
                                       if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                           alert("Done");
                                       }
                                       else {
                                           alert("Failure");
                                       }
                                   }, this),
                                   $.proxy(function (err) {
                                       console.log('Failure');
                                   }, this));
                    this.get('controller.ajaxCalls.updateScore').setPromise(updateStatus);

                },

            }

        });

    });