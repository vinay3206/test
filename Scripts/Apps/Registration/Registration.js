﻿;

//require.config({
//    paths: {
//        registrationroute: 'Apps/Registration/Route/RegistrationRoute',
//        registrationcontroller: 'Apps/Registration/Controller/RegistrationController',
//        registrationview: 'Apps/Registration/View/RegistrationView'
//    }
//});


define(['registrationroute', 'registrationcontroller', 'registrationview'], function (registrationroute, registrationcontroller, registrationview) {

    /**
        RegistrationRoutemap
        RegistrationRouter
        RegistrationView
        RegistrationController
        RegistrationModel
        RegistrationDataFixture
        RegistrationTemplates    
    */
    return {

        registrationRoute : registrationroute,
        registrationView: registrationview,
        registrationController : registrationcontroller,

        mapRoutes : function (appInstance) {
            appInstance.Router.map(function () {
                this.route('registration');//, {path:'/registration'});
            });
        },

        initModule : function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Registration';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.registrationRoute;
            appInstance[viewName] = this.registrationView;
            appInstance[controllerName] = this.registrationController;
        }
    }


});