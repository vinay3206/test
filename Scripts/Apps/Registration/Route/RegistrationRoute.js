﻿;

//require.config({
//    paths: {
//        registrationmodel:'Apps/Registration/Model/RegistrationModel'
//    }
//});
define(['ember','khelkundutils','registrationmodel'],
    function (ember, khelkundutils,registrationmodel) {
        
        return Em.Route.extend({

            beforeModel: function (transition) {
                var user = this.controllerFor('application').get('userInfo');
                if (user) {
                    transition.abort();
                    this.transitionTo('home');
                }

            },



            model: function () {

                var url = $.url.parse(window.location.href);
                var rid = null;
                if (url['query']) {
                    rid = url.query.split('=')[1];
                }

                return registrationmodel.create({
                    rid:rid
                });
            },

            actions: {

                registerUser: function (signupData, callback) {
                  
                    var requestObj = {
                        url: 'Services/LoginService.svc/Register',
                        data: signupData.toServiceModel()
                    };
                    var registerStatus = khelkundutils.ajax.post(requestObj,
                        //success handler
                        $.proxy(function (apiResult) {
                            if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                //call success
                                khelkundutils.removeQueryString();
                                if (callback && (typeof (callback) === "function")) {
                                    callback();
                                }
                            }
                        }, this),
                        //failure handler
                        $.proxy(function (apiResult) {
                            //registration failed
                        }, this)
                    );
                    this.get('controller.ajaxCalls.registerUser').setPromise(registerStatus);
                },

                checkEmailAvailability: function (emailId, successCallback, errorCallback) {
                    var requestObj = {
                        url: 'Services/LoginService.svc/CheckEmailAvilability/' + emailId
                    };
                    var availabilityCallStatus = khelkundutils.ajax.get(requestObj,
                        //success callback
                        $.proxy(function (apiResult) {
                            if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                successCallback && successCallback(apiResult);
                            } else {
                                errorCallback && errorCallback(apiResult);
                            }
                        }, this),
                        //failure callback
                        $.proxy(function (error) {
                            //runs only on network error
                            //should not happen
                            console.log("Checking of email existence faied. Network error.");
                        }, this)
                    );
                    this.get('controller.ajaxCalls.checkEmailAvailability').setPromise(availabilityCallStatus);
                },

                checkTeamNameAvailability: function (tname, successCallback, errorCallback) {
                    var requestObj = {
                        url: 'Services/LoginService.svc/ValidateTeamName/'+tname
                    };
                    var availabilityCallStatus = khelkundutils.ajax.get(requestObj,
                        //success callback
                        $.proxy(function (apiResult) {
                            if (khelkundutils.ajax.validateApiResult(apiResult)) {
                                successCallback && successCallback(apiResult);
                            } else {
                                errorCallback && errorCallback(apiResult);
                            }
                        }, this),
                        //failure callback
                        $.proxy(function (error) {
                            //runs only on network error
                            //should not happen
                            console.log("Checking of team name existence faied. Network error.");
                        }, this)
                    );
                    this.get('controller.ajaxCalls.checkTeamNameAvailability').setPromise(availabilityCallStatus);

                },
            }
         
        });
    });