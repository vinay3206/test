﻿;

//require.config({
//    paths: {
       
//    }
//});

define(['ajaxcallstatushandler'], function () {
    

    return Em.ObjectController.extend(Khelkund.Mixins.AjaxCallsHandler, {

       
        ajaxCallsToRegister: ['registerUser', 'checkEmailAvailability', 'checkTeamNameAvailability'],

        init: function () {
            this._super();
            this.registerAjaxCalls();

           

        },
        stateList: [
               
               "Andaman and Nicobar Island",
               "Andhra Pradesh",
               "Arunachal Pradesh",

               "Bihar",
               "Chandigarh",
               "Chhattisgarh",
               "Dadra and Nagar Haveli",
               "Daman and Diu",
               "Delhi",
               "Goa",
               "Gujarat",
               "Haryana",
               "Himachal Pradesh",
               "Jammu and Kashmir",
               "Jharkhand",
               "Karnataka",
               "Kerala",
               "Lakshadweep",
               "Madhya Pradesh",
               "Maharashtra",
               "Manipur",
               "Meghalaya",
               "Mizoram",
               "Nagaland",

               "Puducherry",
               "Punjab",
               "Rajasthan",
               "Sikkim",
               "Tamil Nadu",
               "Tripura",
               "Uttarakhand",
               "Uttar Pradesh",
               "West Bengal"
        ],

        countryList: [

		"Afghanistan",
		"Albania",
		"Algeria",
		"Andorra",
		"Angola",
		"Anguilla",
		"Antigua And Barbuda",
		"Argentina",
		"Armenia",
		"Aruba",
		"Australia",
		"Austria",
		"Azerbaijan",
		"Bahamas",
		"Bahrain",
		"Bangladesh",
		"Barbados",
		"Belarus",
		"Belize",
		"Benin",
		"Bermuda",
		"Bolivia",
		"Bosnia-Herzegovina",
		"Botswana",
		"Brazil",
		"British Virgin Islands",
		"Brunei Darussalam",
		"Bulgaria",
		"Burkina Faso",
		"Burundi",
		"Cambodia",
		"Cameroon",
		"Canada",
		"Cape Verde Islands",
		"Cayman Islands",
		"Central African Republic",
		"Chad",
		"Chile",
		"China",
		"Colombia",
		"Congo",
		"Cook Islands",
		"Costa Rica",
		"Croatia",
		"Cuba",
		"Cyprus",
		"Czech Republic",
		"Democratic Rep. Of Congo",
		"Denmark",
		"Djibouti",
		"Dominica",
		"Dominican Republic",
		"Ecuador",
		"Egypt",
		"El Salvador",
		"Equatorial Guinea",
		"Eritrea",
		"Estonia",
		"Ethiopia",
		"Faroe Islands",
		"Fiji",
		"Finland",
		"French Polynesia",
		"Gabon",
		"Gambia",
		"Georgia",
		"ermany",
		"Ghana",
		"Gibraltar",
		"Greece",
		"Greenland",
		"Grenada",
		"Guatemala",
		"Guinea",
		"Guinea-Bissau",
		"Guyana",
		"Haiti",
		"Honduras",
		"ungary",
		"Iceland",
		
		"Indonesia",
		"Iraq",
		"Ireland",
		"Ivory Coast",
		"Jamaica",
		"Japan",
		"Jordan",
		"Kazakhstan",
		"Kenya",
		"Korea(North)",
		"Korea (South)",
		"Kuwait",
		"Kyrgyzstan",
		"Laos",
		"Latvia",
		"Lebanon",
		"Lesotho",
		"Liberia",
		"Libya",
		"Liechtenstein",
		"Lithuania",
		"Luxembourg",
		"Macao",
		"Macedonia",
		"Madagascar",
		"Malawi",
		"Malaysia",
		"Maldives",
		"Mali",
		"Malta",
		"Mauritania",
		"Mauritius",
		"Mexico",
		"Moldova",
		"Mongolia",
		"Montenegro",
		"Montserrat",
		"Morocco",
		"Mozambique",
		"Myanmar",
		"Namibia",
		"Nepal",
		"Netherlands Antilles",
		"New Caledonia",
		"New Zealand",
		"Nicaragua",
		"Niger",
		"Nigeria",
		"orway",
		"Oman",
		"Pakistan",
		"Palestine",
		"Panama",
		"Papua New Guinea",
		"Paraguay",
		"Peru",
		"oland",
		"Portugal",
		"Qatar",
		"Romania",
		"ussia",
		"Rwanda",
		"San Marino",
		"Sao Tome E Principe",
		"Saudi Arabia",
		"Senegal",
		"Serbia",
		"Seychelles",
		"Sierra Leone",
		"Singapore",
		"Slovakia",
		"Slovenia",
		"Solomon Islands",
		"Somalia",
		"South Africa",
		"Spain",
		"Sri Lanka",
		"St. Kitts And Nevis",
		"St. Lucia",
		"St. Vincent And The Grenadines",
		"Sudan",
		"Suriname",
		"Swaziland",
		"Sweden",
		"witzerland",
		"Taiwan",
		"Tajikistan",
		"Tanzania",
		"Thailand",
		"Togo",
		"Tonga",
		"Trinidad And Tobago",
		"Tunisia",
		"Turkmenistan",
		"Turks And Caicos Islands",
		"Uganda",
		"Ukraine",
		"United Arab Emirates",
		"United Kingdom",
		"Uruguay",
		"Uzbekistan",
		"Vanuatu",
		"Venezuela",
		"Vietnam",
		"Western Samoa",
		"Yemen",
		"Zambia",
		"Zimbabwe"


        ],

        actions: {
            registerUser: function () {
                if (this.get('model').validate()) {
                    this.get('target').send('registerUser', this.get('model'), $.proxy(function () {
                        
                        //Em.run.later(this, function () {
                        //    this.get('target').transitionTo('login');


                        //}, 2000);
                    }, this));
                } else {
                    
                    //resetting the service errors if client validation fails
                    if (this.get('ajaxCalls.registerUser.status')) {
                        this.set('ajaxCalls.registerUser.status', undefined);
                    }
                }
            },
            //action event
            checkEmailAvailability: function () {
                var emailId = this.get('emailId');
                if (emailId) {
                    this.get('target').send('checkEmailAvailability', emailId,
                        //call success
                        $.proxy(function (apiResult) {

                        }, this),
                        //call failure from service
                        $.proxy(function (apiResult) {
                            //service call failed 
                            //already handled in ajax calls statushandler
                            this.get('ajaxCalls.checkEmailAvailability.errors').pushObject(Em.Object.create({
                                code: '',
                                serviceMsg: 'Email ' + emailId + ' already present.'
                            }));
                        }, this)
                    );
                }
            },

            checkTeamNameAvailability: function () {
                var tname = this.get('teamName');
                if (tname) {
                    this.get('target').send('checkTeamNameAvailability', tname,
                        //call success
                        $.proxy(function (apiResult) {

                        }, this),
                        //call failure from service
                        $.proxy(function (apiResult) {
                            //service call failed 
                            //already handled in ajax calls statushandler
                            this.get('ajaxCalls.checkTeamNameAvailability.errors').pushObject(Em.Object.create({
                                code: '',
                                serviceMsg: 'Team name ' + tname + ' already present.'
                            }));
                        }, this)
                    );
                }
            },

        },










    });

});