﻿define(['ember', 'validationmodel', 'khelkundutils'], function (ember, validationmodel, khelkundutils) {
    var registrationModel = validationmodel.extend({
        //members
        rid:null,
        emailId: '',
        password: '',
        passwordConfirm: '',
        firstName: '',
        lastName: '',
        teamName: '',
        state: '',
        country:'',
        termsNConditions: false,



        
        toServiceModel: function (options) {
           
            var serviceModel = {
                "UserName": null,
                "Password": this.get('password'),
                "Email":this.get('emailId'),
                "FirstName": this.get('firstName'),
                "LastName": this.get('lastName'),
                "TeamName":this.get('teamName'),
                "Country": this.get('country')||"India",
                "State": this.get('state') ||'NA',
                
               
            };
            if (this.get('rid')) {
                serviceModel.ReferralId = this.get('rid');
            }
            
            return serviceModel;
        },

        //validations
        validations: {
            firstName: {
                presence: {
                    controlName: 'First Name',
                },
                format: {
                    value: "^([a-zA-Z]+[-.']{0,1}[a-zA-Z]*)+$",
                    controlName: 'First Name',
                }

            },
            lastName: {
                presence: {
                    controlName: 'Last Name',
                },
                format: {
                    value: "^([a-zA-Z]+[-.']{0,1}[a-zA-Z]*)+$",
                    controlName: 'Last Name',
                }
            },
            emailId: {
                presence: {
                    controlName: 'Email'
                },
                format: {
                    value: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                    controlName: 'Email',
                }
            },
            
             password: {
                presence: {
                    controlName:'Password',
                },
            },
            passwordConfirm: {
                customValidation: {
                    validator: function (obj, attr, value) {

                        if (obj.get('passwordConfirm')) {

                            if (obj.get('password') && obj.get('password') != obj.get('passwordConfirm')) {
                                obj.set('password', null);
                                obj.set('passwordConfirm', null);
                                obj.get('validationErrors').add(attr, null, this.options, "@{controlName} should be equal to @{value}");
                            }
                            // write format validation for email and put accrding error msg
                        }
                        else {
                            obj.get('validationErrors').add(attr, null, this.options, "@{controlName} can't be blank");
                        }

                    },

                    options: {
                        controlName: 'Confirm Password',
                        value: 'Password'

                    }
                }
            },
            
            teamName: {
                presence: {
                    controlName: 'Team Name',
                },

                format: {
                    value: "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$",
                    controlName: 'Team Name',
                }

            },
            //state: {
            //    presence: {
            //        controlName: 'State',
            //    },
            //},

            country: {
                presence: {
                    controlName: 'Country',
                },
            },
            
            termsNConditions: {
                customValidation: {
                    validator: function (obj, attr, value) {
                        if (obj && obj.get('termsNConditions')) {
                        }
                        else {
                            obj.get('validationErrors').add(attr, null, this.options, "@{controlName} needs to be selected"); //set falsy value
                        }
                    },
                    options: {
                        controlName: 'Terms & Conditions'
                    }
                },

            },

        }

    });

    return registrationModel;


});