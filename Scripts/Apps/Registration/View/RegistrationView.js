﻿
//require.config({
//    paths: {


//    }
//});


define([],


    function () {
        
        return Em.View.extend({



            didInsertElement: function () {
                this._super();
                //register events
                this.registerEvents();

            },
            willDestroyElement: function () {
                this._super();
                this.destroyEvents();
            },

            resetPromise: function () {
                if (this.get('controller.ajaxCalls.registerUser')) {
                    this.get('controller.ajaxCalls.registerUser').resetPromise();
                }

            }.on('didInsertElement'),

            registerEvents: function () {
                if (this.get('state') === 'inDOM') {
                    this.$().on('focusout.registration', '[name = userEmail]', $.proxy(function () {
                        this.checkUserNameAvailability();
                    }, this));
                    this.$().on('focusout.registration', '[name = teamName]', $.proxy(function () {
                        this.checkTeamNameAvailability();
                    }, this));
                }
            },
            checkUserNameAvailability: function () {
                this.get('controller').send('checkEmailAvailability');
            },
            checkTeamNameAvailability: function () {
                this.get('controller').send('checkTeamNameAvailability');
            },
            destroyEvents: function () {
                this.$().off('focusout.registration', '[name=userEmail]');
                this.$().off('focusout.registration', '[name=teamName]');
            },
            showLoaderOnEmail: function () {
                return this.get('controller.ajaxCalls.checkEmailAvailability.inProgress');
            }.property('controller.ajaxCalls.checkEmailAvailability.inProgress'),

            showLoaderOnTeamName:function(){
                return this.get('controller.ajaxCalls.checkTeamNameAvailability.inProgress');

            }.property('controller.ajaxCalls.checkTeamNameAvailability.inProgress')
            


        });






    });