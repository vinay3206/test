﻿;

//require.config({
//    paths: {
       
//    }
//});


define(['khelkundutils', 'ajaxcallstatushandler'], function (khelkundutils,ajaxcallstatushandler) {

   
    return {

        verificationRoute: Em.Route.extend({

            model: function () {
                var url = $.url.parse(window.location.href);
                var authToken = null;
                if (url['query']) {
                    authToken = url.query.split('=')[1];
                }
                if (authToken) {

                    var verificationStatus=khelkundutils.ajax.get({
                        url: "Services/LoginService.svc/Validate/" + authToken,

                    }, $.proxy(function (apiResult) {
                        if (khelkundutils.ajax.validateApiResult(apiResult)) {
                            this.get('controller').set('isVerified', true);
                            console.log(apiResult);
                            
                        }

                    }, this), $.proxy(function (err) {
                        console.log('Failure');
                    }, this));
                    this.controllerFor('verification').get('ajaxCalls.verification').setPromise(verificationStatus);
                }
                return {};
                
            }

        }),

        verificationView: Em.View.extend(),
        verificationController: Em.ObjectController.extend(ajaxcallstatushandler, {
            isVerified: false,
            ajaxCallsToRegister: ['verification'],

            init: function () {
                this._super();
                this.registerAjaxCalls();



            },

            onVerificationSuccess: function () {
                if (this.get('isVerified')) {
                    Em.run.later(this, function () {
                        
                        khelkundutils.removeQueryString();
                       
                        this.get('target').transitionTo('login');
                    }, 2000);
                }

            }.observes('isVerified'),

        }),

        mapRoutes: function (appInstance) {
            appInstance.Router.map(function () {
                this.route('verification');//, {path:'/verification'});
            });
        },

        initModule: function (appInstance, moduleName) {
            this.mapRoutes(appInstance);
            moduleName = moduleName || 'Verification';

            var routerName = moduleName + 'Route',
                viewName = moduleName + 'View',
                controllerName = moduleName + 'Controller';

            appInstance[routerName] = this.verificationRoute;
            appInstance[viewName] = this.verificationView;
            appInstance[controllerName] = this.verificationController;
        }
    }


});