﻿;
require.config({
    baseUrl: 'Scripts',
    paths: {
        ember:'Resources/Ember/emberLoader',
        embercore: 'Resources/Ember/ember-1.3.1',
        jquery: 'Resources/Jquery/jquery-1.10.2',
        /*jquery_migrate: 'Resources/jquery-migrate-1.2.1',*/
        handlebars: 'Resources/Handlerbars/handlebars-1.1.2',
        bootstrap: 'Resources/Bootstrap/bootstrap',
        bootstrap3: 'Resources/Bootstrap/bootstrap3.min',
        docs: 'Resources/Bootstrap/docs.min',
        urldecoder: 'Resources/UrlDecoder/jquery.urldecoder',
        validationmodel: 'Resources/EmberValidationModel/ember-validations',
        d3: 'Resources/D3/d3',
        moment:'Resources/Moment/moment.min',

        avatar: 'Resources/Avatar/avatars.io',
        responsivetab: 'Resources/ResponsiveLibs/easyResponsiveTabs',
        customscrollbar: 'Resources/ResponsiveLibs/jquery.mCustomScrollbar',
        mousewheel: 'Resources/ResponsiveLibs/jquery.mousewheel',
        emberextensions:'Resources/EmberExtensions/ember-extensions',
       

        

        //utils
        khelkundutils: 'Resources/Utils/khelkund-utils',
        ajaxcallstatushandler: 'Resources/Utils/AjaxCallsStatusHandlerMixin',
        eventregister: 'Resources/Utils/EventRegisterMixin',
        paginationcontrollermixin: 'Resources/Utils/PaginationControllerMixin',
        paginationviewmixin: 'Resources/Utils/PaginationViewMixin',
        logger: 'Resources/Utils/Logger',
        fbauth: 'Resources/Utils/Fb-auth',
        purechat:'Resources/Utils/PureChat',
        optionsmixin: 'Resources/Mixin/OptionsMixin',
        scrollmixin: 'Resources/Utils/ScrollMixin',
        authenticationmixin: "Resources/Utils/AuthenticationMixin",
        holder: 'Resources/Utils/holder',
       
        


        // bootstrap extensions
        bs_dropdownview: 'Resources/BSExtensions/DropDown/DropdownView',
        bs_buttondropdownview: 'Resources/BSExtensions/DropDown/DropdownButtonView',
        bs_splitbuttondropdownview: 'Resources/BSExtensions/DropDown/DropdownSplitButtonView',
        bs_modalpopupheaderview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpHeaderView',
        bs_modalpopupfooterview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpFooterView',
        bs_modalpopupbodyview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUpBodyView',
        bs_modalpopupview: 'Resources/BSExtensions/ModalPopUp/Views/ModalPopUPView',
        bs_messageview: 'Resources/BSExtensions/MessageView/MessageView',
        bs_floatingmessageview: 'Resources/BSExtensions/MessageView/FloatingMessageView',



        // app file

        khelkundapp: 'KhelkundApp',



        ////extensions
        //logger: 'customResources/Logger',

        ////settings
        //gamesettings: 'customResources/gameSettings',
        //dependencyresolver: 'customResources/DependencyResolver',
        //dependencies: 'customResources/dependencies',
        //rulesmixin: 'customResources/RulesMixin',
       
        //common modules
        userchallengemodel: 'CommonModules/UserChallenge',
        challenge: 'CommonModules/Challenge',
        tournamentmodel: 'CommonModules/Tournament',
        match: 'CommonModules/MatchModel',
        team: 'CommonModules/Team',
        player: 'CommonModules/Player',
        teamplayer: 'CommonModules/TeamPlayer',
        detailedpoints: 'CommonModules/DetailedPoints',
        userteam: 'CommonModules/UserTeam',
        personalinfo: 'CommonModules/UserPersonalInfo',
        accountinfo:'CommonModules/UserAccountInfo',
        user: 'CommonModules/User',
        userstats:'CommonModules/UserStats',
        userteamdetails: 'CommonModules/UserTeamDetails',
        pointsbreakup: 'CommonModules/PointsBreakup',
        scorecard: 'CommonModules/Scorecard',
        headercomponent: 'HeaderComponent',
        loginpopupview: 'CommonModules/LoginPopUpView',
        timercomponent: 'CommonModules/TimerComponent',
        progressbarcomponent: 'CommonModules/ProgressBarComponent',
        


        //guidelinestepsview: 'views/GuidelineStepsView',
        //stepsconfig: 'customResources/StepsConfig'
        

        ////////
        /////local files //
        ////////
        
        //apps
        //userchallengesapp
        userchallengesconfig: 'Apps/UserChallenges/userChallengesConfig',
        //homeapp
        homeconfig: 'Apps/HomeApp/homeConfig',
        //editteamapp
        editconfig: 'Apps/EditTeam/editConfig',

        //components
        //tournaments
        tournamentscontroller: 'Components/Tournaments/TournamentsController',
        tournamentsview: 'Components/Tournaments/TournamentsView',
        matchitemview: 'Components/Tournaments/MatchItemView',

        //upcommingmatches
        upcommingmatchescontroller: 'Components/UpcommingMatches/UpcommingMatchesController',
        upcommingmatchesview: 'Components/UpcommingMatches/UpcommingMatchesView',
        upcommingmatchview: 'Components/UpcommingMatches/UpcommingMatchView',
        upcommingmatchcontroller: 'Components/UpcommingMatches/UpcommingMatchController',

        //activechallenges
        activechallengescontroller: 'Components/ActiveChallenges/ActiveChallengesController',
        activechallengesview: 'Components/ActiveChallenges/ActiveChallengesView',
        activechallengeview: 'Components/ActiveChallenges/ActiveChallengeView',
        activechallengecontroller: 'Components/ActiveChallenges/ActiveChallengeController',

        //playerlist
        playerslistcontroller: 'Components/PlayersList/PlayersListController',
        playerslistview: 'Components/PlayersList/PlayersListView',
        playercontroller: 'Components/PlayersList/PlayerController',
        playerview: 'Components/PlayersList/PlayerView',

        //userteam
        userteamcontroller: 'Components/UserTeam/UserTeamController',
        userteamview: 'Components/UserTeam/UserTeamView',
        teamplayerview: 'Components/UserTeam/TeamPlayerView',

        //userchallengestatus
        userchallengestatuscontroller: 'Components/UserChallengeStatus/UserChallengeStatusController',
        userchallengestatusview: 'Components/UserChallengeStatus/UserChallengeStatusView',

        //userchallengeteam
        userchallengeteamcontroller: 'Components/UserChallengeTeam/UserChallengeTeamController',
        userchallengeteamview: 'Components/UserChallengeTeam/UserChallengeTeamView',
        
        //scorecard
        scorecardcontroller: 'Components/Scorecard/ScorecardController',
        scorecardview: 'Components/Scorecard/ScorecardView',



        // Login
        //login: 'Apps/Login/Login',
        landinglogin: 'Apps/Login/LandingLogin',
        loginroute: 'Apps/Login/Route/LoginRoute',
        logincontroller: 'Apps/Login/Controller/LoginController',
        loginview: 'Apps/Login/View/LoginView',
        loginmodel: 'Apps/Login/Model/LoginModel',


        //Match
        //match: 'Apps/Match/Match',
        matchroute: 'Apps/Match/Route/MatchRoute',
        matchcontroller: 'Apps/Match/Controller/MatchController',
        matchview: 'Apps/Match/View/MatchView',
        customchallenge: 'Apps/Match/Model/CustomChallengeModel',
        invitefriendsmodel: 'Apps/Match/Model/InvitationModel',
        joinChallengeModel: "Apps/Match/Model/JoinChallengeModel",
        seriesmatchselectorview: 'Apps/Match/View/SeriesMatchSelectorView',
        matchdetailview: 'Apps/Match/View/MatchDetailView',
        challengesview: 'Apps/Match/View/ChallengesView',
        displayuserteamview: 'Apps/Match/View/DisplayUserTeamView',
        userteamview: 'Components/UserTeam/UserTeamView',
        customchallengeview: "Apps/Match/View/CustomChallengeView",
        createcustomchallengeview: "Apps/Match/View/CreateCustomChallengeView",
        openchallenge: 'Apps/Match/View/OpenChallengeView',
        joinchallengepopupview: 'Apps/Match/View/JoinChallengePopView',

        //payment
        //payment: "Apps/Payment/Payment",

        //Referral
        //referral: "Apps/Referral/Referral",
        referralroute: 'Apps/Referral/Route/ReferralRoute',
        referralcontroller: 'Apps/Referral/Controller/ReferralController',
        referralview: 'Apps/Referral/View/ReferralView',
        referralmodel: 'Apps/Referral/Model/ReferralModel',
        referralinviteview: 'Apps/Referral/View/ReferralInviteView',

        //Registration
        //registration: "Apps/Registration/Registration",
        registrationroute: 'Apps/Registration/Route/RegistrationRoute',
        registrationcontroller: 'Apps/Registration/Controller/RegistrationController',
        registrationview: 'Apps/Registration/View/RegistrationView',
        registrationmodel: 'Apps/Registration/Model/RegistrationModel',

        //ResetPassword

        //resetpassword: "Apps/Resetpassword/Resetpassword",
        resetpasswordroute: 'Apps/Resetpassword/Route/ResetpasswordRoute',
        resetpasswordcontroller: 'Apps/Resetpassword/Controller/ResetpasswordController',
        resetpasswordmodel:'Apps/Resetpassword/Model/ResetpasswordModel',

        //User-profile

        //userprofile: "Apps/UserProfile/UserProfile",
        userprofileroute: 'Apps/UserProfile/Route/UserProfileRoute',
        userprofilecontroller: 'Apps/UserProfile/Controller/UserProfileController',
        userprofileview: 'Apps/UserProfile/View/UserProfileView',
        withdrawamountmodel: 'Apps/UserProfile/Model/WithDrawModel',
        addamountmodel: 'Apps/UserProfile/Model/AddAmountModel',
        accountdetailsview: 'Apps/UserProfile/View/AccountDetailsView',
        addwithdrawview: 'Apps/UserProfile/View/AddWithdrawView',
        personalinfoview: 'Apps/UserProfile/View/PersonalInfoView',
        statisticsview: 'Apps/UserProfile/View/StatisticsView',
        accountdetailseditview: 'Apps/UserProfile/View/AccountsEditView',
        accountsview: 'Apps/UserProfile/View/AccountsView',
        personaleditview: 'Apps/UserProfile/View/PersonalInfoEditView',
        personalview: 'Apps/UserProfile/View/PersonalView',
        addUserTeamView: 'Apps/UserProfile/View/AddUserTeamView',




        //Verification
        //verification:"Apps/Verification/Verification"

        // admin modules
        modifymatchroute: 'Apps/AdminPanel/Route/ModifyMatchRoute',
modifymatchview: 'Apps/AdminPanel/View/ModifyMatchView',
modifymatchcontroller: 'Apps/AdminPanel/Controller/ModifyMatchController',
footballplayerscoremodel: 'Apps/AdminPanel/Model/UpdatePlayerScoreModel',





    


        

    },
   
    waitSeconds: 0,
    shim: {
        /*'jquery_migrate': ['jquery'],*/
        'embercore': ['jquery', 'handlebars'],
        'bootstrap': ['jquery'],
        'urldecoder': ['jquery'],
        'logger': ['jquery'],
        'responsivetab': ['jquery'],
        'mousewheel': ['jquery'],
        'customscrollbar': ['jquery', 'mousewheel'],
        'docs': ['jquery', 'bootstrap3'],
        
        'emberextensions': ['embercore'],
        'validationmodel': {
            deps: ['ember'],
            exports: 'validationmodel',
            init: function () {
                return Em.Object.extend(Ember.Validations);

            }

        }
        

    },
    callback: function () {
        window.logerror = function () {
            var i;
            var messages = [];
            for (i = 0; i < arguments.length; i++) {
                messages.push(arguments[i]);
            }
            console.error.apply(console, messages);

        };
        require(['jquery', 'bootstrap', 'khelkundutils', 'urldecoder', 'logger', 'validationmodel', 'responsivetab', 'customscrollbar', 'fbauth','moment'], function () {
            require(['khelkundapp']);
        });

    }
});

/*require(['teamapp'],
    function (teamapp) {
        //Application name should start with capital letter.
        var TeamApp = teamapp.create({
            
        });
        //to enable the custom loggings.
        window.loggingEnabled = true;
    });*/