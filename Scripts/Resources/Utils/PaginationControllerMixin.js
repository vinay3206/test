;
define(['ember'],
	function(ember){
		return Em.Mixin.create({
		    currentPage: 1,
            fullList: null,
			moveNextPage: function () {
			    if (this.get('currentPage') < this.get('pagesCount')) {
			        this.incrementProperty('currentPage');
			    }
			},
			pageSize: 10,
			pagesCount: function(){
				if(this.get('fullList') && (this.get('fullList').length > 0) && (this.get('pageSize') > 0)){
					return this.get('fullList').length / this.get('pageSize');
				}
				return 0;
			}.property('fullList.length', 'pageSize'),
			movePreviousPage: function () {
			    if (this.get('currentPage') > 1) {
			        this.decrementProperty('currentPage');
			    }
			},
			setPage: function(pageNumber){
			    this.set('currentPage', pageNumber);
			},

			//override this method if your list is not present in the fullList object
			pageContent: function(){

				if(this.get('fullList') && this.get('fullList').length > 0){
					return this.filterPageContent(this.get('fullList'));
				}
				return [];
			}.property('fullList.@each', 'currentPage'),

			filterPageContent: function (originalArray) {
			    logger.log('Getting players for page: ', this.get('currentPage'));
			    var startPos = (this.get('currentPage') - 1) * this.get('pageSize');
			    return originalArray.slice((this.get('currentPage') -1) * this.get('pageSize'), startPos + this.get('pageSize'));
			}


		});

	});