﻿/**
    This mixin is used to register the events on the implemented object. It can be used by any view, controller or any Object to support 
    the event registration functionality.

    Options:
    eventsToRegister    array[{                     this contains an array of strings containing the list of event names to be mapped with the functions to execute.
                                                    the naming conventions used are e.g. if event name is valueChange then the respective function invoked on this event trigger
                                                        will be onValueChange
                                eventName: '',      name of the event to register
                                element: '',        element on which the event is triggered
                                dynamicElement: '', in case if the element is not present at current time then dynamic element can also be given
                            }]                                                        
    eventNamespaces     string array        this contains ans array of strings having the namespaces to be applied while registering for the event(s)
    eventElement        string/selector     this contains the selector or string selector to select the element on which the event is triggered
    

    Functions:
    bindEvents:         none        this function binds the events given to the functions created on using the convention and namespaces.
    removeAllEvents     none            this function removes all the events binded by the mixin
    removeEvent         eventName   this function remove the eventName named event binded by the mixin
*/
;
//todo: integrate update eventregistermixin from "KHELKUND" later(big change in implementation)
define(['ember'],
    function (ember) {
        window.Mystique = window.Mystique || {};
        window.Mystique.Mixins = Mystique.Mixins || {};

        window.Mystique.Mixins.EventRegistration = Em.Mixin.create({
            //array of events to register
            eventsToRegister: null,
            //array of namespaces to register on events for easy removal of events
            eventNamespaces: null,
            
            //function
            //todo: bindEvents can also take list of arguments to only activate/bind those events only
            bindEvents: function () {
                

                if (this.get('eventsToRegister') && this.get('eventsToRegister').length > 0) {
                    for (var i = 0 ; i < this.get('eventsToRegister').length; i++) {
                        //resolving parameters
                        var eventName = this.get('eventsToRegister')[i].eventName.split('.')[0];
                        var specificNamespaces = (this.get('eventsToRegister')[i].eventName.indexOf('.') != -1) ? this.get('eventsToRegister')[i].eventName.substring(this.get('eventsToRegister')[i].eventName.indexOf('.')): '';
                        var element = this.get('eventsToRegister')[i].element || 'body';
                        var dynamicElement = this.get('eventsToRegister')[i].dynamicElement;
                        var functionName = this._generateEventFunctionName(eventName);
                        var namespaces = this.get('eventNamespaces') ? ('.' + this.get('eventNamespaces').join('.')) : '';
                        namespaces = namespaces + specificNamespaces;
                        if (this.get(functionName)) {
                            if (dynamicElement) {
                                $(element).on(eventName + namespaces, dynamicElement, $.proxy(this.get(functionName), this));
                            } else {
                                $(element).on(eventName + namespaces, $.proxy(this.get(functionName), this));
                            }
                        }
                    }
                }
            },
            removeAllEvents: function () {
                if (Em.isArray(this.get('eventsToRegister'))) {
                    this.get('eventsToRegister').forEach(function (eventItem, index) {
                        var element = Em.get(eventItem, 'element') || 'body';
                        var dynamicElement = Em.get(eventItem, 'dynamicElement');
                        var namespaces = this.get('eventNamespaces') ? ('.' + this.get('eventNamespaces').join('.')) : '';
                        if (dynamicElement) {
                            $(element).off(Em.get(eventItem, 'eventName') + namespaces, dynamicElement);
                        } else {
                            $(element).off(Em.get(eventItem, 'eventName') + namespaces);
                        }
                    }, this);
                }
            },
            removeEvent: function (eventName) {
                if (Em.isArray(this.get('eventsToRegister'))) {
                    var eventItem = this.get('eventsToRegister').findBy('eventName', eventName);
                    if (eventItem) {
                        var element = Em.get(eventItem, 'element') || 'body';
                        var dynamicElement = Em.get(eventItem, 'dynamicElement');
                        if (dynamicElement) {
                            $(element).off(Em.get(eventItem, 'eventName') + namespaces, dynamicElement);
                        } else {
                            $(element).off(Em.get(eventItem, 'eventName') + namespaces);
                        }
                    }
                }
            },
            //function to generate the name of the function which should be created using the conventions
            _generateEventFunctionName: function (name) {
                return 'on' + name[0].toUpperCase() + name.slice(1);
            },
            //event confirmation is also integrated in it.
            confirmedTrigger: function (eventName, element, callback, data) {
                var isEventConfirmed = false;
                var element = element || 'body';
                var triggerCount = 0;
                var triggerArguments = [];
                triggerArguments.push('place reserved for callback function');

                if (callback) {
                    triggerArguments.push(callback);
                }


                if (data && !$.isArray(data)) {
                    triggerArguments.push(data);
                } else if(data) {
                    for (var i = 0; i < data.length; i++) {
                        triggerArguments.push(data[i]);
                    }
                }

                var triggerEvent = function (eventName) {
                
                    if (!isEventConfirmed) {
                        if (triggerCount > 300) {
                            logger.logError(eventName + ' trigger timed out');
                            return;
                        }
                        triggerCount++;
                        logger.log(eventName + ' triggered ' + triggerCount + ' times.');

                        triggerArguments[0] = function () {
                            isEventConfirmed = true;
                            //callback && callback(arguments);
                        };
                        $(element).trigger(eventName, triggerArguments);

                        setTimeout(function () {
                            triggerEvent(eventName);
                        }, 20);
                    }
                };
                triggerEvent(eventName);

            }

        });

        return window.Mystique.Mixins.EventRegistration;

    });