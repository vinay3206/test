﻿;
define(['ember'],
    function (ember) {
        return Em.Mixin.create({
            currentPageBinding: 'controller.currentPage',
            pagesList: function () {
                var pagesArray = [];
                for (var i = 0; i < this.get('controller.pagesCount') ; i++) {
                    var pageObject = Em.Object.extend({
                        pageNumber: null                        
                    });
                    pagesArray.pushObject(Em.Object.create({
                        pageNumber: i + 1
                    }));
                }
                return pagesArray;

            }.property('controller.pagesCount'),
            paginationItemView: Em.View.extend({

                //to be binded from tempalte
                currentPage: null,
                //current page as perindex
                //to be set by template
                pageIndex: null,
                page: function(){
                    return Em.Object.create({
                        id: this.get('pageIndex'),
                        isActive: this.get('pageIndex') === this.get('currentPage')
                    });
                }.property('currentPage')
            })

        });
    });