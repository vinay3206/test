﻿define(['jquery'], function ($) {

           
            //request : { url:'', params:{}, isExternalResouce:false }
            var redirectModule = function (request) {
                if (!request || !request.url)
                    return;

                //Need to handle advance cases like other http verbs
                if (request.openInNewTab) {
                    window.open(ajaxModule.formatUrl(request));
                }
                else {

                    window.location = ajaxModule.formatUrl(request);
                }
            };

            var ajaxModule = {
                //request : { url:'', dataType:'', contentType:'', data:''}
                //request also contains some extra querry string parameters to be added                              
                get: function (request, onSuccess, onError) {
                    var ac = $.ajax({
                        url: this.formatUrl(request),
                        context: request.context,
                        async:request.async,
                        type: 'GET',
                        dataType: request.dataType || 'json',
                        success: (onSuccess || function () { }),
                        error: (onError || function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                        })
                    });
                    //lstAjaxCall.push(ac);
                    return ac;
                },
                post: function (request, onSuccess, onError) {
                    var ac = $.ajax({
                        url: this.formatUrl(request),
                        context: request.context,
                        type: 'POST',
                        async:request.async,
                        contentType: request.contentType || "application/json",
                        dataType: request.dataType || 'json',
                        data: serializeModule.serialize(request.data),
                        success: (onSuccess || function () { }),
                        error: (onError || function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                        })
                    });
                    //lstAjaxCall.push(ac);
                    return ac;
                },
                put: function (request, onSuccess, onError) {
                    var ac = $.ajax({
                        url: this.formatUrl(request),
                        context: request.context,
                        type: 'PUT',
                        contentType: request.contentType || "application/json",
                        dataType: request.dataType || 'json',
                        data: serializeModule.serialize(request.data),
                        success: (onSuccess || function () { }),
                        error: (onError || function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                        })
                    });
                    //lstAjaxCall.push(ac);
                    return ac;
                },
                del: function (request, onSuccess, onError) {
                    var ac = $.ajax({
                        url: this.formatUrl(request),
                        context: request.context,
                        type: 'DELETE',
                        async:request.async,
                        success: (onSuccess || function () { }),
                        error: (onError || function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                        })
                    });
                    //lstAjaxCall.push(ac);
                    return ac;
                },
                stop: function () {
                    if (lstAjaxCall.length > 0) {
                        try {
                            for (var cnt = 0; cnt < this.lstAjaxCall.length; cnt++) {
                                lstAjaxCall[cnt].abort();
                            }
                        } catch (e) { }
                    }
                },
                formatUrl: function (request) {
                    //don't apply application specific info to resource if it is an external one, ex : http://google.com?q=las
                    if (request && request.url && !request.isExternalResouce) {

                        var tempRequest = request.url.split('#/');
                        var requestUrl = tempRequest[0];
                        var requestHash = tempRequest[1] || null;

                        var newLoc = { path: requestUrl, params: request.params };

                        newLoc.params = newLoc.params || null;
                       
                        return $.url.build(newLoc) + (requestHash ? ('#/' + requestHash) : '');
                    }
                    return request.url;
                    
                },
                validateApiResult: function (apiResult) {
                    if (apiResult && apiResult.Status === 0) {
                        return true;
                    }
                    return false;
                }
            };

            var serializeModule = {
                
                serialize: function (obj) {
                    if (!obj == true)
                        return null;
                    return JSON.stringify(obj);
                }
            };

            var removeQueryString = function () {
                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);

                }
            };

            

          
            
         
            return {
                ajax: ajaxModule,
                serializer: serializeModule,
                redirect: redirectModule,
                removeQueryString:removeQueryString
                };

        });