﻿;
define(['ember', 'khelkundutils'],
    function (ember, khelkundutils) {

        Khelkund = window.Khelkund || {};
        Khelkund.Mixins = Khelkund.Mixins || {};
        /**
        * this module handles the errors messages and resolves on the priority of usermessages -> servicemessages -> null
        */
        var errorHandler = Em.Object.extend({
            errorsList: null,
            code: null,
            serviceMsg: null,
            message: function () {
                var i = 0;
                var resolvedMessage = null;
                if (this.get('errorsList')) {
                    $.each(this.get('errorsList'), $.proxy(function (idx, val) {
                        if ((this.get('code') === val.code) && val.message) {
                            resolvedMessage = val.message;
                        }
                    }, this));
                }
                resolvedMessage = resolvedMessage ? resolvedMessage : (this.get('serviceMsg') ? this.get('serviceMsg') : null);
                return resolvedMessage;
            }.property('code')
        });
        /**
        * this module handles the status of the calls using the promises structure and also creates a errorhandler which handles the error messages.
        */
        var call = Em.Object.extend({
            //It contains the the promise of ajax calls. Can be used to integrate some other functionality also other than already present.
            callPromise: null,
            /**
            * Array of object having structure as {code, message } containing messages and codes of some common custom errors.
            * This value is give by the ajaxcallshandler.
            */
            errorsList: null,

            availableStatus: {
                inProgress: 'inProgress',
                isSuccess: 'isSuccess',
                isFailure: 'isFailure'
            },
            setPromise: function (prom) {
                this.resetPromise();
                this.set('callPromise', prom);
                this.set('status', this.availableStatus.inProgress);

                this.set('errors', []);

                this.get('callPromise').then(
                    //onfulfilled
                    $.proxy(function (data) {
                        if (khelkundutils.ajax.validateApiResult(data)) {
                            this.set('status', this.availableStatus.isSuccess);
                        } else {
                            this.set('status', this.availableStatus.isFailure);

                            this.get('errors').pushObject(errorHandler.create({
                                code: Em.get(data, 'Error.Errorcode'),
                                serviceMsg: Em.get(data, 'Error.ErrorMessage'),
                                errorsList: this.get('errorsList')
                            }));
                        }
                    }, this),
                    //onrejected
                    $.proxy(function (data) {
                        this.set('status', this.availableStatus.isFailure);
                        //pushing the error
                        //Assuming error is not because network failure and always service failure containing proper formatted error in "Status" object
                        this.get('errors').pushObject(errorHandler.create({
                            code: Em.get(data, 'status.code'),
                            serviceMsg: Em.get(data, 'status.message'),
                            errorsList: this.get('errorsList')
                        }));
                    }, this)
                );


            },
            resetPromise: function () {
                this.setProperties({
                    callPromise: null,
                    status: null,
                    errors: []
                });
            },
            //private property
            status: null,
            inProgress: function () {
                return this.get('status') === this.availableStatus.inProgress;
            }.property('status'),
            isSuccess: function () {
                return this.get('status') === this.availableStatus.isSuccess;
            }.property('status'),
            isFailure: function () {
                return this.get('status') === this.availableStatus.isFailure;
            }.property('status'),

            errors: null


        });

        /**
        * This module is the mixin which is to be inherited. It creates a ajaxCalls property which has different calls as its property.
        Usage:
        ```javascript
        Em.Object.create(Mystique.Mixins.AjaxCallsHandler, {
            ajaxCallsToRegister: ['getBook', {  name: 'addBook',
                                                errors: [{                              //specific errors
                                                    code: 302
                                                    message: 'book name not valid'
                                                }] ],
            commonErrors: [{
	            code: 345,
                message: not authorized
            }],
            init: function(){
	            this._super();
                this.registerAjaxCalls();
            },
            fetchBook: function(){
                var getBookPromise = khelkundutils.ajax.get({url: '/get/Books'}, this.successHandler, this.failureHandler);
	            Em.get(ajaxCalls, 'getBook').setPromise(getBookPromise);
            }, 
            fetchBookStatus: function(){
	            khelkundutils.logger.log('status of getBook is ' + this.get('ajaxCalls.getBook.status);
            }.observes('ajaxCalls.getBook.status')
        });
        ```
        */

        Khelkund.Mixins.AjaxCallsHandler = Em.Mixin.create({
            ajaxCalls: null,

            //array of ajax calls to observes and maintain their status and errors.
            ajaxCallsToRegister: null,
            //array of object having structure as {code, message } containing messages and codes of some common custom errors.
            commonErrors: null,
            /**
             This method is used to register to the calls.
             @param {callsList} list of string/objects to register to the calls.
             This method can be used to explicitly register for the calls.
            */
            registerAjaxCalls: function (callsList) {

                if (callsList) {
                    var i = 0;
                    this.set('ajaxCalls', Em.Object.create());

                    for (i = 0; i < callsList.length; i++) {
                        var callName = callsList[i];
                        var errors = [];
                        if (this.get('commonErrors')) {
                            errors.setObjects(this.get('commonErrors'));
                        }

                        if (typeof (callsList[i]) == 'object') {
                            callName = callsList[i].name;
                            errors.pushObjects(callsList[i].errors);
                        }
                        this.get('ajaxCalls').set(callName, call.create({
                            errorsList: errors
                        }));
                    }

                }
            },
            //overriding the init function to initialize the calls automatically
            init: function () {
                this._super();
                this.registerAjaxCalls(this.get('ajaxCallsToRegister'));
            }


        });

        return Khelkund.Mixins.AjaxCallsHandler;



    });
