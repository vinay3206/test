;
/**
 * @module scrollmixin
 */
define(['ember', 'customscrollbar', 'mousewheel'], function (ember) {
	/**
	 * This mixin is for Ember views only. Uses jquery.mCustomScrollbar plugin.
	 */

	return Em.Mixin.create({
		/**
		 * Key to control the auto applying of scroll plugin. If turned false we need to manually call
		 * the applyScroll function. Default value is true.
		 * @type {Boolean}
		 */
		autoApply: true,
		/**
		 * Element on which scroll plugin is applied. If this value is not provided uses this.$() as its scrollable element.
		 * @type {String/jquerySelector}
		 */
		scrollElement: null	,
		/**
		 * Handler to control the scroll functions.
		 * @type {Object}
		 */
		scrollHandler: null,
		/**
		 * Applies scroll plugin to the provided scrollElement
		 * @param  {jQuery Selector} scrollElement Jquery selector or string selector to select the element
		 *                           inside the view.
		 * @return {Object}               Handler to control the scroll.
		 */
		applyScroll: function (scrollElement) {
			//validate scrollElement
			scrollElement = scrollElement?scrollElement : this.get('scrollElement');
			if(!scrollElement){
				scrollElement = this.$();
			}

			var scrollHandler = $(scrollElement, this.$()).mCustomScrollbar({
				scrollButtons: {
					enable: true
				}
			});
			this.set('scrollHandler', scrollHandler);
			return scrollHandler;
		},
		/**
		 * Updates the scroll to recalcualte its parameters
		 */
		updateScroll: function (scrollElement) {
			if(!this.get('scrollHandler')) {
				//i.e. scroll not yet applied
			    this.applyScroll(scrollElement);
			    return;
			}
            //if the provided element has multiple refrences call update on each
			var scrollHandler = $(this.get('scrollHandler'));
			if (Em.isArray(scrollHandler)) {
			    $.each(scrollHandler, function (index, element) {
			        $(element).mCustomScrollbar('update');
			    });
			} else {
			    scrollHandler.mCustomScrollbar('update');
			}
		},
		/**
		 * Applies scroll plugin if auto apply is true.
		 * @return {[type]} [description]
		 */
		viewInserted: function () {
			if(this.get('autoApply') ===  true){
				this.applyScroll();
			}
		}.on('didInsertElement')

	});
});