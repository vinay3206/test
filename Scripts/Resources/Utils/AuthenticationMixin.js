﻿;
define(['ember','user'],
    function (ember,user) {
        return Ember.Mixin.create({
           

            beforeModel: function (transition) {
                this._super();
                this.controllerFor('application').set('transition', transition);
                var authenticationPromise = new Em.RSVP.Promise($.proxy(function (resolve, reject) {
                    var userInfo = this.controllerFor('application').get('userInfo');
                    if (userInfo) {
                        resolve({});
                    }
                    else {
                        var promise = user.fetchLoggedInUser();
                        promise.then(function (apiResult) {

                            $('body').trigger('userLoggedIn', [apiResult.data]);
                            resolve({});

                        }, function () { reject({});});
                    }


                }, this));
               
                return authenticationPromise;

            },

            actions: {
                error: function (err) {
                    console.log(err);
                    this.transitionTo('login');
                }
            }

        });
    });