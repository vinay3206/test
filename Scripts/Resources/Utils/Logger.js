﻿;

define(['ember'],
    function (ember) {
        var logger = Em.Object.extend({
            loggingEnabled: function(key, value){
                //use for get only

                //get
                if(arguments.length == 1){
                    if(!window.disableLogging){
                        return true;
                    }
                    return false;
                } else {
                    //setter but not allowed
                    return false;
                }
            }.property(),

            errorLoggingEnabled: function () {
                if (this.get('loggingEnabled') && !window.disableErrorLogging) {
                    return true;
                }
                return false;
            }.property(),
            logError: function () {
                if (this.get('errorLoggingEnabled')) {
                    var message = ['%cCustomError: '];
                    message.push('color:red;');
                    for (var i = 0; i < arguments.length; i++) {
                        message.push(arguments[i]);
                    }                    
                    console.log.apply(console, message);
                }
            },
            log: function () {
                if (this.get('loggingEnabled')) {
                    var message = ['CustomMsg:'];
                    for(var i =0; i< arguments.length; i++){
                        message.push(arguments[i]);
                    }
                    console.log.apply(console, message);
                }
            }
        });
        var loggerObj = logger.create();
        window.logger = loggerObj;
        return loggerObj;
    }, function (err) {
        //err.requireType
        //err.requireModules
        logerror("customerror: ");
        logerror(err.requireModules);
    });