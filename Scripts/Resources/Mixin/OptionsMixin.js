﻿;
define(['ember'], function (ember) {

    Mystique = window.Mystique || {};
    Mystique.Mixins = Mystique.Mixins || {};

    Mystique.Mixins.Options = Em.Mixin.create({
        gatherOptions: function () {
            var uiOptions = this.get('uiOptions'), options = {};

            uiOptions.forEach(function (key) {
                options[key] = this.get(key);
            }, this);

            var uiEvents = this.get('uiEvents') || [], self = this;

            uiEvents.forEach(function (event) {
                var callback = self[event];

                if (callback && $.isFunction(callback)) {
                    options[event] = $.proxy(callback, self);
                }
            });
            return options;
        }
    });
    return Mystique.Mixins.Options;
});