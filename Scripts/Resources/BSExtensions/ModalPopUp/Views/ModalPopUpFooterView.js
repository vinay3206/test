﻿; define(['ember'], function (ember) {
    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};
    var footerTemplate = [
            '{{#if view.secondary}}<a href="#" class="btn btn-secondary" rel="secondary">{{view.secondary}}</a>{{/if}}',
            '{{#if view.primary}}<a href="#" class="btn btn-primary" rel="primary">{{view.primary}}</a>{{/if}}',
    ].join("\n");

    Khelkund.Views.ModalPopUpFooterView = Em.View.extend({
        primary:'Ok',
        secondary: 'Cancel',
        classNames: ['modal-footer'],
        template: Em.Handlebars.compile(footerTemplate),
    });
    return Khelkund.Views.ModalPopUpFooterView;
});
