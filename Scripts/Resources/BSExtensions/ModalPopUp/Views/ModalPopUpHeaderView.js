﻿; define(['ember'], function (ember) {
    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};
    var headerTemplate = [
        '{{#if view.showCloseBtn}}',
        '<a class="close" rel="close">×</a>',
        '{{/if}}',
            '<h2>{{view.header}}</h2>',
        ].join("\n");

    Khelkund.Views.ModalPopUpHeaderView = Em.View.extend({
        header: 'Header title',
        classNames: 'modal-header',
        showCloseBtn:true,// setting to show close btn on header on not
        template: Em.Handlebars.compile(headerTemplate),
        });
    return Khelkund.Views.ModalPopUpHeaderView;
    });
