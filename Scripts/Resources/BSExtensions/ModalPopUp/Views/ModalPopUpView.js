﻿; define(['ember', 'bs_modalpopupheaderview', 'bs_modalpopupbodyview', 'bs_modalpopupfooterview'], function (ember,bs_modalpopupheaderview, bs_modalpopupbodyview, bs_modalpopupfooterview) {
    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};
    var modalPopUpTemplate = [


        '{{view view.headerView}}',

        '{{view view.bodyView}}',

        '{{view view.footerView}}',
    ].join("\n");



    var bodyTemplate = [].join("\n");
    Khelkund.Views.ModalPopUpView = Em.View.extend({
        template: Em.Handlebars.compile(modalPopUpTemplate),
        classNames: ['modal','lowercase','hide'],
       

        headerView: bs_modalpopupheaderview,

        bodyView: bs_modalpopupbodyview,
            
        onPrimaryClick: Em.K,
        
        onSecondaryClick: Em.K,
        
        onHide: Em.K,

        footerView: bs_modalpopupfooterview,

        click: function (event) {
            var target = event.target;
              var  targetRel = target.getAttribute('rel');

            if (targetRel === 'close') {
                this.hide();
                return false;

            } else if (targetRel === 'primary') {
               
                    this.onPrimaryClick();
                    this.hide();
                    return false;
                
            } else if (targetRel === 'secondary') {
               
                    this.onSecondaryClick();
                    this.hide();
                    return false;
                
            }
        },

        popup: function (options) {
           
            $('#' + this.elementId).modal({
            
                backdrop:'static',
                keyboard:false,
            });
           


            //if (this.state == 'inDOM') {
            //    var modalPane;
            //    if (!options) options = {};
            //    modalPane = this.create(options);
            //    modalPane.appendTo('#' + this.elementId);
            //}
        },

        hide: function (options) {
           
           // $('#' + this.elementId).removeData('modal');
            $('#' + this.elementId).modal('hide');
            this.onHide();
           // this.destroy();
            //$('#modal').remove();
            
           
            
        },

        remove: function () {

            $('#' + this.elementId).remove();
            this.destroy();
        }
       
    });

    return Khelkund.Views.ModalPopUpView;
});