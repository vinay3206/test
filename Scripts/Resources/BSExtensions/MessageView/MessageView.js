﻿/* Creates a generic message view used for displaying messages. Provides some settings and hooks to handle its working.
    * MessageView
    * This view works as error, warning and info view. Can be used directely if emberextensions is included in the js file.
    * Type: this field decides the type of view by giving alert-error, alert-warning...... etc classes to the view and also changes the 
            display icon of the message.
    * containerClass: this feild is for customization or adding any additional classes the to container of message view.
    * iconClass: This is used to customize the icon shown in front of the message. If nothing is provided it goes by default implementaion of 
                different views with their icons.
    * autoClose: Default: false. This feild decides weather the view is to be closed after some time automatically or not. `autoCloseInterval` decides the interval if
                `autoClose` is turned on. Default `false`
    * showCloseIcon: Default: true. this decides weather the cross button is to be shown on top-right corner of message box or not. Default `true`.
    * autoFocusOnInsert: Default: false. this option enables the autofocussing of this view when inserted
    * destroyOnHide: Default: true. Controls weather the view gets destroyed on hide or remain hidden only.

   
```js
//basic usage
{{view Khelkund.Views.MessageView}}
```
*output*
```html

```



*/
; define(['ember'],
    function (ember) {
        //extend Khelkund object or create one if it dosent exist
        Khelkund = window.Khelkund || {};
        Khelkund.Views = Khelkund.Views || {};
        //add message view to emberextensions
        Khelkund.Views.MessageView = Em.View.extend({
            /**
             * @private
             * Property to store value for header text.
             * @type {String}
             */
            _viewHeader: null,
            /**
             * Computed property to display header text. Acts
             * as both setter and getter.
             */
            viewHeader: function (key, value) {
                if(arguments.length > 1) {
                    //setter
                    this.set('_viewHeader', value);
                }
                if(this.get('_viewHeader') === null) {
                    this.set('_viewHeader', this.get('type'));
                }
                return this.get('_viewHeader');
            }.property('_viewHeader', 'type'),
            message: null,
            type: null,
            classNames: ['alert'],
            classNameBindings: ['containerClass', 'enableFadeInInsert:fade'],
            containerClass: null,
            iconClass: null,

            //customization settings
            autoClose: false,       //if true automatically hides/destroys the view after the assigned interval
            autoCloseInterval: 5,   //interval in seconds
            showCloseIcon: true,    //enable to close message using cross button.
            destroyOnHide: true,    //destroy the view on hide. Default true.
            autoFocusOnInsert: false,   //automatically scrolls the body to this view if true
            enableFadeInInsert: true,

            //key which can be used to monitor the visibility of view. Message view sets it as false when it gets hidden or destroyed.
            isViewVisible: false,

            init: function () {
                this._super();
                //setup of template and classes to be setup in template if template is not provided
                if (this.get('type') == 'error') {
                    this.set('containerClass', 'alert-error');
                    this.set('iconClass', 'reserved-ticketing-red');
                } else if (this.get('type') == 'warning') {
                    //this.set('containerClass', 'alert-error');
                    this.set('iconClass', 'icon-warning-sign');
                } else if (this.get('type') == 'info') {
                    this.set('containerClass', 'alert-info');
                    this.set('iconClass', 'icon-info-sign');
                } else if (this.get('type') == 'success') {
                    this.set('containerClass', 'alert-success');
                    this.set('iconClass', 'reserved-ticketing-confirmed');
                } else {
                    this.set('containerClass', '');
                    this.set('iconClass', '');
                }

                //setup template if not provided
                if (!this.get('template')) {
                    //this.set('template', Em.Handlebars.compile("{{view.message}}"))
                    var template = [];
                    if (this.get('showCloseIcon') === true) {
                        template.push('<button type="button" class="close mrl" {{action closeView target="view"}}>×</button>');
                    }
                    template.push('<i class="' + this.get('iconClass') + ' mrm"></i>');
                    template.push('<div class="message inlineB">{{{view.viewHeader}}}</div><div>{{{view.message}}}</div>');

                    this.set('template', Em.Handlebars.compile(template.join('\n')));
                }
            },
            didInsertElement: function () {
                this._super();
                //setup of option provided
                if (this.get('autoClose') === true) {
                    var closeDelay;
                    try {
                        closeDelay = parseFloat(this.get('autoCloseInterval'));
                    } catch (err) {
                        closeDelay = 5;
                    }
                    closeDelay = closeDelay * 1000;
                    var autoCloseHandler = Em.run.later(this, function () {
                        this.send('closeView');
                    }, closeDelay);
                    this.set('autoCloseHandler', autoCloseHandler);
                }
                //handle the autofocus functionality
                if (this.get('autoFocusOnInsert')) {
                    this.focusMe();
                }
                if(this.get('enableFadeInInsert')){
                    this.$().addClass('in');
                }
            },
            willDestroyElement: function () {
                //destroy autoclosehandler while destorying view
                Em.run.cancel(this.get('autoCloseHandler'));
                //set isViewVisible to false
                this.set('isViewVisible', false);
            },
            //function to animate the focus functionality.
            //override this if want to change or modify the animation.
            focusMe: function () {
                $('body').animate({
                    scrollTop: this.$().offset().top - 20
                });
            },
            actions: {
                closeView: function () {
                    this.$().hide($.proxy(function () {
                        //set the visibility key to false
                        this.set('isViewVisible', false);
                        //using run once so that above key should trigger observers if any.
                        Em.run.once(this, function () {
                            //after the animation completes check if destory on hide is false
                            if (this.get('destroyOnHide')) {
                                this.destroy();
                            }
                        });

                    }, this));
                }
            }

        });

        return Khelkund.Views.MessageView;
    });

