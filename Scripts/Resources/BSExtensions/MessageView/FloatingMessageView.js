;
define(['ember', 'bs_messageview'],
	function (ember, bs_messageview) {
		Khelkund = window.Khelkund;
		Khelkund.Views = Khelkund.Views || {};

		Khelkund.Views.FloatingMessageView = bs_messageview.extend({
			//any customizations for positioning sould be done here
			
			/**
			 * This property is of lower preference than disableStacking thus if that property is false this will not be considered.
			 * @type {Number}
			 */
			topCord: 0,
			botCord: 0,
			leftCord: 0,
			rightCord: 0,
			classNames: ['floating-message'],
			/**
			 * Disables the stacking i.e. new views will overlap older ones. By default false. Thus new views will stack.
			 * @type {Boolean}
			 */
			disableStacking: false,
			applyFloatingClasses: function () {
				//this featue is still under-heavy dev. thus disabled for now.
				/*var calculatedTop = this.get('topCord');
				if(!this.get('disableStacking')){
					calculatedTop =
				};*/
				this.$().css({
					position: 'fixed',
					top: this.get('topCord'),
					bot: this.get('botCord'),
					left: this.get('leftCord'),
					right: this.get('rightCord')
				});
			}.on('didInsertElement')
		});

		return Khelkund.Views.FloatingMessageView;
	});