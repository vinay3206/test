﻿/*
    This is same as dropdown button view but the default look is button view as in template button is used here

    This view has an additional button before the carret button which has a different action which gets raised on the view with the name of "splitButtonAction" 

    This function can be created in the extended view to handle the click of split button and the other properties of default dropdown view are inherited.

    sample usage:
    SplitButtonView: 
    {{view Khelkund.Views.BSDropdownSplitButtonView dropdownListDataBinding='view.demoData'}}


    Advanced Usage:
    if want to handle the action: 
        extend the view in the script as :
       
       mySplitButton: Khelkund.Views.BSDropdownSplitButtonView.extend({
            dropdownListDataBinding: 'view.parentView.demoData',
            valueBinding: 'controller.choosenValue',
            splitButtonAction: function(){
                khelkundutils.logger.log('handled the split click');
                //----------other code here
            },
        });

        usage in template
        some view having the custom implementation of split button view as mysplitButton
        
        {{view view.mysplitButton}}
        
        //in this case we are not using any binding in the tempate portion because we have already done that in the script portion of the view.


*/

;


define(['bs_dropdownview'], function (dropdownview) {
    
    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};



    Khelkund.Views.BSDropdownSplitButtonView = dropdownview.extend({
        classNames: ['btn-group'],
        
        selectionTemplate: "<button class='btn' {{action splitButtonAction target='view'}}>{{view.title}}</button><a {{bind-attr class=':dropdown-toggle :btn view.selectionClassNames'}} data-toggle='dropdown'><span class='caret'></span></a>",
        titleBinding: Em.Binding.oneWay('selectedItemData.text'),


        actions: {
            splitButtonAction: function () {
            }
        },
    });

    return Khelkund.Views.BSDropdownSplitButtonView;

});