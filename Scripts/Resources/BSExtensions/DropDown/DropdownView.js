﻿/*
DropdownView is a wrapper on bootstap dropdown plugin. It sets all the defaults to BS defaults, and for any customizations extend it.
Selection list is rendered using ul and li tags to render the list.


Basic usage :
    * you need to include bs_dropdownview to get this view.
    * USAGE:
        {{view Khelkund.Views.BSDropdownView dropdownListData="[{ value: '1', text: 'Apple' },{ value: '2', text: 'Orange' },{ value: '3', text: 'Banana' }]"}}
    * dropdownListData is an array of text and value as properties which are used to render the dropdown list
    * text property is used to display the text of the list elements and optionally the title of the list.
    * value property goes into the data-value attriute of the each list element  and points to the selected element's valuekey property.
    * selectedItemData: this property contains the data of the selected item(first match only) this can be used to display the text on the 
        selection.
        



Configurations: 
    * selectionTemplate: type = string, this template is used to render the title of the dropdown.
        - customization: selectionClassNames(type = string) = this property sets some additional classes on the title element of the dropdown.
    * selectionCaretClassNames: type comma seperated string. This is used to customize class names for caret in the dropdown menu. __IMP__: while overriding this property 'caret' should
        also be given as class name as it overrides the defaults.
    * listElementTemplate: type=string, this template contains the single entry in the list and this template is iterated as per the number of enteries
    * selectionClassNames: (type= string)[space seperated] if you dont want to create a new template then use this property to add classes to the title element. for e.g. add class "btn"
        to make it a button looking dropdown
    * listHeaderClassNames: adds class to the ul element of dropdownlist
    * listElementClassNames: adds class to the li element inside the ul of dropdownlist
    * textkey: this property is used if we are giving the dropdownListData array with each object having properties other than
        text and value. Then we can give the textkey as the name of the property to get picked for the text property. This is to be done if we
        are using the default template, else the custom template can be customized as per new properties.
    * valuekey: same as textkey property, but used for the value property. It is used to compare the selections to decide the 
        active selection thus it must unique(for single selection) e.g. Id property.
    * deferInitialSelection: normally dropdown view automatically initializes with default 0th index as selected item but in some cases if we dont want that we can set this key to true

    //to know the default set of classes applied to these elements refer the default templates below used for them.
    [NOTE: do not remove the ul li pattern of dropdown while using your own templates because the selection is written on those elements. You may\
    change the class names using the hooks provided but the list should be using ul and  li tags only]

Events :
    selectionChanged : this will fired if any value change happens on target selection box, will be useful for handling custom requirements on option selection. A "data" is passed to this
        function which has selectedItemData as object of the clicked or changed element. This event is also fired when the "value" property is changed
        from any source.
        If any con

        [NOTE : This event is not supported in actual plgin, and to support this we made some changes in actual plugin code, so need to take care this while plugin upgradation]

AdvancedUsage:
//template--------------
    Choose Your Fruit: 
    {{view Khelkund.Views.BSDropdownView dropdownListDataBinding='view.fruitsList' valueBinding='selectedFruit'}}
    I want: {{view.selectedFruit}}
//template--------------------
*/

;
define(['ember', 'optionsmixin','khelkundutils'], function (ember, optionsmixin, khelkundutils) {
   
    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};

    var selectionTemplate = "<a {{bind-attr class=':dropdown-toggle view.selectionClassNames'}} data-toggle='dropdown' href='#'> {{view.title}} <span {{bind-attr class='view.selectionCaretClassNames'}}></span></a>";
    //data-value has a placeholder which places the valueKey, this valueKey can be set in cases the custom object which dont have value
    // as its property for the data-value
    //similarly the text to show between a tags has a placeholder
    var listElementTemplate = "<li {{action elementSelected this target='view'}} {{bind-attr data-value='{0}' class='disabled view.listElementClassNames'}} ><a href='#;'>{{{1}}}</a></li>";

    Khelkund.Views.BSDropdownView = Em.View.extend(optionsmixin, {
        classNames: ['dropdown'],

        value: '',
        title: 'select a value',
        selectedItemData: { text: '', value: '' },
        //these class [space seperated] will be applied to the selectiontempalte element
        selectionClassNames: '',
        selectionCaretClassNames: 'caret',
        listHeaderClassNames: '',
        listElementClassNames: '',
        //data is assumed to be in format of text and value, it can also contain an extra feild "disabled" to disable the list element 
        //for customization is required change the textkey and valuekey elements
        textKey: 'text',//not able to use these properties now
        valueKey: 'value',
        dropdownListData: null,
        //event to be raised when the element is selected on dropdownlist
        //initialized with an empty function
        selectionChanged: Em.K,

        //tempalte to show the selected element
        selectionTemplate: null,
        //tempalte to show the list element(s)
        listElementTemplate: null,

        //text
        

        //setup templates and the data received
        init: function () {
            this._super();
            if (!this.get('selectionTemplate')) {
                this.set('selectionTemplate', selectionTemplate);
            }
            if (!this.get('listElementTemplate')) {
                this.set('listElementTemplate', listElementTemplate);
            }
            //format function has issues as it remove one curly braces thus not working correctely
            //String.format(this.get('listElementTemplate'), this.get('valueKey'));
            this.set('listElementTemplate', this.get('listElementTemplate').replace("{0}", this.get('valueKey')));
            this.set('listElementTemplate', this.get('listElementTemplate').replace("{1}", this.get('textKey')));



            var dropdownTemplate = [
                this.get('selectionTemplate'),
                "<ul {{bind-attr class=':dropdown-menu view.listHeaderClassNames'}}>",
                "{{#each view.dropdownListData}}",
                this.get('listElementTemplate'),
                "{{/each}}",
                "</ul>",
                this.get('addAfter')
            ].join("\n");

            //format data
            this.setDropDownListData();

            //INFO: creating a template for dropdown
            this.set('template', Em.Handlebars.compile(dropdownTemplate));

        },
        //converts string format data to an array of item, will be used if data is specified in template directly
        setDropDownListData: function () {
            if (this.get('dropdownListData') && typeof (this.get('dropdownListData')) == "string") {
                var data = khelkundutils.serializer.deserialize(this.get('dropdownListData'));
                this.set('dropdownListData', data);
            }
        },

        //setup the bindings on the click of list element
        didInsertElement: function () {
            this._super();

            $('.dropdown-toggle', this.$()).dropdown();
            //INFO: this will not work if the dropdownListData is given as a binding cuz it ll be available later on then.
            //$('ul.dropdown-menu>li', this.$()).each($.proxy(function (idx, ele) {
            //    $(ele).data('value', this.get('dropdownListData').objectAt(idx)[this.get('valueKey')]);
            //}, this));

            //calling updateSelection to set it the firt time when the bindings are not working
            if(!this.get('deferInitialSelection')){
                this.updateSelection();
            }
            //$('.dropdown-menu').on('touchstart.dropdown.data-api', function (e) {
            //    e.stopPropagation();
            //});
        },

        //raised when the item on the dropdown list is clicked
        //this method also raises the selectionChanged event which can be implemented for other usage
        //the data containing the text and value of the selected element is passed as an arugment to the raised function
        actions: {
            elementSelected: function (data) {

                if (data.disabled) return;


                //changing the value will trigger update selection thus making the rest of calls
                this.set('value', data[this.get('valueKey')]);
            }
        },
        //this function is called each time the selection is changed and it sets the active class on the element whose value is same as the selectedvalue
        activateSelection: function () {
            $('ul.dropdown-menu>li', this.$()).removeClass('active');
            $('ul.dropdown-menu>li', this.$()).each($.proxy(function (idx, ele) {
                if ($(ele).data('value') == this.get('value')) {
                    $(ele).addClass('active');
                }
                
            }, this));
        },
        //called eacht time value is changed: it changes the value to the
        //it also raises the selectionChanged event which can be overrided by the implementor.
        updateSelection: function () {
            if (this.get('state') != 'inDOM') {
                return;
            }
            var i = 0;
            var data = null;
            
            if(Em.isArray(this.get('dropdownListData')) && this.get('dropdownListData').length > 0){
                
                //setting the default selected value will run only when value is not set
                if (!this.get('value') && !this.get('deferInitialSelection')) {
                    this.set('value', this.get('dropdownListData')[0][this.valueKey]);
                }

                for (i = 0; i < this.get('dropdownListData').length; i++){
                    if(this.get('dropdownListData')[i][this.valueKey] == this.get('value')){
                        //data[this.get('textKey')] = this.get('dropdownListData')[i][this.get('textKey')];
                        //data[this.get('valueKey')] = this.get('dropdownListData')[i][this.get('valueKey')];
                        data = this.get('dropdownListData')[i];
                        
                        break;
                    }
                }
            

                if(data === null) {
                    data = this.get('dropdownListData')[0];
                }
                   
                //set the selecteditemdata as the data of selected item
                //or if not found then set 0th index as selected
                this.set('selectedItemData', data);
                //INFO: raising the event after change of selection
                this.selectionChanged(data);

                //make the selected element as active
                this.activateSelection();
            }
        }.observes('value', 'dropdownListData.@each'),


        //removes the binding created on the ui
        willDestroyElement: function () {
            this._super();

            this.$().off('click.ember-dropdown');
        }
    });

    return Khelkund.Views.BSDropdownView;

});