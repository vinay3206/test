﻿/*
This is same as normal dropdown but the only difference is that the value of the link is updated when an item is selected in the dropdown.

Sample Usage:
   some parent view [START]

    childrenDdl:[{ value: '0', text: '--Select--' },{ value: '1', text: 'Apple' },{ value: '2', text: 'Orange' },{ value: '3', text: 'Banana' }]   
   
   some parent view [END]
   
   `````
   some parent view template [START]

   {{view Khelkund.Views.BSDropdownButtonView dropdownListDataBinding="view.childrenDdl" class="span11 btn-group" selectionClassNames="span11 btn" title="--Select--"}}

   some parent view template [END]

Sample HTML output rendered :
    <div id="ember351" class="ember-view template-ember dropdown btn-group span11">
        <a data-toggle="dropdown" class="btn dropdown-toggle span11" data-bindattr-9="9">
            --Select--
            <span class="caret pull-right"></span>
       </a>
        <ul class="dropdown-menu">
            <li data-bindattr-11="11"><a href="javascript:void(0);">--Select--</a></li>
            <li data-bindattr-12="12"><a href="javascript:void(0);">Apple</a></li>
            <li data-bindattr-13="13"><a href="javascript:void(0);">Orange</a></li>
            <li data-bindattr-14="14"><a href="javascript:void(0);">Banana</a></li>
        </ul>
    </div>


    The button look of this view is not by default you have to add "btn-group" class to the main view and "btn" class to the inner view for that to happen.

*/

define(['bs_dropdownview'], function (dropdownview) {

    Khelkund = window.Khelkund || {};
    Khelkund.Views = Khelkund.Views || {};

    Khelkund.Views.BSDropdownButtonView = dropdownview.extend({
        
        setRequiredBindings: function () {
          Em.bind(this, 'title', 'selectedItemData.' + this.get('textKey'));
        }.on('init')
    });

    return Khelkund.Views.BSDropdownButtonView;

});