﻿/*
    Requires 'modernizer' for adding html5 polyfills like placeholder etc.
*/

; define(['embercore','moment'], function (ember,moment) {
    

    
    Em.TextField.reopen({
        classNames: [],
        attributeBindings: ['autocomplete', 'data-view-name'],
        autocomplete: 'off',
        selectAllOnFocus: false,
        disabled:false,
        
        
       
    });

    // registering handlerbars helper

    Ember.Handlebars.helper('toLocalFormat', function (date) {
        var d = moment.utc(date),
            month = '' + (d.month()+1),
            day = '' + d.date(),
            year = d.year()

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('/');
    });

    Ember.Handlebars.helper('toMonthFormat', function (date) {
        var d = moment.utc(date),
            month = '' + (d.month()),
            day = '' + d.date(),
            year = d.year();
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June',
                'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        return day + " " + months[month];

    });

    Ember.Handlebars.helper('rankSuperScript', function (rank,options) {
        if (rank == 2) {
            return 'nd';
        }
        if (rank == 3) {
            return 'rd';
        }
        if (rank == 1) {
            return 'st';
        }
        return 'th';

      

    });

    //return value? issue?
    Ember.Handlebars.helper('toLocalTimeFormat', function (date) {
        var d = new Date(date || Date.now());
        if (d) {
            d.toLocaleTimeString();
        }
    });

    Ember.Handlebars.helper('toReadableFormat', function (date) {
        return moment(date,"YYYYMMDD").fromNow();
        
    });


    /**
     * Helper to convert long numbers to their simple concise forms
     * @example
     *         1200 as 1.2K
     * @param  {Number} number Input number to convert to its concise form.
     * @param {hashObject} options Contains the options provided as key value pairs to helper.
     *                             we take roundOffLimit as an option.
     * @return {String}      Smaller and neater representation of that number.
     */
    Ember.Handlebars.helper('consiseNumber', function (number, options) {
        if(number === null || number === undefined) number = 0;
        //convert to Number if it is in string format.
        number = +number;

        var appendText = '';
        var roundOffLimit = options.roundOffLimit || 2;  //read from it settings.
        if(number >= 10000000) {
            //in crores limit
            appendText = 'Cr';
            number = number/10000000;   //also round off to 2/configurable places of decimal
        } else  if(number >= 100000) {
            appendText = 'L';
            number = number/100000;
        } else  if(number >=1000) {
            appendText = "K";
            number = number / 1000;
        }
        return number.toFixed(roundOffLimit) + appendText;
    });
    //also resigter an script accesible function
    Khelkund = window.Khelkund || {};
    Khelkund.Globals = Khelkund.Globals || {};
    Khelkund.Globals.helpers = Khelkund.Globals.helpers || {};
    Khelkund.Globals.helpers.consiseNumber = function (number, options) {
        //initialize option
        options = options || {roundOffLimit : 2};
        if(number === null || number === undefined) number = 0;
        //convert to Number if it is in string format.
        number = +number;

        var appendText = '';
        var roundOffLimit = options.roundOffLimit;  //read from it settings.
        if(number >= 10000000) {
            //in crores limit
            appendText = 'Cr';
            number = number/10000000;   //also round off to 2/configurable places of decimal
        } else  if(number >= 100000) {
            appendText = 'L';
            number = number/100000;
        } else  if(number >=1000) {
            appendText = "K";
            number = number / 1000;
        }
        if(number === parseInt(number)){
            //i.e. number is perfect divisible thus no need to show points
            return number + appendText;
        }

        return number.toFixed(roundOffLimit) + appendText;
    }

    

    
    
});