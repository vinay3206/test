﻿;

require.config({
    paths: {
        
        headercomponent: 'HeaderComponent',
        timercomponent: 'CommonModules/TimerComponent',
        progressbarcomponent: 'CommonModules/ProgressBarComponent'
    }
});

define(['ember','headercomponent', 'timercomponent', 'progressbarcomponent','avatar'],
    function (ember,headercomponent, timercomponent, progressbarcomponent,avatar) {
        window.KhelkundApp = Em.Application.create();
        window.KhelkundGlobals = window.KhelkundGlobals || {};

        //save app name so that all the modules can use ember method to get app by app name.
        //e.g.  Em.Namespace.byName(window.KhelkundGlobals.AppName);
        KhelkundGlobals.AppName = 'KhelkundApp';

        //give all the global settings here
        KhelkundApp.ApplicationController = Em.Controller.extend({
            appSettings: null,
            userInfo: null,


            /**
             * Handles the visibility of loader view. If 0 then loader is not visible.
             * Else visible.
             * @type {Number}
             */
            loaderPromisesCount: 0,
            /**
             * Computed property to control the visibility of loader based on the value of loaderPromiseCount.
             * @return {Boolean} return true when the count of loaderPromiseCount is not 0.
             */
            showGlobalLoader: function () {
                return this.get('loaderPromisesCount') !== 0;
            }.property('loaderPromisesCount'),
            /**
             * Increments the promise count by 1.
             * @param {RSVP.Promise} promise Adds it the list of pending promises.
             */
            addLoaderPromise: function (promise) {
                this.incrementProperty('loaderPromisesCount');
            },
            /**
             * Decrements the promise count by 1.
             * @param  {RSVP.Promise} promise Removes the promise from the list pending promises.
             * @return {Boolean}         true if promise is found.
             */
            removeLoaderPromise: function (promise) {
                this.decrementProperty('loaderPromisesCount');
            },

            actions: {
                setUserInfo: function (userInfo) {
                    this.set('userInfo', userInfo);

                },
            }
        });

        /**
         * Handle all the global events here.
         */
        KhelkundApp.ApplicationRoute = Em.Route.extend({
            actions: {
                /**
                 * Tells controller to add promise to its list to handle the visibility of loaderview.
                 * @param  {RSVP.Promise} promise Promise on whose completion the dependency count should decrease.
                 */
                registerPromiseforLoader: function (promise) {
                    this.get('controller').addLoaderPromise(promise);
                    promise.finally($.proxy(function () {
                        //remove the promise from dependency when complete.
                        this.get('controller').removeLoaderPromise(promise);
                    }, this));
                },

               toLogin: function () {
                    this.transitionTo('login');
                },
            }
        });

        /**
         * Use this view to show Global Loader
         * @type {[type]}
         */
        KhelkundApp.GlobalLoaderView = Em.View.extend({
            templateName: 'globalLoader'
        });


        KhelkundApp.deferReadiness();

        var $element = $('#container');
        if ($element.length === 0) {
            $element = $('body');
        }

        KhelkundApp.rootElement = $element;
        
        //disable these settings when in release mode
        KhelkundApp.LOG_TRANSITIONS = true;
        KhelkundApp.LOG_TRANSITIONS_INTERNAL = true;

        KhelkundGlobals.isAppLoaded = true;
        //initialize the common store for khelkund
        KhelkundGlobals._store = [];


        // registering common components here
        KhelkundApp.HeaderViewComponent = headercomponent;
        KhelkundApp.TimerViewComponent = timercomponent;
        KhelkundApp.ProgressBarComponent=progressbarcomponent;

        var event;
        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent("appLoaded", true, true);
        } else {
            event = document.createEventObject();
            event.eventType = "appLoaded";
        }
        event.eventName = "appLoaded";

        if (document.createEvent) {
            document.body.dispatchEvent(event);
        } else {
            document.body.fireEvent("on" + event.eventType, event);
        }
    });